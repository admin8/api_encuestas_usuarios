<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Encuesta de riesgo psicosocial</title>
   <style>
   

      .clearfix:after {
        content: "";
        display: table;
        clear: both;
      }

      a {
        color: #0087C3;
        text-decoration: none;
      }

      body {
        position: relative;
        width: 100%;  
        height: 29.7cm; 
        margin: 0 auto; 
        color: #555555;
        background: #FFFFFF; 
        font-family: Arial, sans-serif; 
        font-size: 13px; 
      }

      header {
        padding: 10px 0;
        margin-bottom: 20px;
        border-bottom: 1px solid #AAAAAA;
        position:relative;
      }

      #logo {
        
        margin-top: 8px;
      }

      #logo img {
        height: 70px;
      }

      #company {
        position: absolute;
        top: 30px;
        right: 10px;
        text-align: right;
      }


      #details {
        margin-bottom: 50px;
      }

      #client {
        padding-left: 6px;
        border-left: 6px solid #0087C3;
        float: left;
      }

      #client .to {
        color: #777777;
      }

      h2.name {
        font-size: 1.4em;
        font-weight: normal;
        margin: 0;
      }

      #invoice {
        text-align: right;
      }

      #invoice h1 {
        color: #0087C3;
        font-size: 2.4em;
        line-height: 1em;
        font-weight: normal;
        margin: 0  0 10px 0;
      }

      #invoice .date {
        font-size: 1.1em;
        color: #777777;
      }

      table {
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 20px;
        border: 1px solid #DDDDDD !important;
      }
      table td, th{
        border: 1px solid #DDDDDD !important;
      }
      table th,
      table td {
        padding: 20px;
        background: #EEEEEE;
        text-align: center;
        border-bottom: 1px solid #eee;
      }

      table th {
        white-space: nowrap;        
        font-weight: normal;
      }

      table td {
        text-align: right;
      }

      table td h3{
        color: #57B223;
        font-size: 1.2em;
        font-weight: normal;
        margin: 0 0 0.2em 0;
      }

      table .no {
        color: #FFFFFF;
        font-size: 1.6em;
        background: #DDDDDD;
      }

      table .desc {
        text-align: left;
      }

      table .unit {
        background: #DDDDDD;
      }

      table .qty {
      }

      table .total {
        background: #57B223;
        color: #FFFFFF;
      }

      table td.unit,
      table td.qty,
      table td.total {
        font-size: 1.2em;
      }

      table tbody tr:last-child td {
        border: none;
      }

      table tfoot td {
        padding: 10px 20px;
        background: #FFFFFF;
        border-bottom: none;
        font-size: 1.2em;
        white-space: nowrap; 
        border-top: 1px solid #AAAAAA; 
      }

      table tfoot tr:first-child td {
        border-top: none; 
      }

      table tfoot tr:last-child td {
        color: #57B223;
        font-size: 1.4em;
        border-top: 1px solid #57B223; 

      }
      table th, table td{
        text-transform:uppercase;
      }
      table tfoot tr td:first-child {
        border: none;
      }

      #thanks{
        font-size: 2em;
        margin-bottom: 50px;
      }

      #notices{
        padding-left: 6px;
        border-left: 6px solid #0087C3;  
      }

      #notices .notice {
        font-size: 1.2em;
      }

      footer {
        color: #777777;
        width: 100%;
        height: 30px;
        position: absolute;
        bottom: 0;
        border-top: 1px solid #AAAAAA;
        padding: 8px 0;
        text-align: center;
      }
      .b-white{
        background-color: white;
      }
      .t-left{
        text-align: left;
      }

      .t-right{
        text-align: right;
      }

      .t-center{
        text-align: center;
      }
      .upper{
        text-transform: uppercase;
      }
   
   </style>
  </head>
  <body>
    <header class="clearfix">
      <div id="logo">
        <img src="{!!public_path() !!}/logo.jpg">
      </div>
      <div id="company">
        <h2 class="name">Trabajo  sin stress</h2>
        <div><a href="https://www.trabajosinstress.mx/" target="_new">www.trabajosinstress.mx</a></div>
        <div>33 3813 1374 EXT. 1028 Y 1021</div>
        <div><a href="mailto:contacto@trabajosinstress.mx">contacto@trabajosinstress.mx</a></div>
      </div>
      </div>
    </header>
    <main>
      <div id="details" class="clearfix">
        <div id="client">
          <div class="to">ENCUESTA DE RIESGO PSICOSOCIAL</div>
          <h2 class="name">{!! $result['Empleado'] !!}</h2>
          <div class="address">{!! $result['Paysheet'] !!}</div>
        </div>
        <div id="invoice">
          <h1 class="upper">{!! $result['guia'] !!}</h1>
          <div class="date">Fecha de impresión: {!! $date !!} </div>
        </div>
      </div>

      <table border="0" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th class="unit">Riesgo por categoria</th>
            <th class="unit">Riesgo de trabajo</th>
            <th class="unit">Riesgo general</th>
          </tr>
        </thead>
        <tbody>
          @foreach($result['val'] as $row)
            <tr>
              <td class="t-left b-white">{!! $row['categoria'] !!}</td>
              <td class="t-center" style="background-color: {!! $row['color'] !!}">{!! $row['valor'] !!} {!! $row['riesgo'] !!}</td>
              @if($row['categoria'] == 'Ambiente de trabajo')
                <td class="t-center" rowspan="{!! count($result['val']) !!}" style="background-color: {!! $result['colorGral'] !!}">{!! $result['riesgoGralValor'] !!} {!! $result['riesgoGral'] !!}</td>
              @endif
            </tr>
          @endforeach
        </tbody>
      </table>
      <div id="thanks">Gracias por usar nuestra plataforma!</div>
      <div id="notices">
        <div>NOTA:</div>
        <div class="notice">Ayudanos a que se aplique esta encuesta en tu empresa y puedan cumplir con la NOM-035-STPS-2018</div>
      </div>
    </main>
    <footer>
      Estos son resultadods de la encuesta aplicada en trabajo sin stress.
    </footer>
  </body>
</html>