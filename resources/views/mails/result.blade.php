<html>
                    <body>
                    <style type="text/css">
                        @import url("https://cdnjs.cloudflare.com/ajax/libs/foundation-emails/2.2.1/foundation-emails.min.css"); 

                        body,
                        html,
                        p, 
                        .body {
                            background: #FFFFFF  !important;
                            color:#000000;
                            text-align:center;
                        }
                        .container.header {
                            background: #FFFFFF;
                        }
                        .body-drip {
                            border-top: 8px solid #663399;
                        }
                        .success{
                            background: #48f248 !important;
                            border: none;
                            padding: 10px 20px;
                            color: #fff !important;
                            font-size: 16px;
                            text-transform: uppercase;
                            font-weight: 500;
                        }
                        
                        .download{
                            color: #fff !important;
                            background-color: #007bff !important;
                            border-color: #007bff !important;
                            padding: 10px 20px;
                            font-size: 16px;
                            text-transform: uppercase;
                            font-weight: 500;
                        }
                        a{
                            text-align:center !important;
                            display:inline-block !important;
                            position:relative !important;
                        }
                    </style>
                    <!-- move the above styles into your custom stylesheet -->
                    
                    <spacer size="16"></spacer>
                    
                    <container class="header">
                        <row class="collapse">
                        <columns>
                            <center>
                                <h4 class="text-center" style="color:#0067be;"><strong>{{ $Nombre }} </strong> gracias por contestar la encuesta de riesgo psicosocial de la NOM 035.</h4>
                            </center>
                        </columns>
                        </row>
                    </container>
                    
                    <container class="body-drip">
                    
                        <spacer size="16"></spacer>
                    
                        <center>
                            <a href="http://www.web.nom035encuesta.com.mx">
                                <img src="https://www.trabajosinstress.mx/img/mail-result.jpg" width="450" alt="Encuesta nom 035" style="width:450px;">
                            </a>
                        </center>
                        <spacer size="16"></spacer>
                        <row>
                        <columns>
                            <center><br><br>
                            <p class="text-center" style="color:#0067be;">¡Envianos los datos para aplicar esta encuesta en tu centro de trabajo!</p>
                                <a href="http://www.web.nom035encuesta.com.mx/#contacto" class="success">Aplicar esta encuesta en mi centro de trabajo</a>
                            </center>
                        </columns>
                        </row>
                    </container>
                    </body>
                    </html>
