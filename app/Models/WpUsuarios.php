<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WpUsuarios extends Model
{
   	public $incrementing  = false;
	public $timestamps    = false;
	protected $connection = "wp_nom035";
	protected $table      = "wp_bb5zze_records_users_survey";
}
