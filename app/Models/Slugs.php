<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slugs extends Model
{
	protected $connection = "encuestas";
	public $incrementing  = false;
	public $timestamps    = false;
	protected $table      = 'sur_det_slugs';
	protected $primaryKey = 'Id_Slug';
	/**
	* campos a cargar de la tabla
	*
	* @var array
	*/
	protected $fillable = ['Id_Slug','Fk_IdSurvey','Description','Slug','ExpirationDate','Active','Delete','LinkNextSurvey','NumberSurveys'];

}

