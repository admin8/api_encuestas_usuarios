<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ViewSumCategory extends Model
{
	//public $incrementing = false;
	protected $connection = "encuestas";
	public $timestamps    = false;
	protected $table      = 'view_sum_for_category';
	protected $primaryKey = 'Id_Person';
	/**
	* campos a cargar de la tabla
	*
	* @var array
	*/
	protected $fillable = ['Id_Category','Id_Survey','Id_Person','sum'];
}
