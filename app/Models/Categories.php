<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
	protected $connection = "encuestas";
	public $timestamps    = false;
	protected $table      = 'nom_cat_catergories';
	protected $primaryKey = 'Id_Category';
	/**
	* campos a cargar de la tabla
	*
	* @var array
	*/
	protected $fillable = [
				'Id_Category',
				'Category',
				'Active',
				'Delete',
				'DateRecords'
			];

}
