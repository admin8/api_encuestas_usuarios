<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Persons extends Model
{
	protected $connection = "encuestas";
    public $timestamps    = false;
	protected $table      = 'sur_his_persons';
	protected $primaryKey = 'Id_Person';
	/**
	* campos a cargar de la tabla
	*
	* @var array
	*/
	protected $fillable = ['Id_Person','Fk_IdSurvey','Fk_IdSlug','Name','FirstName','LastName','Paysheet','Antiquity','DateRecords','DateUpdate'];

	/**
	* campos para ser guardados
	*
	* @var array
	*/
	protected $guarded = ['Id_Person','Fk_IdSurvey','Fk_IdSlug','Name','FirstName','LastName','Paysheet','Antiquity','DateRecords','DateUpdate'];
}