<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
   //public $incrementing = false;
	protected $connection = "encuestas";
	public $timestamps    = false;
	protected $table      = 'sur_cat_survey';
	protected $primaryKey = 'Id_Survey';
	/**
	* campos a cargar de la tabla
	*
	* @var array
	*/
	protected $fillable = ['Id_Survey','Name','Active','Delete','Fk_IdUser','Fk_QuestionParent','Fk_IdAnswerValueParent'];
}
