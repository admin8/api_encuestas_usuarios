<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserJoinSlug extends Model
{
	//public $incrementing = false;
	protected $connection = "encuestas";
	public $timestamps    = false;
	protected $table      = 'nom_det_user_join_slugs';
	/**
	* campos a cargar de la tabla
	*
	* @var array
	*/
	protected $fillable = ['Fk_IdUser','Fk_IdSlug'];
}
