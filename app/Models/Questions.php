<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Questions extends Model
{
	protected $connection = "encuestas";
    public $timestamps    = false;
	protected $table      = 'sur_cat_questions';
	protected $primaryKey = 'Id_Question';
	/**
	* campos a cargar de la tabla
	*
	* @var array
	*/
	protected $fillable = ['Id_Question','Question','Fk_IdSurvey','Order','Fk_IdQuestionParent','Fk_IdAnswerValueParent','ToRecord'];
}
