<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
	protected $connection = "encuestas";
	public $timestamps    = false;
	protected $table      = 'sur_cat_answers';
	protected $primaryKey = 'Id_Answer';
	/**
	* campos a cargar de la tabla
	*
	* @var array
	*/
	protected $fillable = ['Id_Answer','Fk_IdQuestion','Answer','Value','Order','Active','Delete','Fk_IdUser','Fk_IdUserUpdate','DateRecords','DateUpdate'];

	/**
	* campos para ser guardados
	*
	* @var array
	*/
	protected $guarded = ['Id_Answer','Fk_IdQuestion','Answer','Value','Order','Active','Delete','Fk_IdUser','Fk_IdUserUpdate','DateRecords','DateUpdate'];
}
