<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PersonJoinAnswer extends Model
{
	protected $connection = "encuestas";
    public $timestamps    = false;
	protected $table      = 'sur_det_person_join_answer';
	/**
	* campos a cargar de la tabla
	*
	* @var array
	*/
	protected $fillable = ['Fk_IdPerson','Fk_IdQuestion','Fk_IdAnswer','Delete','DateRecords'];

	/**
	* campos para ser guardados
	*
	* @var array
	*/
	protected $guarded = ['Fk_IdPerson','Fk_IdQuestion','Fk_IdAnswer','Delete','DateRecords'];
}
