<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Domains extends Model
{
	protected $connection = "encuestas";
    public $timestamps    = false;
	protected $table      = 'nom_cat_domains';
	protected $primaryKey = 'Id_Domains';
	/**
	* campos a cargar de la tabla
	*
	* @var array
	*/
	protected $fillable = [
				'Id_Domains',
				'Domain',
				'Active',
				'Delete',
				'DateRecords',
				'Fk_IdUser'
			];
}
