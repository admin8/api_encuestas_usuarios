<?php
#https://medium.com/@manuelmauriciozamarrn/implementing-jwt-on-laravel-5-8-edc39f545886
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class APILoginController extends Controller
{
     //Please add this method
    public function login() {
        // get email and password from request
        $credentials = request(['email', 'password']);
        // try to auth and get the token using api authentication
        if (!$token = auth('api')->attempt($credentials)) {
            // if the credentials are wrong we send an unauthorized error in json format
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return response()->json([
			'email'   => auth('api')->user()->email,
			'name'    => auth('api')->user()->name,
			'a'       => auth('api')->user()->Active,
			'd'       => auth('api')->user()->Delete,
			'token'   => $token,
			'expires' => auth('api')->factory()->getTTL() * 90000, // time to expiration

        ]);
    }
	/**
	* Log the user out (Invalidate the token).
	*
	* @return \Illuminate\Http\JsonResponse
	*/
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }
}