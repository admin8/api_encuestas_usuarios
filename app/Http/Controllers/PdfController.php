<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Survey;
use App\Models\Answer;
use App\Models\Persons;
use App\Models\PersonJoinAnswer;
use App\Models\Slugs;
use App\Models\Questions;
use App\Models\WpUsuarios;
class PdfController extends Controller
{
    public function createPdf(Request $Request)
    {
        $data = $Request->only('slug');
		$validate = [
            'slug'         => 'required',
        ];
		$valid = \Validator::make($data, $validate);

		if ($valid->passes()) {
            $resSlug = Persons::select('Id_Person','s.Fk_IdSurvey')->join('sur_det_slugs as s', 's.Id_Slug','=','Fk_IdSlug')->Where('sur_his_persons.Delete',0)->where('Slug',$data['slug'])->first();
            if ($resSlug) {
                $IdSurvey  = $resSlug->Fk_IdSurvey;
                $Id_Person = $resSlug->Id_Person;
                if($IdSurvey==2){

                    $res = Persons::
                        select('Paysheet','s.Name AS Guia',
                            \DB::raw('CONCAT_WS(" ", sur_his_persons.Name, FirstName, LastName)  AS Empleado'),
                            \DB::raw('IFNULL((SELECT sum FROM view_sum_for_category sc WHERE sc.Id_Person=sur_his_persons.Id_Person AND sc.Id_Category = 1 LIMIT 1 ),0) AS AmbienteDeTrabajo'),
                            \DB::raw('IFNULL((SELECT sum FROM view_sum_for_category sc WHERE sc.Id_Person=sur_his_persons.Id_Person AND sc.Id_Category = 2 LIMIT 1 ),0) AS FactorPropioDeActividad'),
                            \DB::raw('IFNULL((SELECT sum FROM view_sum_for_category sc WHERE sc.Id_Person=sur_his_persons.Id_Person AND sc.Id_Category = 3 LIMIT 1 ),0) AS OrganizacionTiempoTrabajo'),
                            \DB::raw('IFNULL((SELECT sum FROM view_sum_for_category sc WHERE sc.Id_Person=sur_his_persons.Id_Person AND sc.Id_Category = 4 LIMIT 1 ),0) AS LiderazgoRelaciones'),
                            \DB::raw('IFNULL((SELECT sum FROM view_sum_for_category sc WHERE sc.Id_Person=sur_his_persons.Id_Person AND sc.Id_Category = 5 LIMIT 1 ),0) AS EntornoOrganizacional')
                            )
                        ->join('sur_cat_survey as s', 's.Id_Survey','=','Fk_IdSurvey')
                        ->Where('sur_his_persons.Delete',0)
                        ->groupBy('Id_Person')
                        ->where('Id_Person',$Id_Person)
                        ->where('Fk_IdSurvey',$IdSurvey)
                        ->first();
                    $result=['Empleado' => $res->Empleado];
                    $result['Paysheet'] = $res->Paysheet;
                    $result['guia'] = $res->Guia;
                    
        
                    $riesgoGral = ($res->AmbienteDeTrabajo + $res->FactorPropioDeActividad + $res->OrganizacionTiempoTrabajo + $res->LiderazgoRelaciones + $res->EntornoOrganizacional);
                    $result['riesgoGralValor'] = $riesgoGral;
                    if($riesgoGral < 50){
                        $result['colorGral'] = '#9be5f7';
                        $result['riesgoGral'] = 'nulo';
                    } else if ($riesgoGral >= 50 && $riesgoGral < 75) {
                        $result['colorGral'] = '#6bf56e';
                        $result['riesgoGral'] = 'bajo';
                    } else if ($riesgoGral >= 75 && $riesgoGral < 99) {
                        $result['colorGral'] = '#ffff00';
                        $result['riesgoGral'] = 'medio';
                    } else if ($riesgoGral >= 99 && $riesgoGral < 140) {
                        $result['colorGral'] = '#ffc000';
                        $result['riesgoGral'] = 'alto';
                    } else if ($riesgoGral >= 140) {
                        $result['colorGral'] = '#ff0000';
                        $result['riesgoGral'] = 'muy alto';
                    }
        
                    $i=0;
                    $result['val'][$i]['categoria'] = 'Ambiente de trabajo';
                    $result['val'][$i]['valor'] = $res->AmbienteDeTrabajo;
                    if($res->AmbienteDeTrabajo < 5){
                        $result['val'][$i]['riesgo'] = 'nulo';
                        $result['val'][$i]['color'] = '#9be5f7';
                    } else if ($res->AmbienteDeTrabajo >= 5 && $res->AmbienteDeTrabajo < 9) {
                        $result['val'][$i]['riesgo'] = 'bajo';
                        $result['val'][$i]['color'] = '#6bf56e';
                    } else if ($res->AmbienteDeTrabajo >= 9 && $res->AmbienteDeTrabajo < 11) {
                        $result['val'][$i]['riesgo'] = 'medio';
                        $result['val'][$i]['color'] = '#ffff00';
                    } else if ($res->AmbienteDeTrabajo >= 11 && $res->AmbienteDeTrabajo < 14) {
                        $result['val'][$i]['riesgo'] = 'alto';
                        $result['val'][$i]['color'] = '#ffc000';
                    } else if ($res->AmbienteDeTrabajo >= 14) {
                        $result['val'][$i]['riesgo'] = 'muy alto';
                        $result['val'][$i]['color'] = '#ff0000';
                    }
                    $i++;
                    $result['val'][$i]['categoria'] = 'Factores propios de la actividad';
                    $result['val'][$i]['valor'] = $res->FactorPropioDeActividad;
                    if($res->FactorPropioDeActividad < 15){
                        $result['val'][$i]['riesgo'] = 'nulo';
                        $result['val'][$i]['color'] = '#9be5f7';
                    } else if ($res->FactorPropioDeActividad >= 15 && $res->FactorPropioDeActividad < 30) {
                        $result['val'][$i]['riesgo'] = 'bajo';
                        $result['val'][$i]['color'] = '#6bf56e';
                    } else if ($res->FactorPropioDeActividad >= 30 && $res->FactorPropioDeActividad < 45) {
                        $result['val'][$i]['riesgo'] = 'medio';
                        $result['val'][$i]['color'] = '#ffff00';
                    } else if ($res->FactorPropioDeActividad >= 45 && $res->FactorPropioDeActividad < 60) {
                        $result['val'][$i]['riesgo'] = 'alto';
                        $result['val'][$i]['color'] = '#ffc000';
                    } else if ($res->FactorPropioDeActividad >= 60) {
                        $result['val'][$i]['riesgo'] = 'muy alto';
                        $result['val'][$i]['color'] = '#ff0000';
                    }
        
                    $i++;
                    $result['val'][$i]['categoria'] = 'Organización del tiempo de trabajo';
                    $result['val'][$i]['valor'] = $res->OrganizacionTiempoTrabajo;
                    if($res->OrganizacionTiempoTrabajo < 5){
                        $result['val'][$i]['riesgo'] = 'nulo';
                        $result['val'][$i]['color'] = '#9be5f7';
                    } else if ($res->OrganizacionTiempoTrabajo >= 5 && $res->OrganizacionTiempoTrabajo < 7) {
                        $result['val'][$i]['riesgo'] = 'bajo';
                        $result['val'][$i]['color'] = '#6bf56e';
                    } else if ($res->OrganizacionTiempoTrabajo >= 7 && $res->OrganizacionTiempoTrabajo < 10) {
                        $result['val'][$i]['riesgo'] = 'medio';
                        $result['val'][$i]['color'] = '#ffff00';
                    } else if ($res->OrganizacionTiempoTrabajo >= 10 && $res->OrganizacionTiempoTrabajo < 13) {
                        $result['val'][$i]['riesgo'] = 'alto';
                        $result['val'][$i]['color'] = '#ffc000';
                    } else if ($res->OrganizacionTiempoTrabajo >= 13) {
                        $result['val'][$i]['riesgo'] = 'muy alto';
                        $result['val'][$i]['color'] = '#ff0000';
                    }
        
                    $i++;
                    $result['val'][$i]['categoria'] = 'Liderazgo y relaciones en el trabajo';
                    $result['val'][$i]['valor'] = $res->LiderazgoRelaciones;
                    if($res->LiderazgoRelaciones < 14){
                        $result['val'][$i]['riesgo'] = 'nulo';
                        $result['val'][$i]['color'] = '#9be5f7';
                    } else if ($res->LiderazgoRelaciones >= 14 && $res->LiderazgoRelaciones < 29) {
                        $result['val'][$i]['riesgo'] = 'bajo';
                        $result['val'][$i]['color'] = '#6bf56e';
                    } else if ($res->LiderazgoRelaciones >= 29 && $res->LiderazgoRelaciones < 42) {
                        $result['val'][$i]['riesgo'] = 'medio';
                        $result['val'][$i]['color'] = '#ffff00';
                    } else if ($res->LiderazgoRelaciones >= 42 && $res->LiderazgoRelaciones < 58) {
                        $result['val'][$i]['riesgo'] = 'alto';
                        $result['val'][$i]['color'] = '#ffc000';
                    } else if ($res->LiderazgoRelaciones >= 58) {
                        $result['val'][$i]['riesgo'] = 'muy alto';
                        $result['val'][$i]['color'] = '#ff0000';
                    }
        
                    $i++;
                    $result['val'][$i]['categoria'] = 'Entorno organizacional';
                    $result['val'][$i]['valor'] = $res->EntornoOrganizacional;
                    if($res->EntornoOrganizacional < 10){
                        $result['val'][$i]['riesgo'] = 'nulo';
                        $result['val'][$i]['color'] = '#9be5f7';
                    } else if ($res->EntornoOrganizacional >= 10 && $res->EntornoOrganizacional < 14) {
                        $result['val'][$i]['riesgo'] = 'bajo';
                        $result['val'][$i]['color'] = '#6bf56e';
                    } else if ($res->EntornoOrganizacional >= 14 && $res->EntornoOrganizacional < 18) {
                        $result['val'][$i]['riesgo'] = 'medio';
                        $result['val'][$i]['color'] = '#ffff00';
                    } else if ($res->EntornoOrganizacional >= 18 && $res->EntornoOrganizacional < 23) {
                        $result['val'][$i]['riesgo'] = 'alto';
                        $result['val'][$i]['color'] = '#ffc000';
                    } else if ($res->EntornoOrganizacional >= 23) {
                        $result['val'][$i]['riesgo'] = 'muy alto';
                        $result['val'][$i]['color'] = '#ff0000';
                    }
        
                } else if ($IdSurvey == 1) {
        
                    $res = Persons::
                        select('Paysheet','s.Name AS Guia',
                            \DB::raw('CONCAT_WS(" ", sur_his_persons.Name, FirstName, LastName)  AS Empleado'),
                            \DB::raw('IFNULL((SELECT sum FROM view_sum_for_category sc WHERE sc.Id_Person=sur_his_persons.Id_Person AND sc.Id_Category = 1 LIMIT 1 ),0) AS AmbienteDeTrabajo'),
                            \DB::raw('IFNULL((SELECT sum FROM view_sum_for_category sc WHERE sc.Id_Person=sur_his_persons.Id_Person AND sc.Id_Category = 2 LIMIT 1 ),0) AS FactorPropioDeActividad'),
                            \DB::raw('IFNULL((SELECT sum FROM view_sum_for_category sc WHERE sc.Id_Person=sur_his_persons.Id_Person AND sc.Id_Category = 3 LIMIT 1 ),0) AS OrganizacionTiempoTrabajo'),
                            \DB::raw('IFNULL((SELECT sum FROM view_sum_for_category sc WHERE sc.Id_Person=sur_his_persons.Id_Person AND sc.Id_Category = 4 LIMIT 1 ),0) AS LiderazgoRelaciones')
                            )
                        ->join('sur_cat_survey as s', 's.Id_Survey','=','Fk_IdSurvey')
                        ->Where('sur_his_persons.Delete',0)
                        ->groupBy('Id_Person')
                        ->where('Id_Person',$Id_Person)
                        ->where('Fk_IdSurvey',$IdSurvey)
                        ->first();
        
                    $result=['Empleado' => $res->Empleado];
                    $result['Paysheet'] = $res->Paysheet;
                    $result['guia'] = $res->Guia;
        
                    $riesgoGral = ($res->AmbienteDeTrabajo + $res->FactorPropioDeActividad + $res->OrganizacionTiempoTrabajo + $res->LiderazgoRelaciones + $res->EntornoOrganizacional);
                    $result['riesgoGralValor'] = $riesgoGral;
        
                    if($riesgoGral < 20){
                        $result['colorGral'] = '#9be5f7';
                        $result['riesgoGral'] = 'nulo';
                    } else if ($riesgoGral >= 20 && $riesgoGral < 45) {
                        $result['colorGral'] = '#6bf56e';
                        $result['riesgoGral'] = 'bajo';
                    } else if ($riesgoGral >= 45 && $riesgoGral < 70) {
                        $result['colorGral'] = '#ffff00';
                        $result['riesgoGral'] = 'medio';
                    } else if ($riesgoGral >= 70 && $riesgoGral < 90) {
                        $result['colorGral'] = '#ffc000';
                        $result['riesgoGral'] = 'alto';
                    } else if ($riesgoGral >= 90) {
                        $result['colorGral'] = '#ff0000';
                        $result['riesgoGral'] = 'muy alto';
                    }
        
                    $i=0;
                    $result['val'][$i]['categoria'] = 'Ambiente de trabajo';
                    $result['val'][$i]['valor'] = $res->AmbienteDeTrabajo;
                    if($res->AmbienteDeTrabajo < 3){
                        $result['val'][$i]['riesgo'] = 'nulo';
                        $result['val'][$i]['color'] = '#9be5f7';
                    } else if ($res->AmbienteDeTrabajo >= 3 && $res->AmbienteDeTrabajo < 5) {
                        $result['val'][$i]['riesgo'] = 'bajo';
                        $result['val'][$i]['color'] = '#6bf56e';
                    } else if ($res->AmbienteDeTrabajo >= 5 && $res->AmbienteDeTrabajo < 7) {
                        $result['val'][$i]['riesgo'] = 'medio';
                        $result['val'][$i]['color'] = '#ffff00';
                    } else if ($res->AmbienteDeTrabajo >= 7 && $res->AmbienteDeTrabajo < 9) {
                        $result['val'][$i]['riesgo'] = 'alto';
                        $result['val'][$i]['color'] = '#ffc000';
                    } else if ($res->AmbienteDeTrabajo >= 9) {
                        $result['val'][$i]['riesgo'] = 'muy alto';
                        $result['val'][$i]['color'] = '#ff0000';
                    }
                    $i++;
                    $result['val'][$i]['categoria'] = 'Factores propios de la actividad';
                    $result['val'][$i]['valor'] = $res->FactorPropioDeActividad;
                    if($res->FactorPropioDeActividad < 10){
                        $result['val'][$i]['riesgo'] = 'nulo';
                        $result['val'][$i]['color'] = '#9be5f7';
                    } else if ($res->FactorPropioDeActividad >= 10 && $res->FactorPropioDeActividad < 20) {
                        $result['val'][$i]['riesgo'] = 'bajo';
                        $result['val'][$i]['color'] = '#6bf56e';
                    } else if ($res->FactorPropioDeActividad >= 20 && $res->FactorPropioDeActividad < 30) {
                        $result['val'][$i]['riesgo'] = 'medio';
                        $result['val'][$i]['color'] = '#ffff00';
                    } else if ($res->FactorPropioDeActividad >= 30 && $res->FactorPropioDeActividad < 40) {
                        $result['val'][$i]['riesgo'] = 'alto';
                        $result['val'][$i]['color'] = '#ffc000';
                    } else if ($res->FactorPropioDeActividad >= 40) {
                        $result['val'][$i]['riesgo'] = 'muy alto';
                        $result['val'][$i]['color'] = '#ff0000';
                    }
        
                    $i++;
                    $result['val'][$i]['categoria'] = 'Organización del tiempo de trabajo';
                    $result['val'][$i]['valor'] = $res->OrganizacionTiempoTrabajo;
        
                    if($res->OrganizacionTiempoTrabajo < 4){
                        $result['val'][$i]['riesgo'] = 'nulo';
                        $result['val'][$i]['color'] = '#9be5f7';
                    } else if ($res->OrganizacionTiempoTrabajo >= 4 && $res->OrganizacionTiempoTrabajo < 6) {
                        $result['val'][$i]['riesgo'] = 'bajo';
                        $result['val'][$i]['color'] = '#6bf56e';
                    } else if ($res->OrganizacionTiempoTrabajo >= 6 && $res->OrganizacionTiempoTrabajo < 9) {
                        $result['val'][$i]['riesgo'] = 'medio';
                        $result['val'][$i]['color'] = '#ffff00';
                    } else if ($res->OrganizacionTiempoTrabajo >= 9 && $res->OrganizacionTiempoTrabajo < 12) {
                        $result['val'][$i]['riesgo'] = 'alto';
                        $result['val'][$i]['color'] = '#ffc000';
                    } else if ($res->OrganizacionTiempoTrabajo >= 12) {
                        $result['val'][$i]['riesgo'] = 'muy alto';
                        $result['val'][$i]['color'] = '#ff0000';
                    }
        
                    $i++;
                    $result['val'][$i]['categoria'] = 'Liderazgo y relaciones en el trabajo';
                    $result['val'][$i]['valor'] = $res->LiderazgoRelaciones;
        
                    if($res->LiderazgoRelaciones < 10){
                        $result['val'][$i]['riesgo'] = 'nulo';
                        $result['val'][$i]['color'] = '#9be5f7';
                    } else if ($res->LiderazgoRelaciones >= 10 && $res->LiderazgoRelaciones < 18) {
                        $result['val'][$i]['riesgo'] = 'bajo';
                        $result['val'][$i]['color'] = '#6bf56e';
                    } else if ($res->LiderazgoRelaciones >= 18 && $res->LiderazgoRelaciones < 28) {
                        $result['val'][$i]['riesgo'] = 'medio';
                        $result['val'][$i]['color'] = '#ffff00';
                    } else if ($res->LiderazgoRelaciones >= 28 && $res->LiderazgoRelaciones < 38) {
                        $result['val'][$i]['riesgo'] = 'alto';
                        $result['val'][$i]['color'] = '#ffc000';
                    } else if ($res->LiderazgoRelaciones >= 38) {
                        $result['val'][$i]['riesgo'] = 'muy alto';
                        $result['val'][$i]['color'] = '#ff0000';
                    }
        
                }
                /*
                muestra el pdf en pantalla
                $date = date('d/m/Y');
                $view =  \View::make('pdf.resultados', compact('result', 'date'))->render();
                $pdf = \App::make('dompdf.wrapper');
                $pdf->loadHTML($view);
                return $pdf->stream("resultados NOM-033 $result[Empleado]");
                */
                $dataUser = WpUsuarios::select('Nombre','Email')->whereEncuesta($data['slug'])->first()->toArray();
                \Mail::send( 'mails.result', $dataUser, function( $message ) use ($dataUser, $result){
                    
                    $date = date('d/m/Y');
                    $view =  \View::make('pdf.resultados', compact('result', 'date'))->render();
                    $pdf = \App::make('dompdf.wrapper');
                    $pdf->loadHTML($view);

                    $pdf->setOptions(['temp_dir' => '', 'enable_php' => true]);
                    
                    $message->to( $dataUser['Email'] )
                        ->from( 'contacto@trabajosinstress.mx', 'Trabajo Sin Stress' )
                        ->subject( 'Resultado de encuesta de riesgo psicosocial' )
                        ->attachData($pdf->output(), "resultados NOM-033 $result[Empleado]", [
                            'mime' => 'application/pdf',
                        ]);
                });
                if(count(\Mail::failures()) > 0) {
                    return response()->json(['error' => true, 'message'=> 'ho ho! Algo salio mal! Favor de volver a intentar mas tarde.']);
                } else {
                    return response()->json(['success' => true]);
                }
            } else {
                return response()->json(['error' => true, 'message'=> 'ho ho! Algo salio mal! Error URL.']);
            }
        }
        
    }
}
