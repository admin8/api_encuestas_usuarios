<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing as drawing; // Instead PHPExcel_Worksheet_Drawing
use PhpOffice\PhpSpreadsheet\Style\Alignment as alignment; // Instead PHPExcel_Style_Alignment
use PhpOffice\PhpSpreadsheet\Style\Fill as fill; // Instead PHPExcel_Style_Fill
use PhpOffice\PhpSpreadsheet\Style\Color as color_; //Instead PHPExcel_Style_Color
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup as pagesetup; // Instead PHPExcel_Worksheet_PageSetup
use PhpOffice\PhpSpreadsheet\IOFactory as io_factory; // Instead PHPExcel_IOFactory
use \PhpOffice\PhpSpreadsheet\Style\Border as Border;

use PhpOffice\PhpSpreadsheet\Chart\Chart;
use PhpOffice\PhpSpreadsheet\Chart\DataSeries;
use PhpOffice\PhpSpreadsheet\Chart\DataSeriesValues;
use PhpOffice\PhpSpreadsheet\Chart\Layout;
use PhpOffice\PhpSpreadsheet\Chart\Legend;
use PhpOffice\PhpSpreadsheet\Chart\PlotArea;
use PhpOffice\PhpSpreadsheet\Chart\Title;


use App\Models\Survey;
use App\Models\Answer;
use App\Models\Persons;
use App\Models\PersonJoinAnswer;
use App\Models\Slugs;
use App\Models\ViewSumCategory;
use App\Models\UserJoinSlug;
use App\Models\Categories;
use App\Models\Domains;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        exit;
		//questions
		$questions = Survey::select('Id_Question','Question','NumQuestion')
				  ->join('sur_cat_questions AS q','q.Fk_IdSurvey','Id_Survey')
				  ->join('sur_det_slugs AS l','l.Fk_IdSurvey','Id_Survey')
                  ->where('q.Delete',0)
                  ->where('sur_cat_survey.Delete',0)
                  ->where('q.Fk_IdSurvey',2)
                  ->where('l.Id_Slug',12)
                  ->where('q.ToRecord',1)
                  ->where('q.Active',1)
                  ->get();

        //persons
		$ResultPersons = Persons::select('Id_Person',
						'sur_his_persons.Paysheet AS Nomina',
						\DB::raw('CONCAT_WS(" ",FirstName,LastName, Name) AS Nombre')
						)
				->where('Delete',0)
                ->where('Fk_IdSurvey',2)
				->where('Fk_IdSlug',12)
				->get();
		$ResultPersons = $ResultPersons->toArray();
        $persons = $ResultPersons;
        $lastRow = COUNT($ResultPersons) + 3;
        foreach($persons as $k => $v){
        	$Answers = Survey::select('Value','Question')
                  ->leftjoin('sur_cat_questions AS q','q.Fk_IdSurvey','Id_Survey')
				  ->leftjoin('sur_his_persons AS p','p.Fk_IdSurvey','Id_Survey')
				  ->leftJoin('sur_det_person_join_answer AS pja', function($join) {
                        $join->on('pja.Fk_IdQuestion','Id_Question')
                             ->on('pja.Fk_IdPerson','p.Id_Person')
                             ->where('pja.Delete','0');
                  })
				  ->leftJoin('sur_cat_answers AS a','a.Id_Answer','Fk_IdAnswer')
                  ->where('Id_Person',$v['Id_Person'])
                  ->where('sur_cat_survey.Delete',0)
                  ->where('q.ToRecord',1)
                  ->groupBy('q.Id_Question')
                  ->orderBy('q.Order','ASC')
                  ->get();
            unset($persons[$k]['Id_Person']);
            foreach ($Answers as $ky => $val) {
                $persons[$k][] = $val['Value'] ? $val['Value'] : 0 ;
            }
        }
        $columnas = [];
        foreach($persons[0] as $k => $v){
            if(! is_numeric($k)){
                $columnas[] = $k;
            }
        }
        foreach ($questions as $key => $value) {
            $columnas[] = $value['Question'];
        }

        $spreadsheet = new Spreadsheet();
		$spreadsheet->getProperties()
                ->setCreator("Guillermo Francisco Mercado Ramos")
                ->setLastModifiedBy("Guillermo Francisco Mercado Ramos")
                ->setTitle("Reporte de la NOM 035")
                ->setSubject("trabajosinstress.mx")
                ->setDescription("Reporte de resultados de la encuesta NOM 035")
                ->setKeywords("trabajosinstress.mx")
                ->setCategory("encuestas NOM035");

        $objDrawing = new drawing();
		$objDrawing->setName('TrabajoSinStress');
		$objDrawing->setDescription('TrabajoSinStress');
		$objDrawing->setPath('logo-sin-estres.jpg');
		$objDrawing->setHeight(53);
		$objDrawing->setWorksheet($spreadsheet->getActiveSheet());
		$objWriter = io_factory::createWriter($spreadsheet, 'Xlsx');
        #########################################################################################################
        ################################  hoja 1 califacion por PREGUNTA ENCUESTA   #############################
        #########################################################################################################
    		$sheet = $spreadsheet->getActiveSheet();
    		$spreadsheet->getActiveSheet()
    					->getRowDimension('1')
    					->setRowHeight(40);

            $spreadsheet->getActiveSheet()
    		            ->setCellValue("I1", strtoupper('Encuestas'))
    		            ->getDefaultColumnDimension()->setWidth(20);

            $word='A';
            $lastLetter = 'A';
            $i = 3;
            $item = 1;
            foreach($columnas as $colum){

                $colum = str_replace("<strong>",'',$colum);
                $colum = str_replace("</strong>",'',$colum);
                $colum = str_replace("<small>",'',$colum);
                $colum = str_replace("</small>",'',$colum);
                $colum = str_replace("<ul>",'',$colum);
                $colum = str_replace("</ul>",'',$colum);
                $colum = str_replace("<li>",'',$colum);
                $colum = str_replace("</li>",'',$colum);
                $colum = str_replace("<h4>",'',$colum);
                $colum = str_replace("</h4>",'',$colum);
                $colum = str_replace("</br>"," \n ",$colum);
                $colum = str_replace("</p>"," \n ",$colum);
                $colum = str_replace("</b>"," \n ",$colum);
                $colum = str_replace("<br>"," \n ",$colum);
                $colum = str_replace("<b>"," \n ",$colum);
                $colum = str_replace("<p>"," \n ",$colum);
                $colum = str_replace("é","É",$colum);
                $colum = str_replace("ó","Ó",$colum);
                $colum = str_replace("ú","U",$colum);
                $colum = str_replace("á","Á",$colum);
                $colum = str_replace("í","Í",$colum);
                $colum = preg_replace("/[\r\n|\n|\r]+/", " ", $colum);
                $colum = str_replace("Las preguntas siguientes están relacionadas con la atención a clientes y usuarios.","",$colum);
                $col = strtoupper($colum);
                if($col == 'APELLIDOPATERNO') $col = "APELLIDO PATERNO";
                if($col == 'APELLIDOMATERNO') $col = "APELLIDO MATERNO";

               if($word !== 'A' && $word !== 'B'){
                	 $spreadsheet->getActiveSheet()
                    	->setCellValue($word."2",$item);
                    $item++;
                }

                $spreadsheet->getActiveSheet()
                    ->setCellValue($word.$i,$col);

    			$spreadsheet->getActiveSheet()
    					    ->getStyle($word.$i)
    					    ->getAlignment()
    					    ->setWrapText(true);

                $spreadsheet->getActiveSheet()->getStyle('B4')->getFill()->getStartColor()->getRGB();

                $spreadsheet->getActiveSheet()
                    ->getStyle($word.$i)->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('00aae8');
                $lastLetter = $word;
                $word++;

            }
            $ColumSumAllAnswers = $word;
            $ColumLastAnswers = $lastLetter;
            $spreadsheet->getActiveSheet()
                ->setCellValue($ColumSumAllAnswers.$i,"CALIFICACIÓN GENERAL ENCUESTA");

            $spreadsheet->getActiveSheet()->getStyle('B4')->getFill()->getStartColor()->getRGB();

            $spreadsheet->getActiveSheet()
                ->getStyle($ColumSumAllAnswers.$i)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('FFFF00');

            $spreadsheet->getActiveSheet()
    					    ->getStyle($ColumSumAllAnswers.$i)
    					    ->getAlignment()
    					    ->setWrapText(true);

            $spreadsheet->getActiveSheet()
                ->setCellValue($ColumSumAllAnswers."1","NIVEL DE RIESGO");

            $spreadsheet->getActiveSheet()
                ->getStyle($ColumSumAllAnswers."1")->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('FFFF00');

            $spreadsheet->getActiveSheet()
                            ->getStyle($ColumSumAllAnswers."1")
                            ->getAlignment()
                            ->setWrapText(true);


            $spreadsheet->getActiveSheet()
                ->setCellValue($ColumSumAllAnswers."2","=ROUND(AVERAGE($ColumSumAllAnswers"."4:$ColumSumAllAnswers$lastRow),0)");

            $spreadsheet->getActiveSheet()
                ->getStyle($ColumSumAllAnswers."2")->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('FFFF00');

            $spreadsheet->getActiveSheet()
                            ->getStyle($ColumSumAllAnswers."2")
                            ->getAlignment()
                            ->setWrapText(true);

            ## condiciones de colores para calificacion total
                $Totales1 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                $Totales1->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                $Totales1->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                    ->addCondition('0')->addCondition('49');
                $Totales1->getStyle()->applyFromArray(
                    ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "9be5f7"]]]);

                $Totales2 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                $Totales2->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                $Totales2->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                    ->addCondition('50')->addCondition('74');
                $Totales2->getStyle()->applyFromArray(
                    ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "6bf56e"]]]);

                $Totales3 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                $Totales3->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                $Totales3->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                    ->addCondition('75')->addCondition('98');
                $Totales3->getStyle()->applyFromArray(
                    ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffff00"]]]);

                $Totales4 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                $Totales4->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                $Totales4->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                    ->addCondition('99')->addCondition('139');
                $Totales4->getStyle()->applyFromArray(
                    ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffc000"]]]);

                $Totales5 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                $Totales5->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                $Totales5->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_GREATERTHANOREQUAL)
                                    ->addCondition('140');
                $Totales5->getStyle()->applyFromArray(
                    ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ff0000"]]]);
            ##enf totals conditions
            // AGRAGMOS LOS DATOS DEL REPOR
            $WordContent = 4;

            foreach ($persons as $k => $value) {
                $word='A';
                foreach($value as $ky=> $v){
                    $v = $v ? $v : 0;
                    $spreadsheet
                        ->getActiveSheet()
                        ->setCellValue($word.$WordContent, $v);
                    $word++;
                }
                $spreadsheet
                        ->getActiveSheet()
                        ->setCellValue($ColumSumAllAnswers.$WordContent, "=SUM(C$WordContent:$ColumLastAnswers$WordContent)");
                $spreadsheet->getActiveSheet()->getCell($ColumSumAllAnswers.$WordContent)->getCalculatedValue();

                $spreadsheet->getActiveSheet()->getStyle($ColumSumAllAnswers.$WordContent)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_00);

                        /*$spreadsheet->getActiveSheet()
                        	->getStyle($ColumSumAllAnswers.$WordContent)
                        	->getFill()
                        	->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                        	->getStartColor()
                        	->setARGB('FFFF00');
                        */

                $conditionalStyles = $spreadsheet->getActiveSheet()->getStyle($ColumSumAllAnswers.$WordContent)->getConditionalStyles();
                $conditionalStyles[] = $Totales1;
                $conditionalStyles[] = $Totales2;
                $conditionalStyles[] = $Totales3;
                $conditionalStyles[] = $Totales4;
                $conditionalStyles[] = $Totales5;
                $spreadsheet->getActiveSheet()->getStyle($ColumSumAllAnswers.$WordContent)->setConditionalStyles($conditionalStyles);

                $WordContent++;
            }
            $styleArray = array(
                'font' => array(
                    'bold' => true,
                    'size' => 9
                ),
                'alignment' => array(
                    'wrap' => true,
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                )
            );
            $spreadsheet->getActiveSheet()->getStyle('A3:IZ3')->applyFromArray($styleArray);

            $spreadsheet->getActiveSheet()->getStyle('A1:IZ1')->applyFromArray(['font' => array('bold' => true,'size' => 20)]);
            $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

            $styleArray2 = array(
                'font' => array(
                    'bold' => true,
                    'size' => 18
                ),
                'alignment' => array(
                    'wrap' => true,
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                )
            );
            $spreadsheet->getActiveSheet()->getStyle('X1:IZ1')->applyFromArray($styleArray2);
            $spreadsheet->getActiveSheet()->getStyle($ColumSumAllAnswers.'2')->applyFromArray($styleArray2);

            // Rename worksheet
            $spreadsheet->getActiveSheet()->setTitle('Guias online');

            $spreadsheet->setActiveSheetIndex(0);
            //dejamos fija la fila de titulos
            $spreadsheet->getActiveSheet()->freezePane('A4');
            //aplicamos los bordes
            $spreadsheet->getActiveSheet()
                        ->getStyle("A3:BW$lastRow")
                        ->getBorders()
                        ->applyFromArray( [
                                'allBorders' => [
                                    'borderStyle' => Border::BORDER_THIN,
                                    'color' => ['rgb' => '000000' ]
                                ]
                            ]
                        );

        #########################################################################################################
        ##############################  hoja 2 califacion por dominio y categorias  #############################
        #########################################################################################################


            $spreadsheet->createSheet();

            $spreadsheet->setActiveSheetIndex(1);
            $spreadsheet->getActiveSheet()->setTitle('Categorias y dominios');


            $objDrawing = new drawing();
            $objDrawing->setName('TrabajoSinStress');
            $objDrawing->setDescription('TrabajoSinStress');
            $objDrawing->setPath('logo-sin-estres.jpg');
            $objDrawing->setHeight(53);
            $objDrawing->setWorksheet($spreadsheet->getActiveSheet());
            $objWriter = io_factory::createWriter($spreadsheet, 'Xlsx');

            $sheet = $spreadsheet->getActiveSheet();
            $spreadsheet->getActiveSheet()
                        ->getRowDimension('1')
                        ->setRowHeight(40);

            $spreadsheet->getActiveSheet()
                        ->setCellValue("C1", strtoupper('CALIFICACIÓN de la categoria'))
                        ->mergeCells('C1:G1');

            $spreadsheet->getActiveSheet()
                ->getStyle('C1')->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('92d050');

            $spreadsheet->getActiveSheet()
                        ->setCellValue("H1", strtoupper('Resultado del dominio'))
                        ->mergeCells('H1:Q1');
            $spreadsheet->getActiveSheet()
                ->getStyle('H1')->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('00b0f0');

            $word='A';
            $lastLetter = 'A';
            $i = 2;
            $Array[$word]   = ['title'=>'NOMINA','color'=>'002060', 'size'=> 13];
            $Array[++$word] = ['title'=>'NOMBRE','color'=>'002060', 'size'=> 40];
            $Array[++$word] = ['title'=>'Ambiente de trabajo','color'=>'92d050', 'text-rotation'=>90];
            $Array[++$word] = ['title'=>'Factores propios de la actividad', 'color' =>'92d050', 'text-rotation'=>90];
            $Array[++$word] = ['title'=>'Organización del tiempo de trabajo','color'=>'92d050', 'text-rotation'=>90];
            $Array[++$word] = ['title'=>'Liderazgo y relaciones en el trabajo','color'=>'92d050', 'text-rotation'=>90];
            $Array[++$word] = ['title'=>'Entorno organizacional','color'=>'92d050', 'text-rotation'=>90];
            $Array[++$word] = ['title'=>'Condiciones en el ambiente de trabajo','color'=>'00b0f0', 'text-rotation'=>90];
            $Array[++$word] = ['title'=>'Carga de trabajo','color'=>'00b0f0', 'text-rotation'=>90];
            $Array[++$word] = ['title'=>'Falta de control sobre el trabajo','color'=>'00b0f0', 'text-rotation'=>90];
            $Array[++$word] = ['title'=>'Jornada de Trabajo','color'=>'00b0f0', 'text-rotation'=>90];
            $Array[++$word] = ['title'=>'Interferencia en la relacion trabajo-familia','color'=>'00b0f0', 'text-rotation'=>90];
            $Array[++$word] = ['title'=>'Liderazgo','color'=>'00b0f0', 'text-rotation'=>90];
            $Array[++$word] = ['title'=>'Relaciones en el trabajo','color'=>'00b0f0', 'text-rotation'=>90];
            $Array[++$word] = ['title'=>'Violencia','color'=>'00b0f0', 'text-rotation'=>90];
            $Array[++$word] = ['title'=>'Reconocimiento del desempeño','color'=>'00b0f0', 'text-rotation'=>90];
            $Array[++$word] = ['title'=>'Insuficiente sentido de pertenencia e inestabilidad','color'=>'00b0f0', 'text-rotation'=>90];
            $Array[++$word] = ['title'=>'CALIFICACIÓN GENERAL','color'=>'FFFF00', 'size'=> 30];

            foreach ($Array as $k => $val) {
                if(!isset($val['size'])) $val['size']=12;
                $spreadsheet->getActiveSheet()
                        ->setCellValue($k."$i", $val['title']);

                $spreadsheet->getActiveSheet()
                        ->getColumnDimension($k)->setWidth($val['size']);

                $spreadsheet->getActiveSheet()
                            ->getStyle($k."$i")->getFill()
                            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                            ->getStartColor()->setARGB('00aae8');

                if(isset($val['color']) && $val['color']){
                    $spreadsheet->getActiveSheet()
                            ->getStyle($k."$i")->getFill()
                            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                            ->getStartColor()->setARGB($val['color']);
                }
                if(isset($val['text-rotation']) && $val['text-rotation']){

                    $spreadsheet->getActiveSheet()
                                ->getStyle($k."$i")
                                ->getAlignment()
                                ->applyFromArray( [
                                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                                    'textRotation' => $val['text-rotation'],
                                    'wrapText' => TRUE ] );

                    $spreadsheet->getActiveSheet()
                                ->getRowDimension('2')
                                ->setRowHeight(110);
                }
            }

            $styleArray = array(
                'font' => array(
                    'bold' => true,
                    'size' => 11
                ),
                'alignment' => array(
                    'wrap' => true,
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                )
            );
            $spreadsheet->getActiveSheet()->getStyle('A1:R2')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A2:B2')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
            //dejamos fija la fila de titulos
            $spreadsheet->getActiveSheet()->freezePane('A3');
            //aplicamos los bordes
            $lastRow--;
            $spreadsheet->getActiveSheet()
                        ->getStyle("A1:R$lastRow")
                        ->getBorders()
                        ->applyFromArray( [
                                'allBorders' => [
                                    'borderStyle' => Border::BORDER_THIN,
                                    'color' => ['rgb' => '000000' ]
                                ]
                            ]
                        );
            ######### traemos los datos de la db
            $ResultCatDom = $ResultPersons;
            $letterVal = 'A';
            $row = 3;
            foreach($ResultCatDom as $k => $v){
                $spreadsheet->getActiveSheet()
                        ->setCellValue($letterVal."$row", $v['Nomina']);
                $letterVal++;
                $spreadsheet->getActiveSheet()
                        ->setCellValue($letterVal."$row", $v['Nombre']);
                $letterVal++;
                //CATEGORIES
                /*$categories = Persons::select(\DB::raw('IFNULL(sum(ifnull(`a`.`Value`, 0)),0) AS Sum'))
                            ->join('sur_det_person_join_answer as pa', 'Id_Person','pa.Fk_IdPerson')
                            ->join('sur_cat_answers as a', 'a.Id_Answer','pa.Fk_IdAnswer')
                            ->join('nom_det_dimensions_join_question as dq', 'pa.Fk_IdQuestion','dq.Fk_IdQuestion')
                            ->join('nom_det_dimensions_join_domains as dd', function($join){
                                    $join->on('dd.Fk_IdDimencions','dq.Fk_IdDimension')
                                         ->on('dd.Fk_IdSurvey','sur_his_persons.Fk_IdSurvey');
                                  })
                            ->where('Id_Person',$v['Id_Person'])
                            ->groupBy('Fk_IdCategory')
                            ->orderBy('Fk_IdCategory')
                            ->get();
                */
                $categories = Categories::select(\DB::raw('IFNULL(sum(ifnull(`a`.`Value`, 0)),0) AS Sum'))
                            ->join('nom_det_dimensions_join_domains AS dd', 'dd.Fk_IdCategory','Id_Category')
                            ->join('nom_det_dimensions_join_question AS dq', 'dq.Fk_IdDimension','dd.Fk_IdDimencions')
                            ->leftjoin('sur_his_persons as p', 'p.Fk_IdSurvey','dd.Fk_IdSurvey')
                            ->leftjoin('sur_det_person_join_answer as pa', function($join){
                                    $join->on('pa.Fk_IdQuestion','dq.Fk_IdQuestion')
                                         ->on('pa.Fk_IdPerson','Id_Person')
                                         ->where('pa.Delete','0');
                                  })
                            ->leftjoin('sur_cat_answers AS a', 'a.Id_Answer','pa.Fk_IdAnswer')
                            ->where('Id_Person',$v['Id_Person'])
                            ->groupBy('Id_Category')
                            ->orderBy('Id_Category')
                            ->get();
                foreach ($categories as $key => $val) {
                    $spreadsheet->getActiveSheet()
                        ->setCellValue($letterVal."$row", $val['Sum']);

                    ## condiciones de colores para la categoria de ambiente de trabajo
                    $CatAmbienteTrabajo1 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $CatAmbienteTrabajo1->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $CatAmbienteTrabajo1->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('0')->addCondition('4');
                    $CatAmbienteTrabajo1->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "9be5f7"]]]);

                    $CatAmbienteTrabajo2 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $CatAmbienteTrabajo2->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $CatAmbienteTrabajo2->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('5')->addCondition('8');
                    $CatAmbienteTrabajo2->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "6bf56e"]]]);

                    $CatAmbienteTrabajo3 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $CatAmbienteTrabajo3->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $CatAmbienteTrabajo3->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('9')->addCondition('10');
                    $CatAmbienteTrabajo3->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffff00"]]]);

                    $CatAmbienteTrabajo4 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $CatAmbienteTrabajo4->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $CatAmbienteTrabajo4->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('11')->addCondition('13');
                    $CatAmbienteTrabajo4->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffc000"]]]);

                    $CatAmbienteTrabajo5 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $CatAmbienteTrabajo5->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $CatAmbienteTrabajo5->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_GREATERTHANOREQUAL)
                                        ->addCondition('14');
                    $CatAmbienteTrabajo5->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ff0000"]]]);

                    ## condiciones de colores para la categoria de Factores propios de la actividad
                    $FactorActividad1 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $FactorActividad1->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $FactorActividad1->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('0')->addCondition('14');
                    $FactorActividad1->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "9be5f7"]]]);

                    $FactorActividad2 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $FactorActividad2->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $FactorActividad2->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('15')->addCondition('29');
                    $FactorActividad2->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "6bf56e"]]]);

                    $FactorActividad3 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $FactorActividad3->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $FactorActividad3->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('30')->addCondition('44');
                    $FactorActividad3->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffff00"]]]);

                    $FactorActividad4 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $FactorActividad4->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $FactorActividad4->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('45')->addCondition('59');
                    $FactorActividad4->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffc000"]]]);

                    $FactorActividad5 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $FactorActividad5->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $FactorActividad5->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_GREATERTHANOREQUAL)
                                        ->addCondition('60');
                    $FactorActividad5->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ff0000"]]]);

                    ## condiciones de colores para la categoria de Organización del tiempo de trabajo
                    $TiempoTrabajo1 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $TiempoTrabajo1->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $TiempoTrabajo1->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('0')->addCondition('4');
                    $TiempoTrabajo1->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "9be5f7"]]]);

                    $TiempoTrabajo2 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $TiempoTrabajo2->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $TiempoTrabajo2->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('5')->addCondition('6');
                    $TiempoTrabajo2->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "6bf56e"]]]);

                    $TiempoTrabajo3 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $TiempoTrabajo3->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $TiempoTrabajo3->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('7')->addCondition('9');
                    $TiempoTrabajo3->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffff00"]]]);

                    $TiempoTrabajo4 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $TiempoTrabajo4->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $TiempoTrabajo4->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('10')->addCondition('12');
                    $TiempoTrabajo4->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffc000"]]]);

                    $TiempoTrabajo5 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $TiempoTrabajo5->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $TiempoTrabajo5->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_GREATERTHANOREQUAL)
                                        ->addCondition('13');
                    $TiempoTrabajo5->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ff0000"]]]);

                    ## condiciones de colores para la categoria de Liderazgo y relaciones en el trabajo
                    $LiderRelacinTrabajo1 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $LiderRelacinTrabajo1->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $LiderRelacinTrabajo1->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('0')->addCondition('13');
                    $LiderRelacinTrabajo1->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "9be5f7"]]]);

                    $LiderRelacinTrabajo2 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $LiderRelacinTrabajo2->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $LiderRelacinTrabajo2->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('14')->addCondition('28');
                    $LiderRelacinTrabajo2->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "6bf56e"]]]);

                    $LiderRelacinTrabajo3 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $LiderRelacinTrabajo3->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $LiderRelacinTrabajo3->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('29')->addCondition('41');
                    $LiderRelacinTrabajo3->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffff00"]]]);

                    $LiderRelacinTrabajo4 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $LiderRelacinTrabajo4->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $LiderRelacinTrabajo4->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('42')->addCondition('57');
                    $LiderRelacinTrabajo4->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffc000"]]]);

                    $LiderRelacinTrabajo5 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $LiderRelacinTrabajo5->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $LiderRelacinTrabajo5->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_GREATERTHANOREQUAL)
                                        ->addCondition('58');
                    $LiderRelacinTrabajo5->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ff0000"]]]);

                    ## condiciones de colores para la categoria de Entorno organizacional
                    $EntornoOrg1 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $EntornoOrg1->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $EntornoOrg1->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('0')->addCondition('9');
                    $EntornoOrg1->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "9be5f7"]]]);

                    $EntornoOrg2 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $EntornoOrg2->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $EntornoOrg2->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('10')->addCondition('13');
                    $EntornoOrg2->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "6bf56e"]]]);

                    $EntornoOrg3 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $EntornoOrg3->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $EntornoOrg3->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('14')->addCondition('17');
                    $EntornoOrg3->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffff00"]]]);

                    $EntornoOrg4 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $EntornoOrg4->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $EntornoOrg4->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('18')->addCondition('22');
                    $EntornoOrg4->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffc000"]]]);

                    $EntornoOrg5 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $EntornoOrg5->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $EntornoOrg5->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_GREATERTHANOREQUAL)
                                        ->addCondition('23');
                    $EntornoOrg5->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ff0000"]]]);


                    if ($letterVal === 'C') {
                        $conditionalStyles = $spreadsheet->getActiveSheet()->getStyle($letterVal."$row")->getConditionalStyles();
                        $conditionalStyles[] = $CatAmbienteTrabajo1;
                        $conditionalStyles[] = $CatAmbienteTrabajo2;
                        $conditionalStyles[] = $CatAmbienteTrabajo3;
                        $conditionalStyles[] = $CatAmbienteTrabajo4;
                        $conditionalStyles[] = $CatAmbienteTrabajo5;
                        $spreadsheet->getActiveSheet()->getStyle($letterVal."$row")->setConditionalStyles($conditionalStyles);
                    } else if ($letterVal === 'D'){
                        $conditionalStyles = $spreadsheet->getActiveSheet()->getStyle($letterVal."$row")->getConditionalStyles();
                        $conditionalStyles[] = $FactorActividad1;
                        $conditionalStyles[] = $FactorActividad2;
                        $conditionalStyles[] = $FactorActividad3;
                        $conditionalStyles[] = $FactorActividad4;
                        $conditionalStyles[] = $FactorActividad5;
                        $spreadsheet->getActiveSheet()->getStyle($letterVal."$row")->setConditionalStyles($conditionalStyles);
                    } else if ($letterVal === 'E'){
                        $conditionalStyles = $spreadsheet->getActiveSheet()->getStyle($letterVal."$row")->getConditionalStyles();
                        $conditionalStyles[] = $TiempoTrabajo1;
                        $conditionalStyles[] = $TiempoTrabajo2;
                        $conditionalStyles[] = $TiempoTrabajo3;
                        $conditionalStyles[] = $TiempoTrabajo4;
                        $conditionalStyles[] = $TiempoTrabajo5;
                        $spreadsheet->getActiveSheet()->getStyle($letterVal."$row")->setConditionalStyles($conditionalStyles);
                    } else if ($letterVal === 'F'){
                        $conditionalStyles = $spreadsheet->getActiveSheet()->getStyle($letterVal."$row")->getConditionalStyles();
                        $conditionalStyles[] = $LiderRelacinTrabajo1;
                        $conditionalStyles[] = $LiderRelacinTrabajo2;
                        $conditionalStyles[] = $LiderRelacinTrabajo3;
                        $conditionalStyles[] = $LiderRelacinTrabajo4;
                        $conditionalStyles[] = $LiderRelacinTrabajo5;
                        $spreadsheet->getActiveSheet()->getStyle($letterVal."$row")->setConditionalStyles($conditionalStyles);
                    } else if ($letterVal === 'G'){
                        $conditionalStyles = $spreadsheet->getActiveSheet()->getStyle($letterVal."$row")->getConditionalStyles();
                        $conditionalStyles[] = $EntornoOrg1;
                        $conditionalStyles[] = $EntornoOrg2;
                        $conditionalStyles[] = $EntornoOrg3;
                        $conditionalStyles[] = $EntornoOrg4;
                        $conditionalStyles[] = $EntornoOrg5;
                        $spreadsheet->getActiveSheet()->getStyle($letterVal."$row")->setConditionalStyles($conditionalStyles);
                    }
                    unset($conditionalStyles);
                    $letterVal++;
                }
                //DOMAINS
                $domains = Domains::select(\DB::raw('IFNULL(sum(ifnull(`a`.`Value`, 0)),0) AS Sum'))
                            ->join('nom_det_dimensions_join_domains AS dd', 'dd.Fk_IdDomains','Id_Domains')
                            ->join('nom_det_dimensions_join_question AS dq', 'dq.Fk_IdDimension','dd.Fk_IdDimencions')
                            ->leftjoin('sur_his_persons as p', 'p.Fk_IdSurvey','dd.Fk_IdSurvey')
                            ->leftjoin('sur_det_person_join_answer as pa', function($join){
                                    $join->on('pa.Fk_IdQuestion','dq.Fk_IdQuestion')
                                         ->on('pa.Fk_IdPerson','Id_Person')
                                         ->where('pa.Delete','0');
                                  })
                            ->leftjoin('sur_cat_answers AS a', 'a.Id_Answer','pa.Fk_IdAnswer')
                            ->where('Id_Person',$v['Id_Person'])
                            ->groupBy('Id_Domains')
                            ->orderBy('Id_Domains')
                            ->get();
                foreach ($domains as $key => $val) {

                    $spreadsheet->getActiveSheet()
                        ->setCellValue($letterVal."$row", $val['Sum']);

                    ## condiciones para el dominio de condiciones de ambiente de trabajo
                    $ConAmbienteTrabajo1 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $ConAmbienteTrabajo1->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $ConAmbienteTrabajo1->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('0')->addCondition('4');
                    $ConAmbienteTrabajo1->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "9be5f7"]]]);

                    $ConAmbienteTrabajo2 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $ConAmbienteTrabajo2->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $ConAmbienteTrabajo2->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('5')->addCondition('8');
                    $ConAmbienteTrabajo2->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "6bf56e"]]]);

                    $ConAmbienteTrabajo3 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $ConAmbienteTrabajo3->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $ConAmbienteTrabajo3->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('9')->addCondition('10');
                    $ConAmbienteTrabajo3->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffff00"]]]);

                    $ConAmbienteTrabajo4 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $ConAmbienteTrabajo4->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $ConAmbienteTrabajo4->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('11')->addCondition('13');
                    $ConAmbienteTrabajo4->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffc000"]]]);

                    $ConAmbienteTrabajo5 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $ConAmbienteTrabajo5->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $ConAmbienteTrabajo5->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_GREATERTHANOREQUAL)
                                        ->addCondition('14');
                    $ConAmbienteTrabajo5->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ff0000"]]]);

                    ## condiciones de carga de trabajo
                    $CargaTrabajo1 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $CargaTrabajo1->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $CargaTrabajo1->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('0')->addCondition('14');
                    $CargaTrabajo1->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "9be5f7"]]]);

                    $CargaTrabajo2 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $CargaTrabajo2->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $CargaTrabajo2->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('15')->addCondition('20');
                    $CargaTrabajo2->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "6bf56e"]]]);

                    $CargaTrabajo3 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $CargaTrabajo3->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $CargaTrabajo3->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('9')->addCondition('10');
                    $CargaTrabajo3->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffff00"]]]);

                    $CargaTrabajo4 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $CargaTrabajo4->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $CargaTrabajo4->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('21')->addCondition('36');
                    $CargaTrabajo4->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffc000"]]]);

                    $CargaTrabajo5 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $CargaTrabajo5->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $CargaTrabajo5->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_GREATERTHANOREQUAL)
                                        ->addCondition('37');
                    $CargaTrabajo5->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ff0000"]]]);

                    ### Falta de control sobre el trabajo

                    $FaltaCtrlTrabajo1 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $FaltaCtrlTrabajo1->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $FaltaCtrlTrabajo1->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('0')->addCondition('10');
                    $FaltaCtrlTrabajo1->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "9be5f7"]]]);

                    $FaltaCtrlTrabajo2 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $FaltaCtrlTrabajo2->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $FaltaCtrlTrabajo2->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('11')->addCondition('15');
                    $FaltaCtrlTrabajo2->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "6bf56e"]]]);

                    $FaltaCtrlTrabajo3 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $FaltaCtrlTrabajo3->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $FaltaCtrlTrabajo3->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('16')->addCondition('20');
                    $FaltaCtrlTrabajo3->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffff00"]]]);

                    $FaltaCtrlTrabajo4 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $FaltaCtrlTrabajo4->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $FaltaCtrlTrabajo4->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('21')->addCondition('24');
                    $FaltaCtrlTrabajo4->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffc000"]]]);

                    $FaltaCtrlTrabajo5 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $FaltaCtrlTrabajo5->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $FaltaCtrlTrabajo5->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_GREATERTHANOREQUAL)
                                        ->addCondition('15');
                    $FaltaCtrlTrabajo5->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ff0000"]]]);

                    #### Jornada de trabajo
                    $JornadaTrabajo1 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $JornadaTrabajo1->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $JornadaTrabajo1->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('0')->addCondition('0');
                    $JornadaTrabajo1->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "9be5f7"]]]);

                    $JornadaTrabajo2 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $JornadaTrabajo2->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $JornadaTrabajo2->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('1')->addCondition('1');
                    $JornadaTrabajo2->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "6bf56e"]]]);

                    $JornadaTrabajo3 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $JornadaTrabajo3->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $JornadaTrabajo3->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('2')->addCondition('3');
                    $JornadaTrabajo3->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffff00"]]]);

                    $JornadaTrabajo4 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $JornadaTrabajo4->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $JornadaTrabajo4->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('4')->addCondition('5');
                    $JornadaTrabajo4->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffc000"]]]);

                    $JornadaTrabajo5 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $JornadaTrabajo5->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $JornadaTrabajo5->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_GREATERTHANOREQUAL)
                                        ->addCondition('6');
                    $JornadaTrabajo5->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ff0000"]]]);

                    ### Interferencia en el trabajo relacion familia

                    $TrabajoRelacionFam1 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $TrabajoRelacionFam1->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $TrabajoRelacionFam1->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('0')->addCondition('3');
                    $TrabajoRelacionFam1->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "9be5f7"]]]);

                    $TrabajoRelacionFam2 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $TrabajoRelacionFam2->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $TrabajoRelacionFam2->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('4')->addCondition('5');
                    $TrabajoRelacionFam2->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "6bf56e"]]]);

                    $TrabajoRelacionFam3 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $TrabajoRelacionFam3->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $TrabajoRelacionFam3->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('6')->addCondition('7');
                    $TrabajoRelacionFam3->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffff00"]]]);

                    $TrabajoRelacionFam4 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $TrabajoRelacionFam4->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $TrabajoRelacionFam4->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('7')->addCondition('9');
                    $TrabajoRelacionFam4->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffc000"]]]);

                    $TrabajoRelacionFam5 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $TrabajoRelacionFam5->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $TrabajoRelacionFam5->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_GREATERTHANOREQUAL)
                                        ->addCondition('10');
                    $TrabajoRelacionFam5->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ff0000"]]]);

                    #Lidereazgo
                    $Liderazgo1 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $Liderazgo1->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $Liderazgo1->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('0')->addCondition('8');
                    $Liderazgo1->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "9be5f7"]]]);

                    $Liderazgo2 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $Liderazgo2->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $Liderazgo2->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('9')->addCondition('11');
                    $Liderazgo2->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "6bf56e"]]]);

                    $Liderazgo3 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $Liderazgo3->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $Liderazgo3->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('12')->addCondition('15');
                    $Liderazgo3->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffff00"]]]);

                    $Liderazgo4 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $Liderazgo4->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $Liderazgo4->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('16')->addCondition('19');
                    $Liderazgo4->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffc000"]]]);

                    $Liderazgo5 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $Liderazgo5->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $Liderazgo5->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_GREATERTHANOREQUAL)
                                        ->addCondition('20');
                    $Liderazgo5->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ff0000"]]]);

                    ### Relaciones en el trabajo
                    $RelacionesTrabajo1 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $RelacionesTrabajo1->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $RelacionesTrabajo1->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('0')->addCondition('9');
                    $RelacionesTrabajo1->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "9be5f7"]]]);

                    $RelacionesTrabajo2 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $RelacionesTrabajo2->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $RelacionesTrabajo2->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('10')->addCondition('12');
                    $RelacionesTrabajo2->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "6bf56e"]]]);

                    $RelacionesTrabajo3 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $RelacionesTrabajo3->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $RelacionesTrabajo3->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('13')->addCondition('16');
                    $RelacionesTrabajo3->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffff00"]]]);

                    $RelacionesTrabajo4 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $RelacionesTrabajo4->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $RelacionesTrabajo4->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('17')->addCondition('20');
                    $RelacionesTrabajo4->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffc000"]]]);

                    $RelacionesTrabajo5 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $RelacionesTrabajo5->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $RelacionesTrabajo5->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_GREATERTHANOREQUAL)
                                        ->addCondition('21');
                    $RelacionesTrabajo5->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ff0000"]]]);

                    ## violencia

                    $Violencia1 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $Violencia1->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $Violencia1->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('0')->addCondition('6');
                    $Violencia1->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "9be5f7"]]]);

                    $Violencia2 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $Violencia2->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $Violencia2->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('7')->addCondition('9');
                    $Violencia2->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "6bf56e"]]]);

                    $Violencia3 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $Violencia3->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $Violencia3->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('10')->addCondition('12');
                    $Violencia3->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffff00"]]]);

                    $Violencia4 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $Violencia4->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $Violencia4->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('13')->addCondition('15');
                    $Violencia4->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffc000"]]]);

                    $Violencia5 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $Violencia5->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $Violencia5->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_GREATERTHANOREQUAL)
                                        ->addCondition('16');
                    $Violencia5->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ff0000"]]]);


                    #Reconocimiento de desempeño
                    $ReconDesempeno1 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $ReconDesempeno1->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $ReconDesempeno1->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('0')->addCondition('5');
                    $ReconDesempeno1->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "9be5f7"]]]);

                    $ReconDesempeno2 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $ReconDesempeno2->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $ReconDesempeno2->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('6')->addCondition('9');
                    $ReconDesempeno2->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "6bf56e"]]]);

                    $ReconDesempeno3 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $ReconDesempeno3->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $ReconDesempeno3->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('10')->addCondition('13');
                    $ReconDesempeno3->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffff00"]]]);

                    $ReconDesempeno4 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $ReconDesempeno4->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $ReconDesempeno4->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('14')->addCondition('17');
                    $ReconDesempeno4->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffc000"]]]);

                    $ReconDesempeno5 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $ReconDesempeno5->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $ReconDesempeno5->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_GREATERTHANOREQUAL)
                                        ->addCondition('18');
                    $ReconDesempeno5->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ff0000"]]]);

                    #Insuficiente sentido de pertenencia e inestabilidad
                    $PertenenciaInestabilidad1 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $PertenenciaInestabilidad1->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $PertenenciaInestabilidad1->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('0')->addCondition('3');
                    $PertenenciaInestabilidad1->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "9be5f7"]]]);

                    $PertenenciaInestabilidad2 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $PertenenciaInestabilidad2->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $PertenenciaInestabilidad2->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('4')->addCondition('5');
                    $PertenenciaInestabilidad2->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "6bf56e"]]]);

                    $PertenenciaInestabilidad3 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $PertenenciaInestabilidad3->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $PertenenciaInestabilidad3->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('6')->addCondition('7');
                    $PertenenciaInestabilidad3->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffff00"]]]);

                    $PertenenciaInestabilidad4 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $PertenenciaInestabilidad4->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $PertenenciaInestabilidad4->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('8')->addCondition('9');
                    $PertenenciaInestabilidad4->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffc000"]]]);

                    $PertenenciaInestabilidad5 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $PertenenciaInestabilidad5->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $PertenenciaInestabilidad5->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_GREATERTHANOREQUAL)
                                        ->addCondition('10');
                    $PertenenciaInestabilidad5->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ff0000"]]]);


                    if ($letterVal === 'H') {
                        $conditionalStyles = $spreadsheet->getActiveSheet()->getStyle($letterVal."$row")->getConditionalStyles();
                        $conditionalStyles[] = $ConAmbienteTrabajo1;
                        $conditionalStyles[] = $ConAmbienteTrabajo2;
                        $conditionalStyles[] = $ConAmbienteTrabajo3;
                        $conditionalStyles[] = $ConAmbienteTrabajo4;
                        $conditionalStyles[] = $ConAmbienteTrabajo5;
                        $spreadsheet->getActiveSheet()->getStyle($letterVal."$row")->setConditionalStyles($conditionalStyles);
                    } else if ($letterVal === 'I'){
                        $conditionalStyles = $spreadsheet->getActiveSheet()->getStyle($letterVal."$row")->getConditionalStyles();
                        $conditionalStyles[] = $CargaTrabajo1;
                        $conditionalStyles[] = $CargaTrabajo2;
                        $conditionalStyles[] = $CargaTrabajo3;
                        $conditionalStyles[] = $CargaTrabajo4;
                        $conditionalStyles[] = $CargaTrabajo5;
                        $spreadsheet->getActiveSheet()->getStyle($letterVal."$row")->setConditionalStyles($conditionalStyles);
                    } else if ($letterVal === 'J'){
                        $conditionalStyles = $spreadsheet->getActiveSheet()->getStyle($letterVal."$row")->getConditionalStyles();
                        $conditionalStyles[] = $FaltaCtrlTrabajo1;
                        $conditionalStyles[] = $FaltaCtrlTrabajo2;
                        $conditionalStyles[] = $FaltaCtrlTrabajo3;
                        $conditionalStyles[] = $FaltaCtrlTrabajo4;
                        $conditionalStyles[] = $FaltaCtrlTrabajo5;
                        $spreadsheet->getActiveSheet()->getStyle($letterVal."$row")->setConditionalStyles($conditionalStyles);
                    } else if ($letterVal === 'K'){
                        $conditionalStyles = $spreadsheet->getActiveSheet()->getStyle($letterVal."$row")->getConditionalStyles();
                        $conditionalStyles[] = $JornadaTrabajo1;
                        $conditionalStyles[] = $JornadaTrabajo2;
                        $conditionalStyles[] = $JornadaTrabajo3;
                        $conditionalStyles[] = $JornadaTrabajo4;
                        $conditionalStyles[] = $JornadaTrabajo5;
                        $spreadsheet->getActiveSheet()->getStyle($letterVal."$row")->setConditionalStyles($conditionalStyles);
                    } else if ($letterVal === 'L'){
                        $conditionalStyles = $spreadsheet->getActiveSheet()->getStyle($letterVal."$row")->getConditionalStyles();
                        $conditionalStyles[] = $TrabajoRelacionFam1;
                        $conditionalStyles[] = $TrabajoRelacionFam2;
                        $conditionalStyles[] = $TrabajoRelacionFam3;
                        $conditionalStyles[] = $TrabajoRelacionFam4;
                        $conditionalStyles[] = $TrabajoRelacionFam5;
                        $spreadsheet->getActiveSheet()->getStyle($letterVal."$row")->setConditionalStyles($conditionalStyles);
                    } else if ($letterVal === 'M'){
                        $conditionalStyles = $spreadsheet->getActiveSheet()->getStyle($letterVal."$row")->getConditionalStyles();
                        $conditionalStyles[] = $Liderazgo1;
                        $conditionalStyles[] = $Liderazgo2;
                        $conditionalStyles[] = $Liderazgo3;
                        $conditionalStyles[] = $Liderazgo4;
                        $conditionalStyles[] = $Liderazgo5;
                        $spreadsheet->getActiveSheet()->getStyle($letterVal."$row")->setConditionalStyles($conditionalStyles);
                    } else if ($letterVal === 'N'){
                        $conditionalStyles = $spreadsheet->getActiveSheet()->getStyle($letterVal."$row")->getConditionalStyles();
                        $conditionalStyles[] = $RelacionesTrabajo1;
                        $conditionalStyles[] = $RelacionesTrabajo2;
                        $conditionalStyles[] = $RelacionesTrabajo3;
                        $conditionalStyles[] = $RelacionesTrabajo4;
                        $conditionalStyles[] = $RelacionesTrabajo5;
                        $spreadsheet->getActiveSheet()->getStyle($letterVal."$row")->setConditionalStyles($conditionalStyles);
                    } else if ($letterVal === 'O'){
                        $conditionalStyles = $spreadsheet->getActiveSheet()->getStyle($letterVal."$row")->getConditionalStyles();
                        $conditionalStyles[] = $Violencia1;
                        $conditionalStyles[] = $Violencia2;
                        $conditionalStyles[] = $Violencia3;
                        $conditionalStyles[] = $Violencia4;
                        $conditionalStyles[] = $Violencia5;
                        $spreadsheet->getActiveSheet()->getStyle($letterVal."$row")->setConditionalStyles($conditionalStyles);
                    } else if ($letterVal === 'P'){
                        $conditionalStyles = $spreadsheet->getActiveSheet()->getStyle($letterVal."$row")->getConditionalStyles();
                        $conditionalStyles[] = $ReconDesempeno1;
                        $conditionalStyles[] = $ReconDesempeno2;
                        $conditionalStyles[] = $ReconDesempeno3;
                        $conditionalStyles[] = $ReconDesempeno4;
                        $conditionalStyles[] = $ReconDesempeno5;
                        $spreadsheet->getActiveSheet()->getStyle($letterVal."$row")->setConditionalStyles($conditionalStyles);
                    } else if ($letterVal === 'Q'){
                        $conditionalStyles = $spreadsheet->getActiveSheet()->getStyle($letterVal."$row")->getConditionalStyles();
                        $conditionalStyles[] = $PertenenciaInestabilidad1;
                        $conditionalStyles[] = $PertenenciaInestabilidad2;
                        $conditionalStyles[] = $PertenenciaInestabilidad3;
                        $conditionalStyles[] = $PertenenciaInestabilidad4;
                        $conditionalStyles[] = $PertenenciaInestabilidad5;
                        $spreadsheet->getActiveSheet()->getStyle($letterVal."$row")->setConditionalStyles($conditionalStyles);
                    }
                    unset($conditionalStyles);
                    $letterVal++;
                }
                $spreadsheet->getActiveSheet()
                    ->setCellValue("R$row","=SUM(H$row:Q$row)");

                $conditionalStyles = $spreadsheet->getActiveSheet()->getStyle("R$row")->getConditionalStyles();
                $conditionalStyles[] = $Totales1;
                $conditionalStyles[] = $Totales2;
                $conditionalStyles[] = $Totales3;
                $conditionalStyles[] = $Totales4;
                $conditionalStyles[] = $Totales5;
                $spreadsheet->getActiveSheet()->getStyle("R$row")->setConditionalStyles($conditionalStyles);

                $row++;
                $letterVal = 'A';

            }
            ##

        #########################################################################################################
        #####################################  hoja 3 resumen por dimensiones ###################################
        #########################################################################################################
            $spreadsheet->createSheet();

            $spreadsheet->setActiveSheetIndex(2);
            $spreadsheet->getActiveSheet()->setTitle('Resumen de dimensiones');
            #$spreadsheet->getActiveSheet()->getTabColor()->setARGB('465D99');

            $objDrawing = new drawing();
            $objDrawing->setName('TrabajoSinStress');
            $objDrawing->setDescription('TrabajoSinStress');
            $objDrawing->setPath('logo-sin-estres.jpg');
            $objDrawing->setHeight(53);
            $objDrawing->setWorksheet($spreadsheet->getActiveSheet());
            $objWriter = io_factory::createWriter($spreadsheet, 'Xlsx');

            $sheet = $spreadsheet->getActiveSheet();
            $spreadsheet->getActiveSheet()
                        ->getRowDimension('1')
                        ->setRowHeight(50);

            $spreadsheet->getActiveSheet()
                        ->setCellValue("C1", strtoupper('Ambiente de Trabajo/Condiciones en el ambiente de trabajo'))
                        ->mergeCells('C1:E1');
            $spreadsheet->getActiveSheet()
                ->getStyle('C1')->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('eeece1');


            $spreadsheet->getActiveSheet()
                        ->setCellValue("F1", strtoupper('Factores propios de la actividad/Carga de trabajo/Falta de control sobre el trabajo'))
                        ->mergeCells('F1:O1');
            $spreadsheet->getActiveSheet()
                ->getStyle('F1')->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('c0504d');


            $spreadsheet->getActiveSheet()
                        ->setCellValue("P1", strtoupper('Organización del tiempo de trabajo/Jornada de trabajo/Interferencia en la relación trabajo-familia'))
                        ->mergeCells('P1:R1');
            $spreadsheet->getActiveSheet()
                ->getStyle('P1')->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('4f81bd');


            $spreadsheet->getActiveSheet()
                        ->setCellValue("S1", strtoupper('Liderazgo y relaciones en el trabajo/Liderazgo/Relaciones en el trabajo/Violencia'))
                        ->mergeCells('S1:W1');
            $spreadsheet->getActiveSheet()
                ->getStyle('S1')->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('fabf8f');


            $spreadsheet->getActiveSheet()
                        ->setCellValue("X1", strtoupper('Entorno organizacional/Reconocimiento del desempeño/Insuficiente sentido de pertenencia e, inestabilidad'))
                        ->mergeCells('X1:AA1');
            $spreadsheet->getActiveSheet()
                ->getStyle('X1')->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('c4d79b');

            $word='A';
            $lastLetter = 'A';
            $i = 2;
            $Array[$word]   = ['title'=>'NOMINA','color'=>'002060', 'size'=> 13];
            $Array[++$word] = ['title'=>'NOMBRE','color'=>'002060', 'size'=> 40];
            $Array[++$word] = ['title'=>'Condiciones peligrosas e inseguras','color'=>'eeece1', 'text-rotation'=>90];
            $Array[++$word] = ['title'=>'Condiciones deficientes e insalubres','color' =>'eeece1', 'text-rotation'=>90];
            $Array[++$word] = ['title'=>'Trabajos peligrosos','color'=>'eeece1', 'text-rotation'=>90];

            $Array[++$word] = ['title'=>'Cargas cuantitativas','color'=>'c0504d', 'text-rotation'=>90];
            $Array[++$word] = ['title'=>'Ritmos de trabajo acelerado','color'=>'c0504d', 'text-rotation'=>90];
            $Array[++$word] = ['title'=>'Carga mental','color'=>'c0504d', 'text-rotation'=>90];
            $Array[++$word] = ['title'=>'Cargas psicológicas emocionales','color'=>'c0504d', 'text-rotation'=>90];
            $Array[++$word] = ['title'=>'Cargas de alta responsabilidad','color'=>'c0504d', 'text-rotation'=>90];
            $Array[++$word] = ['title'=>'Cargas contradictorias o inconsistentes','color'=>'c0504d', 'text-rotation'=>90];
            $Array[++$word] = ['title'=>'Falta de control y autonomía sobre el trabajo','color'=>'c0504d', 'text-rotation'=>90];
            $Array[++$word] = ['title'=>'Limitada o nula posibilidad de desarrollo','color'=>'c0504d', 'text-rotation'=>90];
            $Array[++$word] = ['title'=>'Insuficiente participación y manejo del cambio','color'=>'c0504d', 'text-rotation'=>90];
            $Array[++$word] = ['title'=>'Limitada o inexistente capacitación','color'=>'c0504d', 'text-rotation'=>90];

            $Array[++$word] = ['title'=>'Jornadas de trabajo extensas','color'=>'4f81bd', 'text-rotation'=>90];
            $Array[++$word] = ['title'=>'Influencia del trabajo fuera del centro laboral','color'=>'4f81bd', 'text-rotation'=>90];
            $Array[++$word] = ['title'=>'Influencia de las responsabilidades familiares ','color'=>'4f81bd', 'text-rotation'=>90];

            $Array[++$word] = ['title'=>'Escaza claridad de funciones','color'=>'fabf8f', 'text-rotation'=>90];
            $Array[++$word] = ['title'=>'Características del liderazgo','color'=>'fabf8f', 'text-rotation'=>90];
            $Array[++$word] = ['title'=>'Relaciones sociales en el trabajo','color'=>'fabf8f', 'text-rotation'=>90];
            $Array[++$word] = ['title'=>'Deficiente relación con los colaboradores que supervisa','color'=>'fabf8f', 'text-rotation'=>90];
            $Array[++$word] = ['title'=>'Violencia laboral','color'=>'fabf8f', 'text-rotation'=>90];

            $Array[++$word] = ['title'=>'Escasa o nula retroalimentación del desempeño','color'=>'c4d79b', 'text-rotation'=>90];
            $Array[++$word] = ['title'=>'Escaso o nulo reconocimiento y compensación','color'=>'c4d79b', 'text-rotation'=>90];
            $Array[++$word] = ['title'=>'Limitado sentido de pertenencia','color'=>'c4d79b', 'text-rotation'=>90];
            $Array[++$word] = ['title'=>'Inestabilidad laboral','color'=>'c4d79b', 'text-rotation'=>90];

            $Array[++$word] = ['title'=>'CALIFICACIÓN GENERAL','color'=>'FFFF00', 'size'=> 30];

            foreach ($Array as $k => $val) {
                if(!isset($val['size'])) $val['size']=11;
                $spreadsheet->getActiveSheet()
                        ->setCellValue($k."$i", $val['title']);

                $spreadsheet->getActiveSheet()
                        ->getColumnDimension($k)->setWidth($val['size']);

                $spreadsheet->getActiveSheet()
                            ->getStyle($k)
                            ->getAlignment()
                            ->setWrapText(true);

                $spreadsheet->getActiveSheet()
                            ->getStyle($k."$i")->getFill()
                            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                            ->getStartColor()->setARGB('00aae8');

                if(isset($val['color']) && $val['color']){
                    $spreadsheet->getActiveSheet()
                            ->getStyle($k."$i")->getFill()
                            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                            ->getStartColor()->setARGB($val['color']);
                }
                if(isset($val['text-rotation']) && $val['text-rotation']){

                    $spreadsheet->getActiveSheet()
                                ->getStyle($k."$i")
                                ->getAlignment()
                                ->applyFromArray( [
                                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                                    'textRotation' => $val['text-rotation'],
                                    'wrapText' => TRUE ] );

                    $spreadsheet->getActiveSheet()
                                ->getRowDimension('2')
                                ->setRowHeight(110);
                }
            }

            $styleArray = array(
                'font' => array(
                    'bold' => true,
                    'size' => 11
                ),
                'alignment' => array(
                    'wrapText' => TRUE,
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                )
            );

            $styleArray2 = array(
                'font' => array(
                    'size' => 9
                )
            );
            $spreadsheet->getActiveSheet()->getStyle('A1:AB2')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A1:AB1')->applyFromArray($styleArray2);
            $spreadsheet->getActiveSheet()->getStyle('A2:B2')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
            //dejamos fija la fila de titulos
            $spreadsheet->getActiveSheet()->freezePane('A3');
            //aplicamos los bordes
            $spreadsheet->getActiveSheet()
                        ->getStyle("A1:AB$lastRow")
                        ->getBorders()
                        ->applyFromArray( [
                                'allBorders' => [
                                    'borderStyle' => Border::BORDER_THIN,
                                    'color' => ['rgb' => '000000' ]
                                ]
                            ]
                        );
            ######### traemos los datos de la db
            $ResultCatDom = $ResultPersons;
            $letterVal = 'A';
            $row = 3;
            foreach($ResultCatDom as $k => $v){
                $spreadsheet->getActiveSheet()
                        ->setCellValue($letterVal."$row", $v['Nomina']);
                $letterVal++;
                $spreadsheet->getActiveSheet()
                        ->setCellValue($letterVal."$row", $v['Nombre']);
                $letterVal++;

                //CATEGORIES
                $categories = Persons::select(\DB::raw('IFNULL(sum(ifnull(`a`.`Value`, 0)),0) AS Sum'))
                            ->join('sur_det_person_join_answer as pa', 'Id_Person','pa.Fk_IdPerson')
                            ->join('sur_cat_answers as a', 'a.Id_Answer','pa.Fk_IdAnswer')
                            ->join('nom_det_dimensions_join_question as dq', 'pa.Fk_IdQuestion','dq.Fk_IdQuestion')
                            ->join('nom_det_dimensions_join_domains as dd', function($join){
                                    $join->on('dd.Fk_IdDimencions','dq.Fk_IdDimension')
                                         ->on('dd.Fk_IdSurvey','sur_his_persons.Fk_IdSurvey');
                                  })
                            ->where('Id_Person',$v['Id_Person'])
                            ->groupBy('Fk_IdCategory')
                            ->orderBy('Fk_IdCategory')
                            ->get();

                foreach ($categories as $key => $val) {
                    #$spreadsheet->getActiveSheet()
                    #    ->setCellValue($letterVal."$row", $val['Sum']);

                    if ($letterVal=='C') {
                        for ($i=1; $i<=3 ; $i++) {
                            $spreadsheet->getActiveSheet()
                                ->setCellValue($letterVal."$row", $val['Sum']);
                            if($i<3)
                                $letterVal++;
                        }
                    } else if ($letterVal=='F') {
                        for ($i=1; $i<=10 ; $i++) {
                            $spreadsheet->getActiveSheet()
                                ->setCellValue($letterVal."$row", $val['Sum']);
                            if($i<10)
                                $letterVal++;
                        }
                    } else if ($letterVal=='P') {
                        for ($i=1; $i<=3 ; $i++) {
                            $spreadsheet->getActiveSheet()
                                ->setCellValue($letterVal."$row", $val['Sum']);
                            if($i<3)
                                $letterVal++;
                        }
                    } else if ($letterVal=='S') {
                        for ($i=1; $i<=5 ; $i++) {
                            $spreadsheet->getActiveSheet()
                                ->setCellValue($letterVal."$row", $val['Sum']);
                            if($i<5)
                                $letterVal++;
                        }
                    } else if ($letterVal=='X') {
                        for ($i=1; $i<=4 ; $i++) {
                            $spreadsheet->getActiveSheet()
                                ->setCellValue($letterVal."$row", $val['Sum']);
                            if($i<4)
                                $letterVal++;
                        }
                    }
                    ## condiciones de colores para la categoria de ambiente de trabajo
                    $CatAmbienteTrabajo1 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $CatAmbienteTrabajo1->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $CatAmbienteTrabajo1->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('0')->addCondition('4');
                    $CatAmbienteTrabajo1->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "9be5f7"]]]);

                    $CatAmbienteTrabajo2 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $CatAmbienteTrabajo2->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $CatAmbienteTrabajo2->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('5')->addCondition('8');
                    $CatAmbienteTrabajo2->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "6bf56e"]]]);

                    $CatAmbienteTrabajo3 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $CatAmbienteTrabajo3->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $CatAmbienteTrabajo3->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('9')->addCondition('10');
                    $CatAmbienteTrabajo3->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffff00"]]]);

                    $CatAmbienteTrabajo4 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $CatAmbienteTrabajo4->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $CatAmbienteTrabajo4->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('11')->addCondition('13');
                    $CatAmbienteTrabajo4->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffc000"]]]);

                    $CatAmbienteTrabajo5 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $CatAmbienteTrabajo5->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $CatAmbienteTrabajo5->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_GREATERTHANOREQUAL)
                                        ->addCondition('14');
                    $CatAmbienteTrabajo5->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ff0000"]]]);

                    ## condiciones de colores para la categoria de Factores propios de la actividad
                    $FactorActividad1 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $FactorActividad1->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $FactorActividad1->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('0')->addCondition('14');
                    $FactorActividad1->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "9be5f7"]]]);

                    $FactorActividad2 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $FactorActividad2->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $FactorActividad2->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('15')->addCondition('29');
                    $FactorActividad2->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "6bf56e"]]]);

                    $FactorActividad3 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $FactorActividad3->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $FactorActividad3->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('30')->addCondition('44');
                    $FactorActividad3->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffff00"]]]);

                    $FactorActividad4 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $FactorActividad4->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $FactorActividad4->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('45')->addCondition('59');
                    $FactorActividad4->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffc000"]]]);

                    $FactorActividad5 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $FactorActividad5->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $FactorActividad5->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_GREATERTHANOREQUAL)
                                        ->addCondition('60');
                    $FactorActividad5->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ff0000"]]]);

                    ## condiciones de colores para la categoria de Organización del tiempo de trabajo
                    $TiempoTrabajo1 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $TiempoTrabajo1->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $TiempoTrabajo1->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('0')->addCondition('4');
                    $TiempoTrabajo1->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "9be5f7"]]]);

                    $TiempoTrabajo2 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $TiempoTrabajo2->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $TiempoTrabajo2->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('5')->addCondition('6');
                    $TiempoTrabajo2->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "6bf56e"]]]);

                    $TiempoTrabajo3 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $TiempoTrabajo3->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $TiempoTrabajo3->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('7')->addCondition('9');
                    $TiempoTrabajo3->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffff00"]]]);

                    $TiempoTrabajo4 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $TiempoTrabajo4->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $TiempoTrabajo4->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('10')->addCondition('12');
                    $TiempoTrabajo4->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffc000"]]]);

                    $TiempoTrabajo5 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $TiempoTrabajo5->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $TiempoTrabajo5->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_GREATERTHANOREQUAL)
                                        ->addCondition('13');
                    $TiempoTrabajo5->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ff0000"]]]);

                    ## condiciones de colores para la categoria de Liderazgo y relaciones en el trabajo
                    $LiderRelacinTrabajo1 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $LiderRelacinTrabajo1->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $LiderRelacinTrabajo1->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('0')->addCondition('13');
                    $LiderRelacinTrabajo1->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "9be5f7"]]]);

                    $LiderRelacinTrabajo2 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $LiderRelacinTrabajo2->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $LiderRelacinTrabajo2->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('14')->addCondition('28');
                    $LiderRelacinTrabajo2->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "6bf56e"]]]);

                    $LiderRelacinTrabajo3 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $LiderRelacinTrabajo3->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $LiderRelacinTrabajo3->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('29')->addCondition('41');
                    $LiderRelacinTrabajo3->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffff00"]]]);

                    $LiderRelacinTrabajo4 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $LiderRelacinTrabajo4->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $LiderRelacinTrabajo4->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('42')->addCondition('57');
                    $LiderRelacinTrabajo4->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffc000"]]]);

                    $LiderRelacinTrabajo5 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $LiderRelacinTrabajo5->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $LiderRelacinTrabajo5->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_GREATERTHANOREQUAL)
                                        ->addCondition('58');
                    $LiderRelacinTrabajo5->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ff0000"]]]);

                    ## condiciones de colores para la categoria de Entorno organizacional
                    $EntornoOrg1 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $EntornoOrg1->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $EntornoOrg1->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('0')->addCondition('9');
                    $EntornoOrg1->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "9be5f7"]]]);

                    $EntornoOrg2 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $EntornoOrg2->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $EntornoOrg2->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('10')->addCondition('13');
                    $EntornoOrg2->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "6bf56e"]]]);

                    $EntornoOrg3 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $EntornoOrg3->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $EntornoOrg3->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('14')->addCondition('17');
                    $EntornoOrg3->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffff00"]]]);

                    $EntornoOrg4 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $EntornoOrg4->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $EntornoOrg4->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_BETWEEN)
                                        ->addCondition('18')->addCondition('22');
                    $EntornoOrg4->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffc000"]]]);

                    $EntornoOrg5 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
                    $EntornoOrg5->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
                    $EntornoOrg5->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_GREATERTHANOREQUAL)
                                        ->addCondition('23');
                    $EntornoOrg5->getStyle()->applyFromArray(
                        ['fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ff0000"]]]);


                    if ($letterVal === 'E') {
                        $conditionalStyles = $spreadsheet->getActiveSheet()->getStyle("C$row:".$letterVal."$row")->getConditionalStyles();
                        $conditionalStyles[] = $CatAmbienteTrabajo1;
                        $conditionalStyles[] = $CatAmbienteTrabajo2;
                        $conditionalStyles[] = $CatAmbienteTrabajo3;
                        $conditionalStyles[] = $CatAmbienteTrabajo4;
                        $conditionalStyles[] = $CatAmbienteTrabajo5;
                        $spreadsheet->getActiveSheet()->getStyle("C$row:".$letterVal."$row")->setConditionalStyles($conditionalStyles);
                    } else if ($letterVal === 'O'){
                        $conditionalStyles = $spreadsheet->getActiveSheet()->getStyle("F$row:".$letterVal."$row")->getConditionalStyles();
                        $conditionalStyles[] = $FactorActividad1;
                        $conditionalStyles[] = $FactorActividad2;
                        $conditionalStyles[] = $FactorActividad3;
                        $conditionalStyles[] = $FactorActividad4;
                        $conditionalStyles[] = $FactorActividad5;
                        $spreadsheet->getActiveSheet()->getStyle("F$row:".$letterVal."$row")->setConditionalStyles($conditionalStyles);
                    } else if ($letterVal === 'R'){
                        $conditionalStyles = $spreadsheet->getActiveSheet()->getStyle("P$row:".$letterVal."$row")->getConditionalStyles();
                        $conditionalStyles[] = $TiempoTrabajo1;
                        $conditionalStyles[] = $TiempoTrabajo2;
                        $conditionalStyles[] = $TiempoTrabajo3;
                        $conditionalStyles[] = $TiempoTrabajo4;
                        $conditionalStyles[] = $TiempoTrabajo5;
                        $spreadsheet->getActiveSheet()->getStyle("P$row:".$letterVal."$row")->setConditionalStyles($conditionalStyles);
                    } else if ($letterVal === 'W'){
                        $conditionalStyles = $spreadsheet->getActiveSheet()->getStyle("S$row:".$letterVal."$row")->getConditionalStyles();
                        $conditionalStyles[] = $LiderRelacinTrabajo1;
                        $conditionalStyles[] = $LiderRelacinTrabajo2;
                        $conditionalStyles[] = $LiderRelacinTrabajo3;
                        $conditionalStyles[] = $LiderRelacinTrabajo4;
                        $conditionalStyles[] = $LiderRelacinTrabajo5;
                        $spreadsheet->getActiveSheet()->getStyle("S$row:".$letterVal."$row")->setConditionalStyles($conditionalStyles);
                    } else if ($letterVal === 'AA'){
                        $conditionalStyles = $spreadsheet->getActiveSheet()->getStyle("X$row:".$letterVal."$row")->getConditionalStyles();
                        $conditionalStyles[] = $EntornoOrg1;
                        $conditionalStyles[] = $EntornoOrg2;
                        $conditionalStyles[] = $EntornoOrg3;
                        $conditionalStyles[] = $EntornoOrg4;
                        $conditionalStyles[] = $EntornoOrg5;
                        $spreadsheet->getActiveSheet()->getStyle("X$row:".$letterVal."$row")->setConditionalStyles($conditionalStyles);
                    }
                    unset($conditionalStyles);
                    $letterVal++;
                }
                $spreadsheet->getActiveSheet()
                    ->setCellValue("AB$row","=SUM(C$row,F$row,P$row,S$row,X$row)");

                $conditionalStyles = $spreadsheet->getActiveSheet()->getStyle("AB$row")->getConditionalStyles();
                $conditionalStyles[] = $Totales1;
                $conditionalStyles[] = $Totales2;
                $conditionalStyles[] = $Totales3;
                $conditionalStyles[] = $Totales4;
                $conditionalStyles[] = $Totales5;
                $spreadsheet->getActiveSheet()->getStyle("AB$row")->setConditionalStyles($conditionalStyles);
                unset($conditionalStyles);

                $row++;
                $letterVal = 'A';
            }
        ################## END SHEET 3 ############

        #########################################################################################################
        #####################################  hoja 4 Graficas por califiacion ##################################
        #########################################################################################################
            $spreadsheet->createSheet();

            $spreadsheet->setActiveSheetIndex(3);
            $spreadsheet->getActiveSheet()->setTitle('Grafica califiación total');
            $spreadsheet->getActiveSheet()->getTabColor()->setARGB('465D99');

            $objDrawing = new drawing();
            $objDrawing->setName('TrabajoSinStress');
            $objDrawing->setDescription('TrabajoSinStress');
            $objDrawing->setPath('logo-sin-estres.jpg');
            $objDrawing->setHeight(53);
            $objDrawing->setWorksheet($spreadsheet->getActiveSheet());
            $objWriter = io_factory::createWriter($spreadsheet, 'Xlsx');

            $sheet = $spreadsheet->getActiveSheet();
            $spreadsheet->getActiveSheet()
                        ->getRowDimension('1')
                        ->setRowHeight(50);
            $spreadsheet->getActiveSheet()
                        ->getRowDimension('2')
                        ->setRowHeight(80);
            $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(15);
            $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(15);
            $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(20);

            $spreadsheet->getActiveSheet()
                        ->setCellValue("A2", "RIESGO DE TRABAJO");
            $spreadsheet->getActiveSheet()
                ->getStyle('A2')->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('00b0f0');


            $spreadsheet->getActiveSheet()->setCellValue("A3", "NULO");
            $spreadsheet->getActiveSheet()
                ->getStyle('A3')->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('9be5f7');

            $spreadsheet->getActiveSheet()->setCellValue("A4", "BAJO");
            $spreadsheet->getActiveSheet()
                ->getStyle('A4')->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('6bf56e');

            $spreadsheet->getActiveSheet()->setCellValue("A5", "MEDIO");
            $spreadsheet->getActiveSheet()
                ->getStyle('A5')->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('ffc000');

            $spreadsheet->getActiveSheet()->setCellValue("A6", "ALTO");
            $spreadsheet->getActiveSheet()
                ->getStyle('A6')->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('ffff00');

            $spreadsheet->getActiveSheet()->setCellValue("A7", "MUY ALTO");
            $spreadsheet->getActiveSheet()
                ->getStyle('A7')->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('ff0000');


            $spreadsheet->getActiveSheet()
                        ->setCellValue("B2", "TOTALES");
            $spreadsheet->getActiveSheet()
                ->getStyle('B2')->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('6bf56e');

            $lastRow++;
            $spreadsheet->getActiveSheet()
                ->setCellValue("B3", '=COUNTIF(\'Guias online\'!'.$ColumSumAllAnswers.'4:'.$ColumSumAllAnswers.$lastRow.',"<50")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B4", '=COUNTIFS(\'Guias online\'!'.$ColumSumAllAnswers.'4:'.$ColumSumAllAnswers.$lastRow.',">=50",\'Guias online\'!'.$ColumSumAllAnswers.'4:'.$ColumSumAllAnswers.$lastRow.',"<75")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B5", '=COUNTIFS(\'Guias online\'!'.$ColumSumAllAnswers.'4:'.$ColumSumAllAnswers.$lastRow.',">=75",\'Guias online\'!'.$ColumSumAllAnswers.'4:'.$ColumSumAllAnswers.$lastRow.',"<99")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B6", '=COUNTIFS(\'Guias online\'!'.$ColumSumAllAnswers.'4:'.$ColumSumAllAnswers.$lastRow.',">=99",\'Guias online\'!'.$ColumSumAllAnswers.'4:'.$ColumSumAllAnswers.$lastRow.',"<140")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B7", '=COUNTIF(\'Guias online\'!'.$ColumSumAllAnswers.'4:'.$ColumSumAllAnswers.$lastRow.',">=140")');

            $spreadsheet->getActiveSheet()->setCellValue("B8", "=SUM(B3:B7)");


            $spreadsheet->getActiveSheet()
                        ->setCellValue("C2", "VALORES");
            $spreadsheet->getActiveSheet()
                ->getStyle('C2')->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('ffc000');

            $spreadsheet->getActiveSheet()->setCellValue("C3", "=(B3/B8)");
            $spreadsheet->getActiveSheet()->setCellValue("C4", "=(B4/B8)");
            $spreadsheet->getActiveSheet()->setCellValue("C5", "=(B5/B8)");
            $spreadsheet->getActiveSheet()->setCellValue("C6", "=(B6/B8)");
            $spreadsheet->getActiveSheet()->setCellValue("C7", "=(B7/B8)");
            $spreadsheet->getActiveSheet()->setCellValue("C8", "=SUM(C3:C7)");

            $spreadsheet->getActiveSheet()
                ->getStyle('C3:C8')
                ->getNumberFormat()
                ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE);


            $spreadsheet->getActiveSheet()
                        ->setCellValue("D2", "CALIFICACIÓN GENERAL");
            $spreadsheet->getActiveSheet()
                ->getStyle('D2:D7')->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('ffff00');

            $spreadsheet->getActiveSheet()->setCellValue("D3", "='Guias online'!$ColumSumAllAnswers"."2")->mergeCells('D3:D7');

            // Custom colors for dataSeries (NULLO, BAJO, REGULAR, ALTO, MUY ALTO)
            $colors = [

                '9be5f7', '6bf56e', 'ffff00', 'ffc000','ff0000',
            ];



            // Set the Labels for each data series we want to plot
            //     Datatype
            //     Cell reference for data
            //     Format Code
            //     Number of datapoints in series
            //     Data values
            //     Data Marker
            $dataSeriesLabels1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'A3:A7', null, 1), // 2011
            ];
            // Set the X-Axis Labels
            //     Datatype
            //     Cell reference for data
            //     Format Code
            //     Number of datapoints in series
            //     Data values
            //     Data Marker
            $xAxisTickValues1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'A3:A7', null, 4), // Q1 to Q4
            ];
            // Set the Data values for each data series we want to plot
            //     Datatype
            //     Cell reference for data
            //     Format Code
            //     Number of datapoints in series
            //     Data values
            //     Data Marker
            //     Custom colors
            $dataSeriesValues1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_NUMBER, 'C3:C7', null, 4, [], null, $colors),
            ];

            // Build the dataseries
            $series1 = new DataSeries(
                DataSeries::TYPE_PIECHART, // plotType
                null, // plotGrouping (Pie charts don't have any grouping)
                range(0, count($dataSeriesValues1) - 1), // plotOrder
                $dataSeriesLabels1, // plotLabel
                $xAxisTickValues1, // plotCategory
                $dataSeriesValues1          // plotValues
            );

            // Set up a layout object for the Pie chart
            $layout1 = new Layout();
            $layout1->setShowVal(FALSE);
            $layout1->setShowPercent(true);

            // Set the series in the plot area
            $plotArea1 = new PlotArea($layout1, [$series1]);
            // Set the chart legend
            $legend1 = new Legend(Legend::POSITION_BOTTOM, null, false);

            $title1 = new Title('Resultado General');

            // Create the chart
            $chart1 = new Chart(
                'chart1', // name
                $title1, // title
                $legend1, // legend
                $plotArea1, // plotArea
                true, // plotVisibleOnly
                0, // displayBlanksAs
                null, // xAxisLabel
                null   // yAxisLabel - Pie charts don't have a Y-Axis
            );

            // Set the position where the chart should appear in the worksheet
            $chart1->setTopLeftPosition('E1');
            $chart1->setBottomRightPosition('M19');
            $spreadsheet->getActiveSheet()->addChart($chart1);




            $styleArray = array(
                'font' => array(
                    'bold' => true,
                    'size' => 14
                ),
                'alignment' => array(
                    'wrapText' => TRUE,
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                )
            );

            $spreadsheet->getActiveSheet()->getStyle('A2:D8')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A3:C8')->applyFromArray(['font'=>['size'=>10]]);
            //aplicamos los bordes
            $spreadsheet->getActiveSheet()
                        ->getStyle("A2:D7")
                        ->getBorders()
                        ->applyFromArray( [
                                'allBorders' => [
                                    'borderStyle' => Border::BORDER_THIN,
                                    'color' => ['rgb' => '000000' ]
                                ]
                            ]
                        );

        ################## END SHEET 4 ############

        #########################################################################################################
        #####################################  hoja 4 Graficas por categorias  ##################################
        #########################################################################################################
            $spreadsheet->createSheet();

            $spreadsheet->setActiveSheetIndex(4);
            $spreadsheet->getActiveSheet()->setTitle('Graficas por categorias');
            $spreadsheet->getActiveSheet()->getTabColor()->setARGB('465D99');

            $sheet = $spreadsheet->getActiveSheet();
            $spreadsheet->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
            $spreadsheet->getActiveSheet()->getRowDimension('10')->setRowHeight(30);
            $spreadsheet->getActiveSheet()->getRowDimension('19')->setRowHeight(30);
            $spreadsheet->getActiveSheet()->getRowDimension('28')->setRowHeight(30);
            $spreadsheet->getActiveSheet()->getRowDimension('37')->setRowHeight(30);

            $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(15);
            $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(15);




            $BgColorTitle = [
                'fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "dcd3a1"]]
            ];
            $BgColorRiesgo = [
                'fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "00b0f0"]]
            ];
            $BgColorNulo = [
                'fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "9be5f7"]]
            ];
            $BgColorBajo = [
                'fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "6bf56e"]]
            ];
            $BgColorMedio = [
                'fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffff00"]]
            ];
            $BgColorAlto = [
                'fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ffc000"]]
            ];
            $BgColorMuyAlto = [
                'fill' => ['fillType' => fill::FILL_SOLID,'color' => ['argb' => "ff0000"]]
            ];

            ### CATEGORIA AMBIENTE DE TRABAJO
            $spreadsheet->getActiveSheet()->setCellValue("A1", "AMBIENTE DE TRABAJO")->mergeCells('A1:C1');
            $spreadsheet->getActiveSheet()->getStyle('A1')->applyFromArray($BgColorTitle);

            $spreadsheet->getActiveSheet()->setCellValue("A2", "RIESGO DE TRABAJO");
            $spreadsheet->getActiveSheet()->getStyle('A2')->applyFromArray($BgColorRiesgo);

            $spreadsheet->getActiveSheet()->setCellValue("A3", "NULO");
            $spreadsheet->getActiveSheet()->getStyle('A3')->applyFromArray($BgColorNulo);

            $spreadsheet->getActiveSheet()->setCellValue("A4", "BAJO");
            $spreadsheet->getActiveSheet()->getStyle('A4')->applyFromArray($BgColorBajo);

            $spreadsheet->getActiveSheet()->setCellValue("A5", "MEDIO");
            $spreadsheet->getActiveSheet()->getStyle('A5')->applyFromArray($BgColorMedio);

            $spreadsheet->getActiveSheet()->setCellValue("A6", "ALTO");
            $spreadsheet->getActiveSheet()->getStyle('A6')->applyFromArray($BgColorAlto);

            $spreadsheet->getActiveSheet()->setCellValue("A7", "MUY ALTO");
            $spreadsheet->getActiveSheet()->getStyle('A7')->applyFromArray($BgColorMuyAlto);



            $spreadsheet->getActiveSheet()
                        ->setCellValue("B2", "TOTAL");
            $spreadsheet->getActiveSheet()->getStyle('B2')->applyFromArray($BgColorBajo);

            $spreadsheet->getActiveSheet()
                ->setCellValue("B3", '=COUNTIF(\'Categorias y dominios\'!'.'C3:C'.$lastRow.',"<5")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B4", '=COUNTIFS(\'Categorias y dominios\'!C3:C'.$lastRow.',">=5",\'Categorias y dominios\'!C3:C'.$lastRow.',"<9")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B5", '=COUNTIFS(\'Categorias y dominios\'!C3:C'.$lastRow.',">=9",\'Categorias y dominios\'!C3:C'.$lastRow.',"<11")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B6", '=COUNTIFS(\'Categorias y dominios\'!C3:C'.$lastRow.',">=11",\'Categorias y dominios\'!C3:C'.$lastRow.',"<14")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B7", '=COUNTIF(\'Categorias y dominios\'!C3:C'.$lastRow.',">=14")');

            $spreadsheet->getActiveSheet()->setCellValue("B8", "=SUM(B3:B7)");


            $spreadsheet->getActiveSheet()
                        ->setCellValue("C2", "VALOR");
            $spreadsheet->getActiveSheet()->getStyle('C2')->applyFromArray($BgColorAlto);

            $spreadsheet->getActiveSheet()->setCellValue("C3", "=(B3/B8)");
            $spreadsheet->getActiveSheet()->setCellValue("C4", "=(B4/B8)");
            $spreadsheet->getActiveSheet()->setCellValue("C5", "=(B5/B8)");
            $spreadsheet->getActiveSheet()->setCellValue("C6", "=(B6/B8)");
            $spreadsheet->getActiveSheet()->setCellValue("C7", "=(B7/B8)");
            $spreadsheet->getActiveSheet()->setCellValue("C8", "=SUM(C3:C7)");

            $spreadsheet->getActiveSheet()
                ->getStyle('C3:C8')
                ->getNumberFormat()
                ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE);


            $dataSeriesLabels1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'A3:A7', null, 1), // 2011
            ];
            $xAxisTickValues1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'A3:A7', null, 4), // Q1 to Q4
            ];
            $dataSeriesValues1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_NUMBER, 'C3:C7', null, 4, [], null, $colors),
            ];

            // Build the dataseries
            $series1 = new DataSeries(
                DataSeries::TYPE_PIECHART, // plotType
                null, // plotGrouping (Pie charts don't have any grouping)
                range(0, count($dataSeriesValues1) - 1), // plotOrder
                $dataSeriesLabels1, // plotLabel
                $xAxisTickValues1, // plotCategory
                $dataSeriesValues1          // plotValues
            );

            // Set up a layout object for the Pie chart
            $layout1 = new Layout();
            $layout1->setShowVal(FALSE);
            $layout1->setShowPercent(true);

            // Set the series in the plot area
            $plotArea1 = new PlotArea($layout1, [$series1]);
            // Set the chart legend
            $legend1 = new Legend(Legend::POSITION_BOTTOM, null, false);

            $title1 = new Title('AMBIENTE DE TRABAJO');

            // Create the chart
            $chart1 = new Chart(
                'chart1', // name
                $title1, // title
                $legend1, // legend
                $plotArea1, // plotArea
                true, // plotVisibleOnly
                0, // displayBlanksAs
                null, // xAxisLabel
                null   // yAxisLabel - Pie charts don't have a Y-Axis
            );

            // Set the position where the chart should appear in the worksheet
            $chart1->setTopLeftPosition('D1');
            $chart1->setBottomRightPosition('M14');
            $spreadsheet->getActiveSheet()->addChart($chart1);
            # --------------------------------------------------------------------------------------------------#
            ################### CATEGORIA Factores propios de la actividad  #####################################

            $spreadsheet->getActiveSheet()->setCellValue("A10", strtoupper("Factores propios de la actividad"))->mergeCells('A10:C10');
            $spreadsheet->getActiveSheet()->getStyle('A10')->applyFromArray($BgColorTitle);

            $spreadsheet->getActiveSheet()->setCellValue("A11", "RIESGO DE TRABAJO");
            $spreadsheet->getActiveSheet()->getStyle('A11')->applyFromArray($BgColorRiesgo);

            $spreadsheet->getActiveSheet()->setCellValue("A12", "NULO");
            $spreadsheet->getActiveSheet()->getStyle('A12')->applyFromArray($BgColorNulo);

            $spreadsheet->getActiveSheet()->setCellValue("A13", "BAJO");
            $spreadsheet->getActiveSheet()->getStyle('A13')->applyFromArray($BgColorBajo);

            $spreadsheet->getActiveSheet()->setCellValue("A14", "MEDIO");
            $spreadsheet->getActiveSheet()->getStyle('A14')->applyFromArray($BgColorMedio);

            $spreadsheet->getActiveSheet()->setCellValue("A15", "ALTO");
            $spreadsheet->getActiveSheet()->getStyle('A15')->applyFromArray($BgColorAlto);

            $spreadsheet->getActiveSheet()->setCellValue("A16", "MUY ALTO");
            $spreadsheet->getActiveSheet()->getStyle('A16')->applyFromArray($BgColorMuyAlto);


            $spreadsheet->getActiveSheet()
                        ->setCellValue("B11", "TOTAL");
            $spreadsheet->getActiveSheet()->getStyle('B11')->applyFromArray($BgColorBajo);

            $spreadsheet->getActiveSheet()
                ->setCellValue("B12", '=COUNTIF(\'Categorias y dominios\'!'.'D3:D'.$lastRow.',"<15")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B13", '=COUNTIFS(\'Categorias y dominios\'!D3:D'.$lastRow.',">=15",\'Categorias y dominios\'!D3:D'.$lastRow.',"<30")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B14", '=COUNTIFS(\'Categorias y dominios\'!D3:D'.$lastRow.',">=30",\'Categorias y dominios\'!D3:D'.$lastRow.',"<45")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B15", '=COUNTIFS(\'Categorias y dominios\'!D3:D'.$lastRow.',">=45",\'Categorias y dominios\'!D3:D'.$lastRow.',"<60")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B16", '=COUNTIF(\'Categorias y dominios\'!D3:D'.$lastRow.',">=60")');

            $spreadsheet->getActiveSheet()->setCellValue("B17", "=SUM(B12:B16)");


            $spreadsheet->getActiveSheet()
                        ->setCellValue("C11", "VALOR");
            $spreadsheet->getActiveSheet()->getStyle('C11')->applyFromArray($BgColorAlto);

            $spreadsheet->getActiveSheet()->setCellValue("C12", "=(B12/B17)");
            $spreadsheet->getActiveSheet()->setCellValue("C13", "=(B13/B17)");
            $spreadsheet->getActiveSheet()->setCellValue("C14", "=(B14/B17)");
            $spreadsheet->getActiveSheet()->setCellValue("C15", "=(B15/B17)");
            $spreadsheet->getActiveSheet()->setCellValue("C16", "=(B16/B17)");
            $spreadsheet->getActiveSheet()->setCellValue("C17", "=SUM(C12:C16)");

            $spreadsheet->getActiveSheet()
                ->getStyle('C12:C17')
                ->getNumberFormat()
                ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE);


            $dataSeriesLabels1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'A12:A16', null, 1), // 2011
            ];
            $xAxisTickValues1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'A12:A16', null, 4), // Q1 to Q4
            ];
            $dataSeriesValues1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_NUMBER, 'C12:C16', null, 4, [], null, $colors),
            ];

            // Build the dataseries
            $series1 = new DataSeries(
                DataSeries::TYPE_PIECHART, // plotType
                null, // plotGrouping (Pie charts don't have any grouping)
                range(0, count($dataSeriesValues1) - 1), // plotOrder
                $dataSeriesLabels1, // plotLabel
                $xAxisTickValues1, // plotCategory
                $dataSeriesValues1          // plotValues
            );

            // Set up a layout object for the Pie chart
            $layout1 = new Layout();
            $layout1->setShowVal(FALSE);
            $layout1->setShowPercent(true);

            // Set the series in the plot area
            $plotArea1 = new PlotArea($layout1, [$series1]);
            // Set the chart legend
            $legend1 = new Legend(Legend::POSITION_BOTTOM, null, false);

            $title1 = new Title(strtoupper('Factores propios de la actividad'));

            // Create the chart
            $chart2 = new Chart(
                'chart2', // name
                $title1, // title
                $legend1, // legend
                $plotArea1, // plotArea
                true, // plotVisibleOnly
                0, // displayBlanksAs
                null, // xAxisLabel
                null   // yAxisLabel - Pie charts don't have a Y-Axis
            );

            // Set the position where the chart should appear in the worksheet
            $chart2->setTopLeftPosition('N1');
            $chart2->setBottomRightPosition('V14');
            $spreadsheet->getActiveSheet()->addChart($chart2);


            # --------------------------------------------------------------------------------------------------#
            ################### CATEGORIA Organización del tiempo de trabajo ####################################

            $spreadsheet->getActiveSheet()->setCellValue("A19", strtoupper("OrganizaciÓn del tiempo de trabajo"))->mergeCells('A19:C19');
            $spreadsheet->getActiveSheet()->getStyle('A19')->applyFromArray($BgColorTitle);

            $spreadsheet->getActiveSheet()->setCellValue("A20", "RIESGO DE TRABAJO");
            $spreadsheet->getActiveSheet()->getStyle('A20')->applyFromArray($BgColorRiesgo);

            $spreadsheet->getActiveSheet()->setCellValue("A21", "NULO");
            $spreadsheet->getActiveSheet()->getStyle('A21')->applyFromArray($BgColorNulo);

            $spreadsheet->getActiveSheet()->setCellValue("A22", "BAJO");
            $spreadsheet->getActiveSheet()->getStyle('A22')->applyFromArray($BgColorBajo);

            $spreadsheet->getActiveSheet()->setCellValue("A23", "MEDIO");
            $spreadsheet->getActiveSheet()->getStyle('A23')->applyFromArray($BgColorMedio);

            $spreadsheet->getActiveSheet()->setCellValue("A24", "ALTO");
            $spreadsheet->getActiveSheet()->getStyle('A24')->applyFromArray($BgColorAlto);

            $spreadsheet->getActiveSheet()->setCellValue("A25", "MUY ALTO");
            $spreadsheet->getActiveSheet()->getStyle('A25')->applyFromArray($BgColorMuyAlto);


            $spreadsheet->getActiveSheet()
                        ->setCellValue("B20", "TOTAL");
            $spreadsheet->getActiveSheet()->getStyle('B20')->applyFromArray($BgColorBajo);

            $spreadsheet->getActiveSheet()
                ->setCellValue("B21", '=COUNTIF(\'Categorias y dominios\'!'.'E3:E'.$lastRow.',"<5")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B22", '=COUNTIFS(\'Categorias y dominios\'!E3:E'.$lastRow.',">=5",\'Categorias y dominios\'!E3:E'.$lastRow.',"<7")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B23", '=COUNTIFS(\'Categorias y dominios\'!E3:E'.$lastRow.',">=7",\'Categorias y dominios\'!E3:E'.$lastRow.',"<10")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B24", '=COUNTIFS(\'Categorias y dominios\'!E3:E'.$lastRow.',">=10",\'Categorias y dominios\'!E3:E'.$lastRow.',"<13")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B25", '=COUNTIF(\'Categorias y dominios\'!E3:E'.$lastRow.',">=13")');

            $spreadsheet->getActiveSheet()->setCellValue("B26", "=SUM(B21:B25)");


            $spreadsheet->getActiveSheet()
                        ->setCellValue("C20", "VALOR");
            $spreadsheet->getActiveSheet()->getStyle('C20')->applyFromArray($BgColorAlto);

            $spreadsheet->getActiveSheet()->setCellValue("C21", "=(B21/B26)");
            $spreadsheet->getActiveSheet()->setCellValue("C22", "=(B22/B26)");
            $spreadsheet->getActiveSheet()->setCellValue("C23", "=(B23/B26)");
            $spreadsheet->getActiveSheet()->setCellValue("C24", "=(B24/B26)");
            $spreadsheet->getActiveSheet()->setCellValue("C25", "=(B25/B26)");
            $spreadsheet->getActiveSheet()->setCellValue("C26", "=SUM(C21:C25)");

            $spreadsheet->getActiveSheet()
                ->getStyle('C21:C26')
                ->getNumberFormat()
                ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE);


            $dataSeriesLabels1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'A21:A25', null, 1), // 2011
            ];
            $xAxisTickValues1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'A21:A25', null, 4), // Q1 to Q4
            ];
            $dataSeriesValues1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_NUMBER, 'C21:C25', null, 4, [], null, $colors),
            ];

            // Build the dataseries
            $series1 = new DataSeries(
                DataSeries::TYPE_PIECHART, // plotType
                null, // plotGrouping (Pie charts don't have any grouping)
                range(0, count($dataSeriesValues1) - 1), // plotOrder
                $dataSeriesLabels1, // plotLabel
                $xAxisTickValues1, // plotCategory
                $dataSeriesValues1          // plotValues
            );

            // Set up a layout object for the Pie chart
            $layout1 = new Layout();
            $layout1->setShowVal(FALSE);
            $layout1->setShowPercent(true);

            // Set the series in the plot area
            $plotArea1 = new PlotArea($layout1, [$series1]);
            // Set the chart legend
            $legend1 = new Legend(Legend::POSITION_BOTTOM, null, false);

            $title1 = new Title(strtoupper('OrganizaciÓn del tiempo de trabajo'));

            // Create the chart
            $chart3 = new Chart(
                'chart3', // name
                $title1, // title
                $legend1, // legend
                $plotArea1, // plotArea
                true, // plotVisibleOnly
                0, // displayBlanksAs
                null, // xAxisLabel
                null   // yAxisLabel - Pie charts don't have a Y-Axis
            );

            // Set the position where the chart should appear in the worksheet
            $chart3->setTopLeftPosition('W1');
            $chart3->setBottomRightPosition('AE14');
            $spreadsheet->getActiveSheet()->addChart($chart3);

            # --------------------------------------------------------------------------------------------------#
            ################### CATEGORIA Liderazgo y relaciones en el trabajo ##################################

            $spreadsheet->getActiveSheet()->setCellValue("A28", strtoupper("Liderazgo y relaciones en el trabajo"))->mergeCells('A28:C28');
            $spreadsheet->getActiveSheet()->getStyle('A28')->applyFromArray($BgColorTitle);

            $spreadsheet->getActiveSheet()->setCellValue("A29", "RIESGO DE TRABAJO");
            $spreadsheet->getActiveSheet()->getStyle('A29')->applyFromArray($BgColorRiesgo);

            $spreadsheet->getActiveSheet()->setCellValue("A30", "NULO");
            $spreadsheet->getActiveSheet()->getStyle('A30')->applyFromArray($BgColorNulo);

            $spreadsheet->getActiveSheet()->setCellValue("A31", "BAJO");
            $spreadsheet->getActiveSheet()->getStyle('A31')->applyFromArray($BgColorBajo);

            $spreadsheet->getActiveSheet()->setCellValue("A32", "MEDIO");
            $spreadsheet->getActiveSheet()->getStyle('A32')->applyFromArray($BgColorMedio);

            $spreadsheet->getActiveSheet()->setCellValue("A33", "ALTO");
            $spreadsheet->getActiveSheet()->getStyle('A33')->applyFromArray($BgColorAlto);

            $spreadsheet->getActiveSheet()->setCellValue("A34", "MUY ALTO");
            $spreadsheet->getActiveSheet()->getStyle('A34')->applyFromArray($BgColorMuyAlto);


            $spreadsheet->getActiveSheet()
                        ->setCellValue("B29", "TOTAL");
            $spreadsheet->getActiveSheet()->getStyle('B29')->applyFromArray($BgColorBajo);

            $spreadsheet->getActiveSheet()
                ->setCellValue("B30", '=COUNTIF(\'Categorias y dominios\'!'.'F3:F'.$lastRow.',"<14")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B31", '=COUNTIFS(\'Categorias y dominios\'!F3:F'.$lastRow.',">=14",\'Categorias y dominios\'!F3:F'.$lastRow.',"<29")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B32", '=COUNTIFS(\'Categorias y dominios\'!F3:F'.$lastRow.',">=29",\'Categorias y dominios\'!F3:F'.$lastRow.',"<42")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B33", '=COUNTIFS(\'Categorias y dominios\'!F3:F'.$lastRow.',">=42",\'Categorias y dominios\'!F3:F'.$lastRow.',"<58")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B34", '=COUNTIF(\'Categorias y dominios\'!F3:F'.$lastRow.',">=58")');

            $spreadsheet->getActiveSheet()->setCellValue("B35", "=SUM(B30:B34)");


            $spreadsheet->getActiveSheet()
                        ->setCellValue("C29", "VALOR");
            $spreadsheet->getActiveSheet()->getStyle('C29')->applyFromArray($BgColorAlto);

            $spreadsheet->getActiveSheet()->setCellValue("C30", "=(B30/B35)");
            $spreadsheet->getActiveSheet()->setCellValue("C31", "=(B31/B35)");
            $spreadsheet->getActiveSheet()->setCellValue("C32", "=(B32/B35)");
            $spreadsheet->getActiveSheet()->setCellValue("C33", "=(B33/B35)");
            $spreadsheet->getActiveSheet()->setCellValue("C34", "=(B34/B35)");
            $spreadsheet->getActiveSheet()->setCellValue("C35", "=SUM(C30:C34)");

            $spreadsheet->getActiveSheet()
                ->getStyle('C30:C35')
                ->getNumberFormat()
                ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE);


            $dataSeriesLabels1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'A30:A34', null, 1), // 2011
            ];
            $xAxisTickValues1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'A30:A34', null, 4), // Q1 to Q4
            ];
            $dataSeriesValues1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_NUMBER, 'C30:C34', null, 4, [], null, $colors),
            ];

            // Build the dataseries
            $series1 = new DataSeries(
                DataSeries::TYPE_PIECHART, // plotType
                null, // plotGrouping (Pie charts don't have any grouping)
                range(0, count($dataSeriesValues1) - 1), // plotOrder
                $dataSeriesLabels1, // plotLabel
                $xAxisTickValues1, // plotCategory
                $dataSeriesValues1          // plotValues
            );

            // Set up a layout object for the Pie chart
            $layout1 = new Layout();
            $layout1->setShowVal(FALSE);
            $layout1->setShowPercent(true);

            // Set the series in the plot area
            $plotArea1 = new PlotArea($layout1, [$series1]);
            // Set the chart legend
            $legend1 = new Legend(Legend::POSITION_BOTTOM, null, false);

            $title1 = new Title(strtoupper('Liderazgo y relaciones en el trabajo'));

            // Create the chart
            $chart4 = new Chart(
                'chart4', // name
                $title1, // title
                $legend1, // legend
                $plotArea1, // plotArea
                true, // plotVisibleOnly
                0, // displayBlanksAs
                null, // xAxisLabel
                null   // yAxisLabel - Pie charts don't have a Y-Axis
            );

            // Set the position where the chart should appear in the worksheet
            $chart4->setTopLeftPosition('D16');
            $chart4->setBottomRightPosition('M30');
            $spreadsheet->getActiveSheet()->addChart($chart4);

            # --------------------------------------------------------------------------------------------------#
            ################### CATEGORIA Entorno organizacional ##################################

            $spreadsheet->getActiveSheet()->setCellValue("A37", strtoupper("Entorno organizacional"))->mergeCells('A37:C37');
            $spreadsheet->getActiveSheet()->getStyle('A37')->applyFromArray($BgColorTitle);

            $spreadsheet->getActiveSheet()->setCellValue("A38", "RIESGO DE TRABAJO");
            $spreadsheet->getActiveSheet()->getStyle('A38')->applyFromArray($BgColorRiesgo);

            $spreadsheet->getActiveSheet()->setCellValue("A39", "NULO");
            $spreadsheet->getActiveSheet()->getStyle('A39')->applyFromArray($BgColorNulo);

            $spreadsheet->getActiveSheet()->setCellValue("A40", "BAJO");
            $spreadsheet->getActiveSheet()->getStyle('A40')->applyFromArray($BgColorBajo);

            $spreadsheet->getActiveSheet()->setCellValue("A41", "MEDIO");
            $spreadsheet->getActiveSheet()->getStyle('A41')->applyFromArray($BgColorMedio);

            $spreadsheet->getActiveSheet()->setCellValue("A42", "ALTO");
            $spreadsheet->getActiveSheet()->getStyle('A42')->applyFromArray($BgColorAlto);

            $spreadsheet->getActiveSheet()->setCellValue("A43", "MUY ALTO");
            $spreadsheet->getActiveSheet()->getStyle('A43')->applyFromArray($BgColorMuyAlto);


            $spreadsheet->getActiveSheet()
                        ->setCellValue("B38", "TOTAL");
            $spreadsheet->getActiveSheet()->getStyle('B38')->applyFromArray($BgColorBajo);

            $spreadsheet->getActiveSheet()
                ->setCellValue("B39", '=COUNTIF(\'Categorias y dominios\'!'.'G3:G'.$lastRow.',"<10")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B40", '=COUNTIFS(\'Categorias y dominios\'!G3:G'.$lastRow.',">=10",\'Categorias y dominios\'!G3:G'.$lastRow.',"<14")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B41", '=COUNTIFS(\'Categorias y dominios\'!G3:G'.$lastRow.',">=14",\'Categorias y dominios\'!G3:G'.$lastRow.',"<18")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B42", '=COUNTIFS(\'Categorias y dominios\'!G3:G'.$lastRow.',">=18",\'Categorias y dominios\'!G3:G'.$lastRow.',"<23")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B43", '=COUNTIF(\'Categorias y dominios\'!G3:G'.$lastRow.',">=23")');

            $spreadsheet->getActiveSheet()->setCellValue("B44", "=SUM(B39:B43)");


            $spreadsheet->getActiveSheet()
                        ->setCellValue("C38", "VALOR");
            $spreadsheet->getActiveSheet()->getStyle('C38')->applyFromArray($BgColorAlto);

            $spreadsheet->getActiveSheet()->setCellValue("C39", "=(B39/B44)");
            $spreadsheet->getActiveSheet()->setCellValue("C40", "=(B40/B44)");
            $spreadsheet->getActiveSheet()->setCellValue("C41", "=(B41/B44)");
            $spreadsheet->getActiveSheet()->setCellValue("C42", "=(B42/B44)");
            $spreadsheet->getActiveSheet()->setCellValue("C43", "=(B43/B44)");
            $spreadsheet->getActiveSheet()->setCellValue("C44", "=SUM(C39:C43)");

            $spreadsheet->getActiveSheet()
                ->getStyle('C39:C44')
                ->getNumberFormat()
                ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE);


            $dataSeriesLabels1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'A39:A43', null, 1), // 2011
            ];
            $xAxisTickValues1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'A39:A43', null, 4), // Q1 to Q4
            ];
            $dataSeriesValues1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_NUMBER, 'C39:C43', null, 4, [], null, $colors),
            ];

            // Build the dataseries
            $series1 = new DataSeries(
                DataSeries::TYPE_PIECHART, // plotType
                null, // plotGrouping (Pie charts don't have any grouping)
                range(0, count($dataSeriesValues1) - 1), // plotOrder
                $dataSeriesLabels1, // plotLabel
                $xAxisTickValues1, // plotCategory
                $dataSeriesValues1          // plotValues
            );

            // Set up a layout object for the Pie chart
            $layout1 = new Layout();
            $layout1->setShowVal(FALSE);
            $layout1->setShowPercent(true);

            // Set the series in the plot area
            $plotArea1 = new PlotArea($layout1, [$series1]);
            // Set the chart legend
            $legend1 = new Legend(Legend::POSITION_BOTTOM, null, false);

            $title1 = new Title(strtoupper('Entorno organizacional'));

            // Create the chart
            $chart5 = new Chart(
                'chart5', // name
                $title1, // title
                $legend1, // legend
                $plotArea1, // plotArea
                true, // plotVisibleOnly
                0, // displayBlanksAs
                null, // xAxisLabel
                null   // yAxisLabel - Pie charts don't have a Y-Axis
            );

            // Set the position where the chart should appear in the worksheet
            $chart5->setTopLeftPosition('N16');
            $chart5->setBottomRightPosition('V30');
            $spreadsheet->getActiveSheet()->addChart($chart5);

            ###### FIN GRAFICAS POR CATEGORIA
            //$styleArray esta declarada en la hoja de calificacion total
            $spreadsheet->getActiveSheet()->getStyle('A1:C8')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A3:C8')->applyFromArray(['font'=>['size'=>10]]);

            $spreadsheet->getActiveSheet()->getStyle('A10:C17')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A12:C17')->applyFromArray(['font'=>['size'=>10]]);

            $spreadsheet->getActiveSheet()->getStyle('A19:C26')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A21:C26')->applyFromArray(['font'=>['size'=>10]]);

            $spreadsheet->getActiveSheet()->getStyle('A28:C35')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A30:C35')->applyFromArray(['font'=>['size'=>10]]);

            $spreadsheet->getActiveSheet()->getStyle('A37:C44')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A39:C44')->applyFromArray(['font'=>['size'=>10]]);

            //OCULTAMOS LAS COLUMNAS A B y C
            $spreadsheet->getActiveSheet()->getColumnDimension('A')->setVisible(false);
            $spreadsheet->getActiveSheet()->getColumnDimension('B')->setVisible(false);
            $spreadsheet->getActiveSheet()->getColumnDimension('C')->setVisible(false);


        ################## END SHEET 5 ############

        #########################################################################################################
        #####################################  hoja 6 Graficas por dominio   ####################################
        #########################################################################################################
            $spreadsheet->createSheet();

            $spreadsheet->setActiveSheetIndex(5);
            $spreadsheet->getActiveSheet()->setTitle('Graficas por dominio');
            $spreadsheet->getActiveSheet()->getTabColor()->setARGB('465D99');

            $sheet = $spreadsheet->getActiveSheet();
            $spreadsheet->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
            $spreadsheet->getActiveSheet()->getRowDimension('10')->setRowHeight(30);
            $spreadsheet->getActiveSheet()->getRowDimension('19')->setRowHeight(30);
            $spreadsheet->getActiveSheet()->getRowDimension('28')->setRowHeight(30);
            $spreadsheet->getActiveSheet()->getRowDimension('37')->setRowHeight(30);
            $spreadsheet->getActiveSheet()->getRowDimension('46')->setRowHeight(30);
            $spreadsheet->getActiveSheet()->getRowDimension('55')->setRowHeight(30);
            $spreadsheet->getActiveSheet()->getRowDimension('64')->setRowHeight(30);
            $spreadsheet->getActiveSheet()->getRowDimension('73')->setRowHeight(30);
            $spreadsheet->getActiveSheet()->getRowDimension('82')->setRowHeight(30);

            $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(15);
            $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(15);



            ### Condiciones en el ambiente de trabajo
            $spreadsheet->getActiveSheet()->setCellValue("A1", strtoupper("Condiciones en el ambiente de trabajo"))->mergeCells('A1:C1');
            $spreadsheet->getActiveSheet()->getStyle('A1')->applyFromArray($BgColorTitle);

            $spreadsheet->getActiveSheet()->setCellValue("A2", "RIESGO DE TRABAJO");
            $spreadsheet->getActiveSheet()->getStyle('A2')->applyFromArray($BgColorRiesgo);

            $spreadsheet->getActiveSheet()->setCellValue("A3", "NULO");
            $spreadsheet->getActiveSheet()->getStyle('A3')->applyFromArray($BgColorNulo);

            $spreadsheet->getActiveSheet()->setCellValue("A4", "BAJO");
            $spreadsheet->getActiveSheet()->getStyle('A4')->applyFromArray($BgColorBajo);

            $spreadsheet->getActiveSheet()->setCellValue("A5", "MEDIO");
            $spreadsheet->getActiveSheet()->getStyle('A5')->applyFromArray($BgColorMedio);

            $spreadsheet->getActiveSheet()->setCellValue("A6", "ALTO");
            $spreadsheet->getActiveSheet()->getStyle('A6')->applyFromArray($BgColorAlto);

            $spreadsheet->getActiveSheet()->setCellValue("A7", "MUY ALTO");
            $spreadsheet->getActiveSheet()->getStyle('A7')->applyFromArray($BgColorMuyAlto);



            $spreadsheet->getActiveSheet()
                        ->setCellValue("B2", "TOTAL");
            $spreadsheet->getActiveSheet()->getStyle('B2')->applyFromArray($BgColorBajo);

            $spreadsheet->getActiveSheet()
                ->setCellValue("B3", '=COUNTIF(\'Categorias y dominios\'!'.'H3:H'.$lastRow.',"<5")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B4", '=COUNTIFS(\'Categorias y dominios\'!H3:H'.$lastRow.',">=5",\'Categorias y dominios\'!H3:H'.$lastRow.',"<9")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B5", '=COUNTIFS(\'Categorias y dominios\'!H3:H'.$lastRow.',">=9",\'Categorias y dominios\'!H3:H'.$lastRow.',"<11")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B6", '=COUNTIFS(\'Categorias y dominios\'!H3:H'.$lastRow.',">=11",\'Categorias y dominios\'!H3:H'.$lastRow.',"<14")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B7", '=COUNTIF(\'Categorias y dominios\'!H3:H'.$lastRow.',">=14")');

            $spreadsheet->getActiveSheet()->setCellValue("B8", "=SUM(B3:B7)");


            $spreadsheet->getActiveSheet()
                        ->setCellValue("C2", "VALOR");
            $spreadsheet->getActiveSheet()->getStyle('C2')->applyFromArray($BgColorAlto);

            $spreadsheet->getActiveSheet()->setCellValue("C3", "=(B3/B8)");
            $spreadsheet->getActiveSheet()->setCellValue("C4", "=(B4/B8)");
            $spreadsheet->getActiveSheet()->setCellValue("C5", "=(B5/B8)");
            $spreadsheet->getActiveSheet()->setCellValue("C6", "=(B6/B8)");
            $spreadsheet->getActiveSheet()->setCellValue("C7", "=(B7/B8)");
            $spreadsheet->getActiveSheet()->setCellValue("C8", "=SUM(C3:C7)");

            $spreadsheet->getActiveSheet()
                ->getStyle('C3:C8')
                ->getNumberFormat()
                ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE);


            $dataSeriesLabels1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'A3:A7', null, 1), // 2011
            ];
            $xAxisTickValues1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'A3:A7', null, 4), // Q1 to Q4
            ];
            $dataSeriesValues1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_NUMBER, 'C3:C7', null, 4, [], null, $colors),
            ];

            // Build the dataseries
            $series1 = new DataSeries(
                DataSeries::TYPE_PIECHART, // plotType
                null, // plotGrouping (Pie charts don't have any grouping)
                range(0, count($dataSeriesValues1) - 1), // plotOrder
                $dataSeriesLabels1, // plotLabel
                $xAxisTickValues1, // plotCategory
                $dataSeriesValues1          // plotValues
            );

            // Set up a layout object for the Pie chart
            $layout1 = new Layout();
            $layout1->setShowVal(FALSE);
            $layout1->setShowPercent(true);

            // Set the series in the plot area
            $plotArea1 = new PlotArea($layout1, [$series1]);
            // Set the chart legend
            $legend1 = new Legend(Legend::POSITION_BOTTOM, null, false);

            $title1 = new Title(strtoupper("Condiciones en el ambiente de trabajo"));

            // Create the chart
            $chart1 = new Chart(
                'chart1', // name
                $title1, // title
                $legend1, // legend
                $plotArea1, // plotArea
                true, // plotVisibleOnly
                0, // displayBlanksAs
                null, // xAxisLabel
                null   // yAxisLabel - Pie charts don't have a Y-Axis
            );

            // Set the position where the chart should appear in the worksheet
            $chart1->setTopLeftPosition('D1');
            $chart1->setBottomRightPosition('M14');
            $spreadsheet->getActiveSheet()->addChart($chart1);
            # --------------------------------------------------------------------------------------------------#
            ###################  DOMINIO Carga de trabajo  #####################################

            $spreadsheet->getActiveSheet()->setCellValue("A10", strtoupper("Carga de trabajo"))->mergeCells('A10:C10');
            $spreadsheet->getActiveSheet()->getStyle('A10')->applyFromArray($BgColorTitle);

            $spreadsheet->getActiveSheet()->setCellValue("A11", "RIESGO DE TRABAJO");
            $spreadsheet->getActiveSheet()->getStyle('A11')->applyFromArray($BgColorRiesgo);

            $spreadsheet->getActiveSheet()->setCellValue("A12", "NULO");
            $spreadsheet->getActiveSheet()->getStyle('A12')->applyFromArray($BgColorNulo);

            $spreadsheet->getActiveSheet()->setCellValue("A13", "BAJO");
            $spreadsheet->getActiveSheet()->getStyle('A13')->applyFromArray($BgColorBajo);

            $spreadsheet->getActiveSheet()->setCellValue("A14", "MEDIO");
            $spreadsheet->getActiveSheet()->getStyle('A14')->applyFromArray($BgColorMedio);

            $spreadsheet->getActiveSheet()->setCellValue("A15", "ALTO");
            $spreadsheet->getActiveSheet()->getStyle('A15')->applyFromArray($BgColorAlto);

            $spreadsheet->getActiveSheet()->setCellValue("A16", "MUY ALTO");
            $spreadsheet->getActiveSheet()->getStyle('A16')->applyFromArray($BgColorMuyAlto);


            $spreadsheet->getActiveSheet()
                        ->setCellValue("B11", "TOTAL");
            $spreadsheet->getActiveSheet()->getStyle('B11')->applyFromArray($BgColorBajo);

            $spreadsheet->getActiveSheet()
                ->setCellValue("B12", '=COUNTIF(\'Categorias y dominios\'!'.'I3:I'.$lastRow.',"<15")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B13", '=COUNTIFS(\'Categorias y dominios\'!I3:I'.$lastRow.',">=15",\'Categorias y dominios\'!I3:I'.$lastRow.',"<21")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B14", '=COUNTIFS(\'Categorias y dominios\'!I3:I'.$lastRow.',">=21",\'Categorias y dominios\'!I3:I'.$lastRow.',"<27")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B15", '=COUNTIFS(\'Categorias y dominios\'!I3:I'.$lastRow.',">=27",\'Categorias y dominios\'!I3:I'.$lastRow.',"<37")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B16", '=COUNTIF(\'Categorias y dominios\'!I3:I'.$lastRow.',">=37")');

            $spreadsheet->getActiveSheet()->setCellValue("B17", "=SUM(B12:B16)");


            $spreadsheet->getActiveSheet()
                        ->setCellValue("C11", "VALOR");
            $spreadsheet->getActiveSheet()->getStyle('C11')->applyFromArray($BgColorAlto);

            $spreadsheet->getActiveSheet()->setCellValue("C12", "=(B12/B17)");
            $spreadsheet->getActiveSheet()->setCellValue("C13", "=(B13/B17)");
            $spreadsheet->getActiveSheet()->setCellValue("C14", "=(B14/B17)");
            $spreadsheet->getActiveSheet()->setCellValue("C15", "=(B15/B17)");
            $spreadsheet->getActiveSheet()->setCellValue("C16", "=(B16/B17)");
            $spreadsheet->getActiveSheet()->setCellValue("C17", "=SUM(C12:C16)");

            $spreadsheet->getActiveSheet()
                ->getStyle('C12:C17')
                ->getNumberFormat()
                ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE);


            $dataSeriesLabels1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'A12:A16', null, 1), // 2011
            ];
            $xAxisTickValues1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'A12:A16', null, 4), // Q1 to Q4
            ];
            $dataSeriesValues1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_NUMBER, 'C12:C16', null, 4, [], null, $colors),
            ];

            // Build the dataseries
            $series1 = new DataSeries(
                DataSeries::TYPE_PIECHART, // plotType
                null, // plotGrouping (Pie charts don't have any grouping)
                range(0, count($dataSeriesValues1) - 1), // plotOrder
                $dataSeriesLabels1, // plotLabel
                $xAxisTickValues1, // plotCategory
                $dataSeriesValues1          // plotValues
            );

            // Set up a layout object for the Pie chart
            $layout1 = new Layout();
            $layout1->setShowVal(FALSE);
            $layout1->setShowPercent(true);

            // Set the series in the plot area
            $plotArea1 = new PlotArea($layout1, [$series1]);
            // Set the chart legend
            $legend1 = new Legend(Legend::POSITION_BOTTOM, null, false);

            $title1 = new Title(strtoupper('Carga de trabajo'));

            // Create the chart
            $chart2 = new Chart(
                'chart2', // name
                $title1, // title
                $legend1, // legend
                $plotArea1, // plotArea
                true, // plotVisibleOnly
                0, // displayBlanksAs
                null, // xAxisLabel
                null   // yAxisLabel - Pie charts don't have a Y-Axis
            );

            // Set the position where the chart should appear in the worksheet
            $chart2->setTopLeftPosition('N1');
            $chart2->setBottomRightPosition('V14');
            $spreadsheet->getActiveSheet()->addChart($chart2);


            # --------------------------------------------------------------------------------------------------#
            ################### DOMINIO DE Falta de control sobre el trabajo ####################################

            $spreadsheet->getActiveSheet()->setCellValue("A19", strtoupper("Falta de control sobre el trabajo"))->mergeCells('A19:C19');
            $spreadsheet->getActiveSheet()->getStyle('A19')->applyFromArray($BgColorTitle);

            $spreadsheet->getActiveSheet()->setCellValue("A20", "RIESGO DE TRABAJO");
            $spreadsheet->getActiveSheet()->getStyle('A20')->applyFromArray($BgColorRiesgo);

            $spreadsheet->getActiveSheet()->setCellValue("A21", "NULO");
            $spreadsheet->getActiveSheet()->getStyle('A21')->applyFromArray($BgColorNulo);

            $spreadsheet->getActiveSheet()->setCellValue("A22", "BAJO");
            $spreadsheet->getActiveSheet()->getStyle('A22')->applyFromArray($BgColorBajo);

            $spreadsheet->getActiveSheet()->setCellValue("A23", "MEDIO");
            $spreadsheet->getActiveSheet()->getStyle('A23')->applyFromArray($BgColorMedio);

            $spreadsheet->getActiveSheet()->setCellValue("A24", "ALTO");
            $spreadsheet->getActiveSheet()->getStyle('A24')->applyFromArray($BgColorAlto);

            $spreadsheet->getActiveSheet()->setCellValue("A25", "MUY ALTO");
            $spreadsheet->getActiveSheet()->getStyle('A25')->applyFromArray($BgColorMuyAlto);


            $spreadsheet->getActiveSheet()
                        ->setCellValue("B20", "TOTAL");
            $spreadsheet->getActiveSheet()->getStyle('B20')->applyFromArray($BgColorBajo);

            $spreadsheet->getActiveSheet()
                ->setCellValue("B21", '=COUNTIF(\'Categorias y dominios\'!'.'J3:J'.$lastRow.',"<11")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B22", '=COUNTIFS(\'Categorias y dominios\'!J3:J'.$lastRow.',">=11",\'Categorias y dominios\'!J3:J'.$lastRow.',"<16")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B23", '=COUNTIFS(\'Categorias y dominios\'!J3:J'.$lastRow.',">=16",\'Categorias y dominios\'!J3:J'.$lastRow.',"<21")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B24", '=COUNTIFS(\'Categorias y dominios\'!J3:J'.$lastRow.',">=21",\'Categorias y dominios\'!J3:J'.$lastRow.',"<25")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B25", '=COUNTIF(\'Categorias y dominios\'!J3:J'.$lastRow.',">=25")');

            $spreadsheet->getActiveSheet()->setCellValue("B26", "=SUM(B21:B25)");


            $spreadsheet->getActiveSheet()
                        ->setCellValue("C20", "VALOR");
            $spreadsheet->getActiveSheet()->getStyle('C20')->applyFromArray($BgColorAlto);

            $spreadsheet->getActiveSheet()->setCellValue("C21", "=(B21/B26)");
            $spreadsheet->getActiveSheet()->setCellValue("C22", "=(B22/B26)");
            $spreadsheet->getActiveSheet()->setCellValue("C23", "=(B23/B26)");
            $spreadsheet->getActiveSheet()->setCellValue("C24", "=(B24/B26)");
            $spreadsheet->getActiveSheet()->setCellValue("C25", "=(B25/B26)");
            $spreadsheet->getActiveSheet()->setCellValue("C26", "=SUM(C21:C25)");

            $spreadsheet->getActiveSheet()
                ->getStyle('C21:C26')
                ->getNumberFormat()
                ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE);


            $dataSeriesLabels1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'A21:A25', null, 1), // 2011
            ];
            $xAxisTickValues1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'A21:A25', null, 4), // Q1 to Q4
            ];
            $dataSeriesValues1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_NUMBER, 'C21:C25', null, 4, [], null, $colors),
            ];

            // Build the dataseries
            $series1 = new DataSeries(
                DataSeries::TYPE_PIECHART, // plotType
                null, // plotGrouping (Pie charts don't have any grouping)
                range(0, count($dataSeriesValues1) - 1), // plotOrder
                $dataSeriesLabels1, // plotLabel
                $xAxisTickValues1, // plotCategory
                $dataSeriesValues1          // plotValues
            );

            // Set up a layout object for the Pie chart
            $layout1 = new Layout();
            $layout1->setShowVal(FALSE);
            $layout1->setShowPercent(true);

            // Set the series in the plot area
            $plotArea1 = new PlotArea($layout1, [$series1]);
            // Set the chart legend
            $legend1 = new Legend(Legend::POSITION_BOTTOM, null, false);

            $title1 = new Title(strtoupper('Falta de control sobre el trabajo'));

            // Create the chart
            $chart3 = new Chart(
                'chart3', // name
                $title1, // title
                $legend1, // legend
                $plotArea1, // plotArea
                true, // plotVisibleOnly
                0, // displayBlanksAs
                null, // xAxisLabel
                null   // yAxisLabel - Pie charts don't have a Y-Axis
            );

            // Set the position where the chart should appear in the worksheet
            $chart3->setTopLeftPosition('W1');
            $chart3->setBottomRightPosition('AE14');
            $spreadsheet->getActiveSheet()->addChart($chart3);

            # --------------------------------------------------------------------------------------------------#
            ################### DOMINIO Jornada de trabajo ##################################

            $spreadsheet->getActiveSheet()->setCellValue("A28", strtoupper("Jornada de trabajo"))->mergeCells('A28:C28');
            $spreadsheet->getActiveSheet()->getStyle('A28')->applyFromArray($BgColorTitle);

            $spreadsheet->getActiveSheet()->setCellValue("A29", "RIESGO DE TRABAJO");
            $spreadsheet->getActiveSheet()->getStyle('A29')->applyFromArray($BgColorRiesgo);

            $spreadsheet->getActiveSheet()->setCellValue("A30", "NULO");
            $spreadsheet->getActiveSheet()->getStyle('A30')->applyFromArray($BgColorNulo);

            $spreadsheet->getActiveSheet()->setCellValue("A31", "BAJO");
            $spreadsheet->getActiveSheet()->getStyle('A31')->applyFromArray($BgColorBajo);

            $spreadsheet->getActiveSheet()->setCellValue("A32", "MEDIO");
            $spreadsheet->getActiveSheet()->getStyle('A32')->applyFromArray($BgColorMedio);

            $spreadsheet->getActiveSheet()->setCellValue("A33", "ALTO");
            $spreadsheet->getActiveSheet()->getStyle('A33')->applyFromArray($BgColorAlto);

            $spreadsheet->getActiveSheet()->setCellValue("A34", "MUY ALTO");
            $spreadsheet->getActiveSheet()->getStyle('A34')->applyFromArray($BgColorMuyAlto);


            $spreadsheet->getActiveSheet()
                        ->setCellValue("B29", "TOTAL");
            $spreadsheet->getActiveSheet()->getStyle('B29')->applyFromArray($BgColorBajo);

            $spreadsheet->getActiveSheet()
                ->setCellValue("B30", '=COUNTIF(\'Categorias y dominios\'!'.'K3:K'.$lastRow.',"<1")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B31", '=COUNTIFS(\'Categorias y dominios\'!K3:K'.$lastRow.',">=1",\'Categorias y dominios\'!K3:K'.$lastRow.',"<2")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B32", '=COUNTIFS(\'Categorias y dominios\'!K3:K'.$lastRow.',">=2",\'Categorias y dominios\'!K3:K'.$lastRow.',"<4")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B33", '=COUNTIFS(\'Categorias y dominios\'!K3:K'.$lastRow.',">=4",\'Categorias y dominios\'!K3:K'.$lastRow.',"<6")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B34", '=COUNTIF(\'Categorias y dominios\'!K3:K'.$lastRow.',">=6")');

            $spreadsheet->getActiveSheet()->setCellValue("B35", "=SUM(B30:B34)");


            $spreadsheet->getActiveSheet()
                        ->setCellValue("C29", "VALOR");
            $spreadsheet->getActiveSheet()->getStyle('C29')->applyFromArray($BgColorAlto);

            $spreadsheet->getActiveSheet()->setCellValue("C30", "=(B30/B35)");
            $spreadsheet->getActiveSheet()->setCellValue("C31", "=(B31/B35)");
            $spreadsheet->getActiveSheet()->setCellValue("C32", "=(B32/B35)");
            $spreadsheet->getActiveSheet()->setCellValue("C33", "=(B33/B35)");
            $spreadsheet->getActiveSheet()->setCellValue("C34", "=(B34/B35)");
            $spreadsheet->getActiveSheet()->setCellValue("C35", "=SUM(C30:C34)");

            $spreadsheet->getActiveSheet()
                ->getStyle('C30:C35')
                ->getNumberFormat()
                ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE);


            $dataSeriesLabels1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'A30:A34', null, 1), // 2011
            ];
            $xAxisTickValues1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'A30:A34', null, 4), // Q1 to Q4
            ];
            $dataSeriesValues1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_NUMBER, 'C30:C34', null, 4, [], null, $colors),
            ];

            // Build the dataseries
            $series1 = new DataSeries(
                DataSeries::TYPE_PIECHART, // plotType
                null, // plotGrouping (Pie charts don't have any grouping)
                range(0, count($dataSeriesValues1) - 1), // plotOrder
                $dataSeriesLabels1, // plotLabel
                $xAxisTickValues1, // plotCategory
                $dataSeriesValues1          // plotValues
            );

            // Set up a layout object for the Pie chart
            $layout1 = new Layout();
            $layout1->setShowVal(FALSE);
            $layout1->setShowPercent(true);

            // Set the series in the plot area
            $plotArea1 = new PlotArea($layout1, [$series1]);
            // Set the chart legend
            $legend1 = new Legend(Legend::POSITION_BOTTOM, null, false);

            $title1 = new Title(strtoupper('Jornada de trabajo'));

            // Create the chart
            $chart4 = new Chart(
                'chart4', // name
                $title1, // title
                $legend1, // legend
                $plotArea1, // plotArea
                true, // plotVisibleOnly
                0, // displayBlanksAs
                null, // xAxisLabel
                null   // yAxisLabel - Pie charts don't have a Y-Axis
            );

            // Set the position where the chart should appear in the worksheet
            $chart4->setTopLeftPosition('D16');
            $chart4->setBottomRightPosition('M30');
            $spreadsheet->getActiveSheet()->addChart($chart4);

            # --------------------------------------------------------------------------------------------------#
            ################### DOMINIO Interferencia en la relación trabajo-familia ##################################

            $spreadsheet->getActiveSheet()->setCellValue("A37", strtoupper("Interferencia en la relación trabajo-familia"))->mergeCells('A37:C37');
            $spreadsheet->getActiveSheet()->getStyle('A37')->applyFromArray($BgColorTitle);

            $spreadsheet->getActiveSheet()->setCellValue("A38", "RIESGO DE TRABAJO");
            $spreadsheet->getActiveSheet()->getStyle('A38')->applyFromArray($BgColorRiesgo);

            $spreadsheet->getActiveSheet()->setCellValue("A39", "NULO");
            $spreadsheet->getActiveSheet()->getStyle('A39')->applyFromArray($BgColorNulo);

            $spreadsheet->getActiveSheet()->setCellValue("A40", "BAJO");
            $spreadsheet->getActiveSheet()->getStyle('A40')->applyFromArray($BgColorBajo);

            $spreadsheet->getActiveSheet()->setCellValue("A41", "MEDIO");
            $spreadsheet->getActiveSheet()->getStyle('A41')->applyFromArray($BgColorMedio);

            $spreadsheet->getActiveSheet()->setCellValue("A42", "ALTO");
            $spreadsheet->getActiveSheet()->getStyle('A42')->applyFromArray($BgColorAlto);

            $spreadsheet->getActiveSheet()->setCellValue("A43", "MUY ALTO");
            $spreadsheet->getActiveSheet()->getStyle('A43')->applyFromArray($BgColorMuyAlto);


            $spreadsheet->getActiveSheet()
                        ->setCellValue("B38", "TOTAL");
            $spreadsheet->getActiveSheet()->getStyle('B38')->applyFromArray($BgColorBajo);

            $spreadsheet->getActiveSheet()
                ->setCellValue("B39", '=COUNTIF(\'Categorias y dominios\'!'.'L3:L'.$lastRow.',"<4")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B40", '=COUNTIFS(\'Categorias y dominios\'!L3:L'.$lastRow.',">=4",\'Categorias y dominios\'!L3:L'.$lastRow.',"<6")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B41", '=COUNTIFS(\'Categorias y dominios\'!L3:L'.$lastRow.',">=6",\'Categorias y dominios\'!L3:L'.$lastRow.',"<8")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B42", '=COUNTIFS(\'Categorias y dominios\'!L3:L'.$lastRow.',">=8",\'Categorias y dominios\'!L3:L'.$lastRow.',"<10")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B43", '=COUNTIF(\'Categorias y dominios\'!L3:L'.$lastRow.',">=10")');

            $spreadsheet->getActiveSheet()->setCellValue("B44", "=SUM(B39:B43)");


            $spreadsheet->getActiveSheet()
                        ->setCellValue("C38", "VALOR");
            $spreadsheet->getActiveSheet()->getStyle('C38')->applyFromArray($BgColorAlto);

            $spreadsheet->getActiveSheet()->setCellValue("C39", "=(B39/B44)");
            $spreadsheet->getActiveSheet()->setCellValue("C40", "=(B40/B44)");
            $spreadsheet->getActiveSheet()->setCellValue("C41", "=(B41/B44)");
            $spreadsheet->getActiveSheet()->setCellValue("C42", "=(B42/B44)");
            $spreadsheet->getActiveSheet()->setCellValue("C43", "=(B43/B44)");
            $spreadsheet->getActiveSheet()->setCellValue("C44", "=SUM(C39:C43)");

            $spreadsheet->getActiveSheet()
                ->getStyle('C39:C44')
                ->getNumberFormat()
                ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE);


            $dataSeriesLabels1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'A39:A43', null, 1), // 2011
            ];
            $xAxisTickValues1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'A39:A43', null, 4), // Q1 to Q4
            ];
            $dataSeriesValues1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_NUMBER, 'C39:C43', null, 4, [], null, $colors),
            ];

            // Build the dataseries
            $series1 = new DataSeries(
                DataSeries::TYPE_PIECHART, // plotType
                null, // plotGrouping (Pie charts don't have any grouping)
                range(0, count($dataSeriesValues1) - 1), // plotOrder
                $dataSeriesLabels1, // plotLabel
                $xAxisTickValues1, // plotCategory
                $dataSeriesValues1          // plotValues
            );

            // Set up a layout object for the Pie chart
            $layout1 = new Layout();
            $layout1->setShowVal(FALSE);
            $layout1->setShowPercent(true);

            // Set the series in the plot area
            $plotArea1 = new PlotArea($layout1, [$series1]);
            // Set the chart legend
            $legend1 = new Legend(Legend::POSITION_BOTTOM, null, false);

            $title1 = new Title(strtoupper('Interferencia en la relación trabajo-familia'));

            // Create the chart
            $chart5 = new Chart(
                'chart5', // name
                $title1, // title
                $legend1, // legend
                $plotArea1, // plotArea
                true, // plotVisibleOnly
                0, // displayBlanksAs
                null, // xAxisLabel
                null   // yAxisLabel - Pie charts don't have a Y-Axis
            );

            // Set the position where the chart should appear in the worksheet
            $chart5->setTopLeftPosition('N16');
            $chart5->setBottomRightPosition('V30');
            $spreadsheet->getActiveSheet()->addChart($chart5);

            # --------------------------------------------------------------------------------------------------#
            ################### DOMINIO Liderazgo ##################################

            $spreadsheet->getActiveSheet()->setCellValue("A46", strtoupper("Liderazgo"))->mergeCells('A46:C46');
            $spreadsheet->getActiveSheet()->getStyle('A46')->applyFromArray($BgColorTitle);

            $spreadsheet->getActiveSheet()->setCellValue("A47", "RIESGO DE TRABAJO");
            $spreadsheet->getActiveSheet()->getStyle('A47')->applyFromArray($BgColorRiesgo);

            $spreadsheet->getActiveSheet()->setCellValue("A48", "NULO");
            $spreadsheet->getActiveSheet()->getStyle('A48')->applyFromArray($BgColorNulo);

            $spreadsheet->getActiveSheet()->setCellValue("A49", "BAJO");
            $spreadsheet->getActiveSheet()->getStyle('A49')->applyFromArray($BgColorBajo);

            $spreadsheet->getActiveSheet()->setCellValue("A50", "MEDIO");
            $spreadsheet->getActiveSheet()->getStyle('A50')->applyFromArray($BgColorMedio);

            $spreadsheet->getActiveSheet()->setCellValue("A51", "ALTO");
            $spreadsheet->getActiveSheet()->getStyle('A51')->applyFromArray($BgColorAlto);

            $spreadsheet->getActiveSheet()->setCellValue("A52", "MUY ALTO");
            $spreadsheet->getActiveSheet()->getStyle('A52')->applyFromArray($BgColorMuyAlto);


            $spreadsheet->getActiveSheet()
                        ->setCellValue("B47", "TOTAL");
            $spreadsheet->getActiveSheet()->getStyle('B47')->applyFromArray($BgColorBajo);

            $spreadsheet->getActiveSheet()
                ->setCellValue("B48", '=COUNTIF(\'Categorias y dominios\'!'.'M3:M'.$lastRow.',"<9")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B49", '=COUNTIFS(\'Categorias y dominios\'!M3:M'.$lastRow.',">=9",\'Categorias y dominios\'!M3:M'.$lastRow.',"<12")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B50", '=COUNTIFS(\'Categorias y dominios\'!M3:M'.$lastRow.',">=12",\'Categorias y dominios\'!M3:M'.$lastRow.',"<16")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B51", '=COUNTIFS(\'Categorias y dominios\'!M3:M'.$lastRow.',">=16",\'Categorias y dominios\'!M3:M'.$lastRow.',"<20")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B52", '=COUNTIF(\'Categorias y dominios\'!M3:M'.$lastRow.',">=20")');

            $spreadsheet->getActiveSheet()->setCellValue("B53", "=SUM(B48:B52)");


            $spreadsheet->getActiveSheet()
                        ->setCellValue("C47", "VALOR");
            $spreadsheet->getActiveSheet()->getStyle('C47')->applyFromArray($BgColorAlto);

            $spreadsheet->getActiveSheet()->setCellValue("C48", "=(B48/B53)");
            $spreadsheet->getActiveSheet()->setCellValue("C49", "=(B49/B53)");
            $spreadsheet->getActiveSheet()->setCellValue("C50", "=(B50/B53)");
            $spreadsheet->getActiveSheet()->setCellValue("C51", "=(B51/B53)");
            $spreadsheet->getActiveSheet()->setCellValue("C52", "=(B52/B53)");
            $spreadsheet->getActiveSheet()->setCellValue("C53", "=SUM(C48:C52)");

            $spreadsheet->getActiveSheet()
                ->getStyle('C48:C53')
                ->getNumberFormat()
                ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE);


            $dataSeriesLabels1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'A48:A52', null, 1), // 2011
            ];
            $xAxisTickValues1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'A48:A52', null, 4), // Q1 to Q4
            ];
            $dataSeriesValues1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_NUMBER, 'C48:C52', null, 4, [], null, $colors),
            ];

            // Build the dataseries
            $series1 = new DataSeries(
                DataSeries::TYPE_PIECHART, // plotType
                null, // plotGrouping (Pie charts don't have any grouping)
                range(0, count($dataSeriesValues1) - 1), // plotOrder
                $dataSeriesLabels1, // plotLabel
                $xAxisTickValues1, // plotCategory
                $dataSeriesValues1          // plotValues
            );

            // Set up a layout object for the Pie chart
            $layout1 = new Layout();
            $layout1->setShowVal(FALSE);
            $layout1->setShowPercent(true);

            // Set the series in the plot area
            $plotArea1 = new PlotArea($layout1, [$series1]);
            // Set the chart legend
            $legend1 = new Legend(Legend::POSITION_BOTTOM, null, false);

            $title1 = new Title(strtoupper('Liderazgo'));

            // Create the chart
            $chart6 = new Chart(
                'chart6', // name
                $title1, // title
                $legend1, // legend
                $plotArea1, // plotArea
                true, // plotVisibleOnly
                0, // displayBlanksAs
                null, // xAxisLabel
                null   // yAxisLabel - Pie charts don't have a Y-Axis
            );

            // Set the position where the chart should appear in the worksheet
            $chart6->setTopLeftPosition('W16');
            $chart6->setBottomRightPosition('AE30');
            $spreadsheet->getActiveSheet()->addChart($chart6);

            # --------------------------------------------------------------------------------------------------#
            ################### DOMINIO Relaciones en el trabajo ##################################

            $spreadsheet->getActiveSheet()->setCellValue("A55", strtoupper("Relaciones en el trabajo"))->mergeCells('A55:C55');
            $spreadsheet->getActiveSheet()->getStyle('A55')->applyFromArray($BgColorTitle);

            $spreadsheet->getActiveSheet()->setCellValue("A56", "RIESGO DE TRABAJO");
            $spreadsheet->getActiveSheet()->getStyle('A56')->applyFromArray($BgColorRiesgo);

            $spreadsheet->getActiveSheet()->setCellValue("A57", "NULO");
            $spreadsheet->getActiveSheet()->getStyle('A57')->applyFromArray($BgColorNulo);

            $spreadsheet->getActiveSheet()->setCellValue("A58", "BAJO");
            $spreadsheet->getActiveSheet()->getStyle('A58')->applyFromArray($BgColorBajo);

            $spreadsheet->getActiveSheet()->setCellValue("A59", "MEDIO");
            $spreadsheet->getActiveSheet()->getStyle('A59')->applyFromArray($BgColorMedio);

            $spreadsheet->getActiveSheet()->setCellValue("A60", "ALTO");
            $spreadsheet->getActiveSheet()->getStyle('A60')->applyFromArray($BgColorAlto);

            $spreadsheet->getActiveSheet()->setCellValue("A61", "MUY ALTO");
            $spreadsheet->getActiveSheet()->getStyle('A61')->applyFromArray($BgColorMuyAlto);


            $spreadsheet->getActiveSheet()
                        ->setCellValue("B56", "TOTAL");
            $spreadsheet->getActiveSheet()->getStyle('B56')->applyFromArray($BgColorBajo);

            $spreadsheet->getActiveSheet()
                ->setCellValue("B57", '=COUNTIF(\'Categorias y dominios\'!'.'N3:N'.$lastRow.',"<10")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B58", '=COUNTIFS(\'Categorias y dominios\'!N3:N'.$lastRow.',">=10",\'Categorias y dominios\'!N3:N'.$lastRow.',"<13")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B59", '=COUNTIFS(\'Categorias y dominios\'!N3:N'.$lastRow.',">=13",\'Categorias y dominios\'!N3:N'.$lastRow.',"<17")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B60", '=COUNTIFS(\'Categorias y dominios\'!N3:N'.$lastRow.',">=17",\'Categorias y dominios\'!N3:N'.$lastRow.',"<21")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B61", '=COUNTIF(\'Categorias y dominios\'!N3:N'.$lastRow.',">=21")');

            $spreadsheet->getActiveSheet()->setCellValue("B62", "=SUM(B57:B61)");


            $spreadsheet->getActiveSheet()
                        ->setCellValue("C56", "VALOR");
            $spreadsheet->getActiveSheet()->getStyle('C56')->applyFromArray($BgColorAlto);

            $spreadsheet->getActiveSheet()->setCellValue("C57", "=(B57/B62)");
            $spreadsheet->getActiveSheet()->setCellValue("C58", "=(B58/B62)");
            $spreadsheet->getActiveSheet()->setCellValue("C59", "=(B59/B62)");
            $spreadsheet->getActiveSheet()->setCellValue("C60", "=(B60/B62)");
            $spreadsheet->getActiveSheet()->setCellValue("C61", "=(B61/B62)");
            $spreadsheet->getActiveSheet()->setCellValue("C62", "=SUM(C57:C61)");

            $spreadsheet->getActiveSheet()
                ->getStyle('C57:C62')
                ->getNumberFormat()
                ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE);


            $dataSeriesLabels1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'A57:A61', null, 1), // 2011
            ];
            $xAxisTickValues1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'A57:A61', null, 4), // Q1 to Q4
            ];
            $dataSeriesValues1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_NUMBER, 'C57:C61', null, 4, [], null, $colors),
            ];

            // Build the dataseries
            $series1 = new DataSeries(
                DataSeries::TYPE_PIECHART, // plotType
                null, // plotGrouping (Pie charts don't have any grouping)
                range(0, count($dataSeriesValues1) - 1), // plotOrder
                $dataSeriesLabels1, // plotLabel
                $xAxisTickValues1, // plotCategory
                $dataSeriesValues1          // plotValues
            );

            // Set up a layout object for the Pie chart
            $layout1 = new Layout();
            $layout1->setShowVal(FALSE);
            $layout1->setShowPercent(true);

            // Set the series in the plot area
            $plotArea1 = new PlotArea($layout1, [$series1]);
            // Set the chart legend
            $legend1 = new Legend(Legend::POSITION_BOTTOM, null, false);

            $title1 = new Title(strtoupper('Relaciones en el trabajo'));

            // Create the chart
            $chart7 = new Chart(
                'chart7', // name
                $title1, // title
                $legend1, // legend
                $plotArea1, // plotArea
                true, // plotVisibleOnly
                0, // displayBlanksAs
                null, // xAxisLabel
                null   // yAxisLabel - Pie charts don't have a Y-Axis
            );

            // Set the position where the chart should appear in the worksheet
            $chart7->setTopLeftPosition('D32');
            $chart7->setBottomRightPosition('M47');
            $spreadsheet->getActiveSheet()->addChart($chart7);

            # --------------------------------------------------------------------------------------------------#
            ################### DOMINIO Violencia ##################################

            $spreadsheet->getActiveSheet()->setCellValue("A64", strtoupper("Violencia"))->mergeCells('A64:C64');
            $spreadsheet->getActiveSheet()->getStyle('A64')->applyFromArray($BgColorTitle);

            $spreadsheet->getActiveSheet()->setCellValue("A65", "RIESGO DE TRABAJO");
            $spreadsheet->getActiveSheet()->getStyle('A65')->applyFromArray($BgColorRiesgo);

            $spreadsheet->getActiveSheet()->setCellValue("A66", "NULO");
            $spreadsheet->getActiveSheet()->getStyle('A66')->applyFromArray($BgColorNulo);

            $spreadsheet->getActiveSheet()->setCellValue("A67", "BAJO");
            $spreadsheet->getActiveSheet()->getStyle('A67')->applyFromArray($BgColorBajo);

            $spreadsheet->getActiveSheet()->setCellValue("A68", "MEDIO");
            $spreadsheet->getActiveSheet()->getStyle('A68')->applyFromArray($BgColorMedio);

            $spreadsheet->getActiveSheet()->setCellValue("A69", "ALTO");
            $spreadsheet->getActiveSheet()->getStyle('A69')->applyFromArray($BgColorAlto);

            $spreadsheet->getActiveSheet()->setCellValue("A70", "MUY ALTO");
            $spreadsheet->getActiveSheet()->getStyle('A70')->applyFromArray($BgColorMuyAlto);


            $spreadsheet->getActiveSheet()
                        ->setCellValue("B65", "TOTAL");
            $spreadsheet->getActiveSheet()->getStyle('B65')->applyFromArray($BgColorBajo);

            $spreadsheet->getActiveSheet()
                ->setCellValue("B66", '=COUNTIF(\'Categorias y dominios\'!'.'O3:O'.$lastRow.',"<7")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B67", '=COUNTIFS(\'Categorias y dominios\'!O3:O'.$lastRow.',">=7",\'Categorias y dominios\'!O3:O'.$lastRow.',"<10")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B68", '=COUNTIFS(\'Categorias y dominios\'!O3:O'.$lastRow.',">=10",\'Categorias y dominios\'!O3:O'.$lastRow.',"<13")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B69", '=COUNTIFS(\'Categorias y dominios\'!O3:O'.$lastRow.',">=13",\'Categorias y dominios\'!O3:O'.$lastRow.',"<16")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B70", '=COUNTIF(\'Categorias y dominios\'!O3:O'.$lastRow.',">=16")');

            $spreadsheet->getActiveSheet()->setCellValue("B71", "=SUM(B66:B70)");


            $spreadsheet->getActiveSheet()
                        ->setCellValue("C65", "VALOR");
            $spreadsheet->getActiveSheet()->getStyle('C65')->applyFromArray($BgColorAlto);

            $spreadsheet->getActiveSheet()->setCellValue("C66", "=(B66/B71)");
            $spreadsheet->getActiveSheet()->setCellValue("C67", "=(B67/B71)");
            $spreadsheet->getActiveSheet()->setCellValue("C68", "=(B68/B71)");
            $spreadsheet->getActiveSheet()->setCellValue("C69", "=(B69/B71)");
            $spreadsheet->getActiveSheet()->setCellValue("C70", "=(B70/B71)");
            $spreadsheet->getActiveSheet()->setCellValue("C71", "=SUM(C66:C70)");

            $spreadsheet->getActiveSheet()
                ->getStyle('C66:C71')
                ->getNumberFormat()
                ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE);


            $dataSeriesLabels1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'A66:A70', null, 1), // 2011
            ];
            $xAxisTickValues1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'A66:A70', null, 4), // Q1 to Q4
            ];
            $dataSeriesValues1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_NUMBER, 'C66:C70', null, 4, [], null, $colors),
            ];

            // Build the dataseries
            $series1 = new DataSeries(
                DataSeries::TYPE_PIECHART, // plotType
                null, // plotGrouping (Pie charts don't have any grouping)
                range(0, count($dataSeriesValues1) - 1), // plotOrder
                $dataSeriesLabels1, // plotLabel
                $xAxisTickValues1, // plotCategory
                $dataSeriesValues1          // plotValues
            );

            // Set up a layout object for the Pie chart
            $layout1 = new Layout();
            $layout1->setShowVal(FALSE);
            $layout1->setShowPercent(true);

            // Set the series in the plot area
            $plotArea1 = new PlotArea($layout1, [$series1]);
            // Set the chart legend
            $legend1 = new Legend(Legend::POSITION_BOTTOM, null, false);

            $title1 = new Title(strtoupper('Violencia'));

            // Create the chart
            $chart8 = new Chart(
                'chart8', // name
                $title1, // title
                $legend1, // legend
                $plotArea1, // plotArea
                true, // plotVisibleOnly
                0, // displayBlanksAs
                null, // xAxisLabel
                null   // yAxisLabel - Pie charts don't have a Y-Axis
            );

            // Set the position where the chart should appear in the worksheet
            $chart8->setTopLeftPosition('N32');
            $chart8->setBottomRightPosition('V47');
            $spreadsheet->getActiveSheet()->addChart($chart8);

            # --------------------------------------------------------------------------------------------------#
            ################### DOMINIO Reconocimiento del desempeño ##################################

            $spreadsheet->getActiveSheet()->setCellValue("A73", strtoupper("Reconocimiento del desempeño"))->mergeCells('A73:C73');
            $spreadsheet->getActiveSheet()->getStyle('A73')->applyFromArray($BgColorTitle);

            $spreadsheet->getActiveSheet()->setCellValue("A74", "RIESGO DE TRABAJO");
            $spreadsheet->getActiveSheet()->getStyle('A74')->applyFromArray($BgColorRiesgo);

            $spreadsheet->getActiveSheet()->setCellValue("A75", "NULO");
            $spreadsheet->getActiveSheet()->getStyle('A75')->applyFromArray($BgColorNulo);

            $spreadsheet->getActiveSheet()->setCellValue("A76", "BAJO");
            $spreadsheet->getActiveSheet()->getStyle('A76')->applyFromArray($BgColorBajo);

            $spreadsheet->getActiveSheet()->setCellValue("A77", "MEDIO");
            $spreadsheet->getActiveSheet()->getStyle('A77')->applyFromArray($BgColorMedio);

            $spreadsheet->getActiveSheet()->setCellValue("A78", "ALTO");
            $spreadsheet->getActiveSheet()->getStyle('A78')->applyFromArray($BgColorAlto);

            $spreadsheet->getActiveSheet()->setCellValue("A79", "MUY ALTO");
            $spreadsheet->getActiveSheet()->getStyle('A79')->applyFromArray($BgColorMuyAlto);


            $spreadsheet->getActiveSheet()
                        ->setCellValue("B74", "TOTAL");
            $spreadsheet->getActiveSheet()->getStyle('B74')->applyFromArray($BgColorBajo);

            $spreadsheet->getActiveSheet()
                ->setCellValue("B75", '=COUNTIF(\'Categorias y dominios\'!'.'P3:P'.$lastRow.',"<6")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B76", '=COUNTIFS(\'Categorias y dominios\'!P3:P'.$lastRow.',">=6",\'Categorias y dominios\'!P3:P'.$lastRow.',"<10")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B77", '=COUNTIFS(\'Categorias y dominios\'!P3:P'.$lastRow.',">=10",\'Categorias y dominios\'!P3:P'.$lastRow.',"<14")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B78", '=COUNTIFS(\'Categorias y dominios\'!P3:P'.$lastRow.',">=14",\'Categorias y dominios\'!P3:P'.$lastRow.',"<18")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B79", '=COUNTIF(\'Categorias y dominios\'!P3:P'.$lastRow.',">=18")');

            $spreadsheet->getActiveSheet()->setCellValue("B80", "=SUM(B75:B79)");


            $spreadsheet->getActiveSheet()
                        ->setCellValue("C74", "VALOR");
            $spreadsheet->getActiveSheet()->getStyle('C74')->applyFromArray($BgColorAlto);

            $spreadsheet->getActiveSheet()->setCellValue("C75", "=(B75/B80)");
            $spreadsheet->getActiveSheet()->setCellValue("C76", "=(B76/B80)");
            $spreadsheet->getActiveSheet()->setCellValue("C77", "=(B77/B80)");
            $spreadsheet->getActiveSheet()->setCellValue("C78", "=(B78/B80)");
            $spreadsheet->getActiveSheet()->setCellValue("C79", "=(B79/B80)");
            $spreadsheet->getActiveSheet()->setCellValue("C80", "=SUM(C75:C79)");

            $spreadsheet->getActiveSheet()
                ->getStyle('C75:C80')
                ->getNumberFormat()
                ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE);


            $dataSeriesLabels1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'A75:A79', null, 1), // 2011
            ];
            $xAxisTickValues1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'A75:A79', null, 4), // Q1 to Q4
            ];
            $dataSeriesValues1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_NUMBER, 'C75:C79', null, 4, [], null, $colors),
            ];

            // Build the dataseries
            $series1 = new DataSeries(
                DataSeries::TYPE_PIECHART, // plotType
                null, // plotGrouping (Pie charts don't have any grouping)
                range(0, count($dataSeriesValues1) - 1), // plotOrder
                $dataSeriesLabels1, // plotLabel
                $xAxisTickValues1, // plotCategory
                $dataSeriesValues1          // plotValues
            );

            // Set up a layout object for the Pie chart
            $layout1 = new Layout();
            $layout1->setShowVal(FALSE);
            $layout1->setShowPercent(true);

            // Set the series in the plot area
            $plotArea1 = new PlotArea($layout1, [$series1]);
            // Set the chart legend
            $legend1 = new Legend(Legend::POSITION_BOTTOM, null, false);

            $title1 = new Title(strtoupper('Reconocimiento del desempeño'));

            // Create the chart
            $chart9 = new Chart(
                'chart9', // name
                $title1, // title
                $legend1, // legend
                $plotArea1, // plotArea
                true, // plotVisibleOnly
                0, // displayBlanksAs
                null, // xAxisLabel
                null   // yAxisLabel - Pie charts don't have a Y-Axis
            );

            // Set the position where the chart should appear in the worksheet
            $chart9->setTopLeftPosition('W32');
            $chart9->setBottomRightPosition('AE47');
            $spreadsheet->getActiveSheet()->addChart($chart9);

            # --------------------------------------------------------------------------------------------------#
            ################### DOMINIO Insuficiente sentido de pertenencia e, inestabilidad ##################################

            $spreadsheet->getActiveSheet()->setCellValue("A82", strtoupper("Insuficiente sentido de pertenencia e, inestabilidad"))->mergeCells('A82:C82');
            $spreadsheet->getActiveSheet()->getStyle('A82')->applyFromArray($BgColorTitle);

            $spreadsheet->getActiveSheet()->setCellValue("A83", "RIESGO DE TRABAJO");
            $spreadsheet->getActiveSheet()->getStyle('A83')->applyFromArray($BgColorRiesgo);

            $spreadsheet->getActiveSheet()->setCellValue("A84", "NULO");
            $spreadsheet->getActiveSheet()->getStyle('A84')->applyFromArray($BgColorNulo);

            $spreadsheet->getActiveSheet()->setCellValue("A85", "BAJO");
            $spreadsheet->getActiveSheet()->getStyle('A85')->applyFromArray($BgColorBajo);

            $spreadsheet->getActiveSheet()->setCellValue("A86", "MEDIO");
            $spreadsheet->getActiveSheet()->getStyle('A86')->applyFromArray($BgColorMedio);

            $spreadsheet->getActiveSheet()->setCellValue("A87", "ALTO");
            $spreadsheet->getActiveSheet()->getStyle('A87')->applyFromArray($BgColorAlto);

            $spreadsheet->getActiveSheet()->setCellValue("A88", "MUY ALTO");
            $spreadsheet->getActiveSheet()->getStyle('A88')->applyFromArray($BgColorMuyAlto);


            $spreadsheet->getActiveSheet()
                        ->setCellValue("B83", "TOTAL");
            $spreadsheet->getActiveSheet()->getStyle('B83')->applyFromArray($BgColorBajo);

            $spreadsheet->getActiveSheet()
                ->setCellValue("B84", '=COUNTIF(\'Categorias y dominios\'!'.'Q3:Q'.$lastRow.',"<4")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B85", '=COUNTIFS(\'Categorias y dominios\'!Q3:Q'.$lastRow.',">=4",\'Categorias y dominios\'!Q3:Q'.$lastRow.',"<6")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B86", '=COUNTIFS(\'Categorias y dominios\'!Q3:Q'.$lastRow.',">=6",\'Categorias y dominios\'!Q3:Q'.$lastRow.',"<8")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B87", '=COUNTIFS(\'Categorias y dominios\'!Q3:Q'.$lastRow.',">=8",\'Categorias y dominios\'!Q3:Q'.$lastRow.',"<10")');
            $spreadsheet->getActiveSheet()
                ->setCellValue("B88", '=COUNTIF(\'Categorias y dominios\'!Q3:Q'.$lastRow.',">=10")');

            $spreadsheet->getActiveSheet()->setCellValue("B89", "=SUM(B84:B88)");


            $spreadsheet->getActiveSheet()
                        ->setCellValue("C83", "VALOR");
            $spreadsheet->getActiveSheet()->getStyle('C83')->applyFromArray($BgColorAlto);

            $spreadsheet->getActiveSheet()->setCellValue("C84", "=(B84/B89)");
            $spreadsheet->getActiveSheet()->setCellValue("C85", "=(B85/B89)");
            $spreadsheet->getActiveSheet()->setCellValue("C86", "=(B86/B89)");
            $spreadsheet->getActiveSheet()->setCellValue("C87", "=(B87/B89)");
            $spreadsheet->getActiveSheet()->setCellValue("C88", "=(B88/B89)");
            $spreadsheet->getActiveSheet()->setCellValue("C89", "=SUM(C84:C88)");

            $spreadsheet->getActiveSheet()
                ->getStyle('C84:C89')
                ->getNumberFormat()
                ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE);


            $dataSeriesLabels1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'A84:A88', null, 1), // 2011
            ];
            $xAxisTickValues1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'A84:A88', null, 4), // Q1 to Q4
            ];
            $dataSeriesValues1 = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_NUMBER, 'C84:C88', null, 4, [], null, $colors),
            ];

            // Build the dataseries
            $series1 = new DataSeries(
                DataSeries::TYPE_PIECHART, // plotType
                null, // plotGrouping (Pie charts don't have any grouping)
                range(0, count($dataSeriesValues1) - 1), // plotOrder
                $dataSeriesLabels1, // plotLabel
                $xAxisTickValues1, // plotCategory
                $dataSeriesValues1          // plotValues
            );

            // Set up a layout object for the Pie chart
            $layout1 = new Layout();
            $layout1->setShowVal(FALSE);
            $layout1->setShowPercent(true);

            // Set the series in the plot area
            $plotArea1 = new PlotArea($layout1, [$series1]);
            // Set the chart legend
            $legend1 = new Legend(Legend::POSITION_BOTTOM, null, false);

            $title1 = new Title(strtoupper('Insuficiente sentido de pertenencia e, inestabilidad'));

            // Create the chart
            $chart10 = new Chart(
                'chart10', // name
                $title1, // title
                $legend1, // legend
                $plotArea1, // plotArea
                true, // plotVisibleOnly
                0, // displayBlanksAs
                null, // xAxisLabel
                null   // yAxisLabel - Pie charts don't have a Y-Axis
            );

            // Set the position where the chart should appear in the worksheet
            $chart10->setTopLeftPosition('D50');
            $chart10->setBottomRightPosition('M65');
            $spreadsheet->getActiveSheet()->addChart($chart10);

            ###### FIN GRAFICAS POR DOMINIOS
            //$styleArray esta declarada en la hoja de calificacion total
            $spreadsheet->getActiveSheet()->getStyle('A1:C8')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A3:C8')->applyFromArray(['font'=>['size'=>10]]);

            $spreadsheet->getActiveSheet()->getStyle('A10:C17')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A12:C17')->applyFromArray(['font'=>['size'=>10]]);

            $spreadsheet->getActiveSheet()->getStyle('A19:C26')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A22:C26')->applyFromArray(['font'=>['size'=>10]]);

            $spreadsheet->getActiveSheet()->getStyle('A28:C35')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A30:C35')->applyFromArray(['font'=>['size'=>10]]);

            $spreadsheet->getActiveSheet()->getStyle('A37:C44')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A39:C44')->applyFromArray(['font'=>['size'=>10]]);

            $spreadsheet->getActiveSheet()->getStyle('A55:C62')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A57:C62')->applyFromArray(['font'=>['size'=>10]]);

            $spreadsheet->getActiveSheet()->getStyle('A64:C71')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A66:C71')->applyFromArray(['font'=>['size'=>10]]);

            $spreadsheet->getActiveSheet()->getStyle('A46:C53')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A48:C53')->applyFromArray(['font'=>['size'=>10]]);

            $spreadsheet->getActiveSheet()->getStyle('A73:C80')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A75:C80')->applyFromArray(['font'=>['size'=>10]]);

            $spreadsheet->getActiveSheet()->getStyle('A82:C89')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A84:C89')->applyFromArray(['font'=>['size'=>10]]);

            //OCULTAMOS LAS COLUMNAS A B y C
            $spreadsheet->getActiveSheet()->getColumnDimension('A')->setVisible(false);
            $spreadsheet->getActiveSheet()->getColumnDimension('B')->setVisible(false);
            $spreadsheet->getActiveSheet()->getColumnDimension('C')->setVisible(false);


        ################## END SHEET 6 ############
		$writer = new Xlsx($spreadsheet);
        $writer->setIncludeCharts(true);
		$writer->save('Reporte nom035.xlsx');

    }
}
