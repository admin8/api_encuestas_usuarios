<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Survey;
use App\Models\Answer;
use App\Models\Persons;
use App\Models\PersonJoinAnswer;
use App\Models\Slugs;
use App\Models\Questions;
use App\Models\WpUsuarios;
class SurveyController extends Controller
{
	public function validSurvey($slug = null)
	{
		$wp_nom035 = WpUsuarios::
                select('Encuesta','Nombre','Empresa','FechaRegistro','NumeroDeTrabajadores','Activo AS Active',\DB::raw('IF(NumeroDeTrabajadores="1-15",1,IF(NumeroDeTrabajadores="16-50",1,2)) as Fk_IdSurvey' ),\DB::raw('TIMESTAMPDIFF(HOUR, FechaRegistro, NOW()) AS Horas'))
                ->Where('Encuesta',$slug)
                ->first();
        if (isset($wp_nom035->Horas) && $wp_nom035->Horas>48) {
            return response()->json(['error'=>1,'message'=>'Su encuesta expiro, por lo tanto ya no es posible realizarla, ya que tiene más de 48 horas que estuvo activa.']);
        }

        if(isset($wp_nom035->Encuesta)){
            if(!Slugs::where('Slug', $wp_nom035->Encuesta)->exists()){
                $data['Fk_IdSurvey']   = $wp_nom035->Fk_IdSurvey;
                $data['Description']   = "$wp_nom035->Nombre de $wp_nom035->Empresa de $wp_nom035->NumeroDeTrabajadores trabajadores";
                $data['Slug']          = $wp_nom035->Encuesta;
                $data['Active']        = $wp_nom035->Active;
                $data['NumberSurveys'] = 1;
                Slugs::create($data);
            }
        }

        $result = Slugs::
                select('Fk_IdSurvey','ShowLogo',\DB::raw('IF('.$wp_nom035->Horas.'>48,0,Active) AS Active'), \DB::raw('NumberSurveys - (SELECT IF(COUNT(*) = 1, 0,COUNT(*)) FROM sur_his_persons WHERE Fk_IdSurvey=sur_det_slugs.Fk_IdSurvey AND sur_his_persons.Fk_IdSlug=sur_det_slugs.Id_Slug)   AS Available'))
                ->Where('Delete',0)
                ->Where('Slug',$slug)
                ->first();

        return response()->json($result);
	}
	/**
	 * this method load only question
	 */
	public function getOneQuestion($slug = null, $Id_Person = null, $Id_Survey = null)
	{
        $result = Survey::
                select(\DB::raw("IF(pja.Fk_IdAnswer IS NOT NULL, pja.Fk_IdAnswer,'') as ModelQuestion"),'sur_cat_survey.Id_Survey','sur_cat_survey.Name','q.Id_Question','q.Question','Id_Person','Fk_IdQuestionParent','Fk_IdAnswerValueParent','q.Order')
                ->join('sur_cat_questions as q', function($join1){
                	$join1->on('q.Fk_IdSurvey','=','Id_Survey')
                			->where('q.Delete',0)
                			->where('q.Active',1);
                })
                ->leftjoin('sur_his_persons as p', function($join) use ($Id_Person) {
                	$join->on('p.Fk_IdSurvey','=','Id_Survey')
                		 ->where('p.Id_Person', '=', $Id_Person);
                })
                ->leftjoin('sur_det_person_join_answer as pja', function($j) use ($Id_Person) {
                	$j->on('pja.Fk_IdQuestion', '=' ,'q.Id_Question')
                	  ->where('pja.Fk_IdPerson', '=' , $Id_Person)
                	  ->where('pja.Delete', '=' , 0);
                })
                ->leftjoin('sur_det_slugs as s', function($joins) use ($Id_Person) {
                	$joins->on('s.Fk_IdSurvey','=','Id_Survey')
                	  ->where('s.Delete', '=', 0)
                	  ->where('s.Active', '=', 1);
                })
                ->Where('sur_cat_survey.Delete',0)
                ->Where('sur_cat_survey.Active',1)
                ->whereNull('pja.Fk_IdAnswer')
                ->Where('s.Slug',$slug)
                ->Where('Id_Survey',$Id_Survey)
                ->orderBy('q.Order','ASC')
                ->first();
        
        if ($result) {
        	$result['Answers'] = Answer::select('Id_Answer','Answer')
	        						->whereDelete(0)
	        						->whereActive(1)
	        						->where('Fk_IdQuestion', $result['Id_Question'])
	        						->get()
	        						->toArray();
        } else {
        	$result = Survey::
                select(\DB::raw("IF(COUNT(Id_Question) = COUNT(Fk_IdAnswer),'done','error') as StatusSurvey"))
                ->join('sur_cat_questions as q', function($join1){
                	$join1->on('q.Fk_IdSurvey','=','Id_Survey')
                			->where('q.Delete',0)
                			->where('q.Active',1);
                })
                ->leftjoin('sur_his_persons as p', function($join) use ($Id_Person) {
                	$join->on('p.Fk_IdSurvey','=','Id_Survey')
                		 ->where('p.Id_Person', '=', $Id_Person);
                })
                ->leftjoin('sur_det_person_join_answer as pja', function($j) use ($Id_Person) {
                	$j->on('pja.Fk_IdQuestion','=','q.Id_Question')
                	  ->where('pja.Fk_IdPerson', '=', $Id_Person)
                	  ->where('pja.Delete', '=', 0);
                })
                ->leftjoin('sur_det_slugs as s', function($joins) use ($Id_Person) {
                	$joins->on('s.Fk_IdSurvey','=','Id_Survey')
                	  ->where('s.Delete', '=', 0);
                })
                ->Where('sur_cat_survey.Delete',0)
                ->Where('sur_cat_survey.Active',1)
                ->Where('s.Slug',$slug)
                ->Where('Id_Survey',$Id_Survey)
                ->groupBy('Id_Survey')
                ->first();
            if (isset($result) && $result->StatusSurvey == 'done') {
                Slugs::where('Slug',$slug)->update(['Active' => 0]);
            }
        }
        return response()->json($result);
	}
	public function getSurvey($slug = null, $Id_Person = null)
	{
        $result = Survey::
                select(\DB::raw("IF(pja.Fk_IdAnswer IS NOT NULL, pja.Fk_IdAnswer,NULL) as ModelQuestion"),'sur_cat_survey.Id_Survey','sur_cat_survey.Name','q.Id_Question','q.Question','Id_Person','Fk_IdQuestionParent','Fk_IdAnswerValueParent')
                ->join('sur_cat_questions as q', function($join1){
                	$join1->on('q.Fk_IdSurvey','=','Id_Survey')
                			->where('q.Delete',0)
                			->where('q.Active',1);
                })
                ->leftjoin('sur_his_persons as p', function($join) use ($Id_Person) {
                	$join->on('p.Fk_IdSurvey','=','Id_Survey')
                		 ->where('p.Id_Person', '=', $Id_Person);
                })
                ->leftjoin('sur_det_person_join_answer as pja', function($j) use ($Id_Person) {
                	$j->on('pja.Fk_IdQuestion','=','q.Id_Question')
                	  ->where('pja.Fk_IdPerson', '=', $Id_Person)
                	  ->where('pja.Delete', '=', 0);
                })
                ->leftjoin('sur_det_slugs as s', function($joins) use ($Id_Person) {
                	$joins->on('s.Fk_IdSurvey','=','Id_Survey')
                	  ->where('s.Delete', '=', 0)
                	  ->where('s.Active', '=', 1);
                })
                ->Where('sur_cat_survey.Delete',0)
                ->Where('sur_cat_survey.Active',1)
                ->Where('s.Slug',$slug)
                ->orderBy('q.Order','ASC')
                ->get()->toArray();

        foreach ($result as $k => $val) {
        	$result[$k]['Answers'] = Answer::select('Id_Answer','Answer')
        						->whereDelete(0)
        						->whereActive(1)
        						->where('Fk_IdQuestion', $val['Id_Question'])
        						->get()
        						->toArray();
        }
        return response()->json($result);
	}
	public function getPerson($Id_Person = null, $Id_Survey = null)
	{
		$result = Persons::Where('Id_Person',$Id_Person)
						   ->where('Fk_IdSurvey', $Id_Survey)->first();
        return response()->json($result);
	}
	/**
	 * this function save persons of survey
	 */
	public function savePerson(Request $Request)
	{
		$data = $Request->only('Name','FirstName','LastName','Paysheet','Survey','Id_Person');
		$validate = [
			'Name'         => 'required',
			'FirstName'    => 'required',
			'LastName'     => 'required',
			'Paysheet'     => 'required'
		];
		$valid = \Validator::make($data, $validate);

		if ($valid->passes()) {
			$_slug = Slugs::select('Fk_IdSurvey','Id_Slug')
	                ->Where('Slug',$data['Survey'] )
	                ->first()->toArray();

			$data['Fk_IdSlug'] = $_slug['Id_Slug'];
			$data['Fk_IdSurvey'] = $_slug['Fk_IdSurvey'];

            $save = Persons::create($data);
                    return response()->json(['Fk_IdSurvey'=> $data['Fk_IdSurvey'],'Id_Person'=> $save->Id_Person, 'result'=>'success','alert'=>"Los datos personales se guardaron correctamente!"]);

            /*
			$per = Persons::
							where('Delete',0)
							-> where('Fk_IdSlug', $data['Fk_IdSlug'])
							-> where('Fk_IdSurvey', $data['Fk_IdSurvey'])
							-> where('Paysheet', trim($data['Paysheet']))
							-> count();

			if (isset($data['Id_Person'])) {
				if ($per > 1) {
					return response()->json(['result'=>'error','alert'=>"¡ya existe el registro del numero de nomina $data[Paysheet]!"]);
				}
				$persons = Persons::find($data['Id_Person']);

	            $persons->fill($data);

	            if($persons->save())
					return response()->json(['Fk_IdSurvey'=> $data['Fk_IdSurvey'],'Id_Person'=> $data['Id_Person'], 'result'=>'success','alert'=>"Los datos personales se guardaron correctamente!"]);
			} else {
				if (!$per) {
					$save = Persons::create($data);
					return response()->json(['Fk_IdSurvey'=> $data['Fk_IdSurvey'],'Id_Person'=> $save->Id_Person, 'result'=>'success','alert'=>"Los datos personales se guardaron correctamente!"]);
				} else{
					return response()->json(['result'=>'error','alert'=>"¡$data[Name] $data[FirstName] $data[LastName] ya contesto esta encuesta!"]);
				}
			}*/
		}else{
            return response()->json(['result'=>'error','alert'=>"HO HO! Algo solio mal, favor de refrescar y volver a intentar"]);
        }
	}
	/**
	 * this function save persons of survey
	 */
	public function saveAnswer(Request $Request)
	{
		$data = $Request->only('Fk_IdQuestion','Fk_IdPerson','Fk_IdAnswer');
		$validate = [
			'Fk_IdQuestion' => 'required',
			'Fk_IdPerson'   => 'required',
		];
		$valid = \Validator::make($data, $validate);

		if ($valid->passes()) {
			$date = date('Y-m-d H:i:s');
    		PersonJoinAnswer::whereDelete(0)
    				->where('Fk_IdPerson',$data['Fk_IdPerson'])
    				->where('Fk_IdQuestion',$data['Fk_IdQuestion'])
    				->update(['Delete' => 1]);

			$data['Delete']      = 0;
			$data['DateRecords'] = $date;
    		if (isset($data['Fk_IdAnswer'])) {

	            PersonJoinAnswer::create($data);
	            $ParentQuestion = Questions::where('Fk_IdQuestionParent',$data['Fk_IdQuestion'])
	            				->where('Fk_IdAnswerValueParent','!=',$data['Fk_IdAnswer'])
	            				->whereActive(1)
	            				->whereDelete(0)->get();
	           	if($ParentQuestion){
	           		foreach ($ParentQuestion as $key => $value) {

		           		$data['Fk_IdQuestion'] = $value->Id_Question;
		           		$data['Fk_IdAnswer']   = '1'; // VALOR 0 Y ES UTILIZADA PARA CUANDO SON PREGUNTAS DEPENDIENTES  POR LO REGULAR DE UNA PREGUNTA CON RESPUESTA [SI/NO]

		           		PersonJoinAnswer::create($data);
	           		}
	           	}
                //preguntas para el cuestionario de la guia 1
                $questionGuide1=[123,124,125,126,127,128];

                if (in_array($data['Fk_IdQuestion'], $questionGuide1)) {
                   $ans1 = PersonJoinAnswer::select(\DB::raw('SUM(Value) AS Sum'),\DB::raw('COUNT(sur_det_person_join_answer.Fk_IdAnswer) AS NumAnswers'))
                        ->join('sur_cat_answers','Id_Answer','Fk_IdAnswer')
                        ->where('sur_det_person_join_answer.Delete',0)
                        ->where('Fk_IdPerson',$data['Fk_IdPerson'])
                        ->whereIn('sur_det_person_join_answer.Fk_IdQuestion',$questionGuide1)
                        ->first();
                    if( $ans1 &&  $ans1['Sum'] == 0 && $ans1['NumAnswers'] == 6 ){
                        $_QuestionGuide1 = Questions::where('Fk_IdSurvey',3)//3 => guia 1
                                        ->whereNotIn('Id_Question',$questionGuide1)
                                        ->whereActive(1)
                                        ->whereDelete(0)->get();
                        if($_QuestionGuide1){
                            foreach ($_QuestionGuide1 as $k => $val) {

                                $data['Fk_IdQuestion'] = $val->Id_Question;
                                $data['Fk_IdAnswer']   = '1'; // VALOR 0 Y ES UTILIZADA PARA CUANDO SON PREGUNTAS DEPENDIENTES  POR LO REGULAR DE UNA PREGUNTA CON RESPUESTA [SI/NO]
                                PersonJoinAnswer::create($data);
                            }
                        }
                    }
                }
    		}
			return response()->json(['result'=>'success','alert'=>"Los datos se guardaron correctamente!"]);
		} else {
            return response()->json(['result'=>'error','alert'=>"HO HO! Algo solio mal, favor de refrescar y volver a intentar"]);
        }
	}
    /**
     * this function load results of end survey
     */
    public function getResults($IdSurvey = null, $Id_Person = null)
    {
        if($IdSurvey==2){

            $res = Persons::
                select(
                    \DB::raw('CONCAT_WS(" ", `Name`, FirstName, LastName)  AS Empleado'),
                    \DB::raw('IFNULL((SELECT sum FROM view_sum_for_category sc WHERE sc.Id_Person=sur_his_persons.Id_Person AND sc.Id_Category = 1 LIMIT 1 ),0) AS AmbienteDeTrabajo'),
                    \DB::raw('IFNULL((SELECT sum FROM view_sum_for_category sc WHERE sc.Id_Person=sur_his_persons.Id_Person AND sc.Id_Category = 2 LIMIT 1 ),0) AS FactorPropioDeActividad'),
                    \DB::raw('IFNULL((SELECT sum FROM view_sum_for_category sc WHERE sc.Id_Person=sur_his_persons.Id_Person AND sc.Id_Category = 3 LIMIT 1 ),0) AS OrganizacionTiempoTrabajo'),
                    \DB::raw('IFNULL((SELECT sum FROM view_sum_for_category sc WHERE sc.Id_Person=sur_his_persons.Id_Person AND sc.Id_Category = 4 LIMIT 1 ),0) AS LiderazgoRelaciones'),
                    \DB::raw('IFNULL((SELECT sum FROM view_sum_for_category sc WHERE sc.Id_Person=sur_his_persons.Id_Person AND sc.Id_Category = 5 LIMIT 1 ),0) AS EntornoOrganizacional')
                    )
                ->Where('Delete',0)
                ->groupBy('Id_Person')
                ->where('Id_Person',$Id_Person)
                ->where('Fk_IdSurvey',$IdSurvey)
                ->first();
            $result=['Empleado' => $res->Empleado];

            $riesgoGral = ($res->AmbienteDeTrabajo + $res->FactorPropioDeActividad + $res->OrganizacionTiempoTrabajo + $res->LiderazgoRelaciones + $res->EntornoOrganizacional);
            if($riesgoGral < 50){
                $result['colorGral'] = '#9be5f7';
                $result['riesgoGral'] = 'nulo';
            } else if ($riesgoGral >= 50 && $riesgoGral < 75) {
                $result['colorGral'] = '#6bf56e';
                $result['riesgoGral'] = 'bajo';
            } else if ($riesgoGral >= 75 && $riesgoGral < 99) {
                $result['colorGral'] = '#ffff00';
                $result['riesgoGral'] = 'medio';
            } else if ($riesgoGral >= 99 && $riesgoGral < 140) {
                $result['colorGral'] = '#ffc000';
                $result['riesgoGral'] = 'alto';
            } else if ($riesgoGral >= 140) {
                $result['colorGral'] = '#ff0000';
                $result['riesgoGral'] = 'muy alto';
            }

            $i=0;
            $result['val'][$i]['categoria'] = 'Ambiente de trabajo';
            if($res->AmbienteDeTrabajo < 5){
                $result['val'][$i]['riesgo'] = 'nulo';
                $result['val'][$i]['color'] = '#9be5f7';
            } else if ($res->AmbienteDeTrabajo >= 5 && $res->AmbienteDeTrabajo < 9) {
                $result['val'][$i]['riesgo'] = 'bajo';
                $result['val'][$i]['color'] = '#6bf56e';
            } else if ($res->AmbienteDeTrabajo >= 9 && $res->AmbienteDeTrabajo < 11) {
                $result['val'][$i]['riesgo'] = 'medio';
                $result['val'][$i]['color'] = '#ffff00';
            } else if ($res->AmbienteDeTrabajo >= 11 && $res->AmbienteDeTrabajo < 14) {
                $result['val'][$i]['riesgo'] = 'alto';
                $result['val'][$i]['color'] = '#ffc000';
            } else if ($res->AmbienteDeTrabajo >= 14) {
                $result['val'][$i]['riesgo'] = 'muy alto';
                $result['val'][$i]['color'] = '#ff0000';
            }
            $i++;
            $result['val'][$i]['categoria'] = 'Factores propios de la actividad';
            if($res->FactorPropioDeActividad < 15){
                $result['val'][$i]['riesgo'] = 'nulo';
                $result['val'][$i]['color'] = '#9be5f7';
            } else if ($res->FactorPropioDeActividad >= 15 && $res->FactorPropioDeActividad < 30) {
                $result['val'][$i]['riesgo'] = 'bajo';
                $result['val'][$i]['color'] = '#6bf56e';
            } else if ($res->FactorPropioDeActividad >= 30 && $res->FactorPropioDeActividad < 45) {
                $result['val'][$i]['riesgo'] = 'medio';
                $result['val'][$i]['color'] = '#ffff00';
            } else if ($res->FactorPropioDeActividad >= 45 && $res->FactorPropioDeActividad < 60) {
                $result['val'][$i]['riesgo'] = 'alto';
                $result['val'][$i]['color'] = '#ffc000';
            } else if ($res->FactorPropioDeActividad >= 60) {
                $result['val'][$i]['riesgo'] = 'muy alto';
                $result['val'][$i]['color'] = '#ff0000';
            }

            $i++;
            $result['val'][$i]['categoria'] = 'Organización del tiempo de trabajo';
            if($res->OrganizacionTiempoTrabajo < 5){
                $result['val'][$i]['riesgo'] = 'nulo';
                $result['val'][$i]['color'] = '#9be5f7';
            } else if ($res->OrganizacionTiempoTrabajo >= 5 && $res->OrganizacionTiempoTrabajo < 7) {
                $result['val'][$i]['riesgo'] = 'bajo';
                $result['val'][$i]['color'] = '#6bf56e';
            } else if ($res->OrganizacionTiempoTrabajo >= 7 && $res->OrganizacionTiempoTrabajo < 10) {
                $result['val'][$i]['riesgo'] = 'medio';
                $result['val'][$i]['color'] = '#ffff00';
            } else if ($res->OrganizacionTiempoTrabajo >= 10 && $res->OrganizacionTiempoTrabajo < 13) {
                $result['val'][$i]['riesgo'] = 'alto';
                $result['val'][$i]['color'] = '#ffc000';
            } else if ($res->OrganizacionTiempoTrabajo >= 13) {
                $result['val'][$i]['riesgo'] = 'muy alto';
                $result['val'][$i]['color'] = '#ff0000';
            }

            $i++;
            $result['val'][$i]['categoria'] = 'Liderazgo y relaciones en el trabajo';
            if($res->LiderazgoRelaciones < 14){
                $result['val'][$i]['riesgo'] = 'nulo';
                $result['val'][$i]['color'] = '#9be5f7';
            } else if ($res->LiderazgoRelaciones >= 14 && $res->LiderazgoRelaciones < 29) {
                $result['val'][$i]['riesgo'] = 'bajo';
                $result['val'][$i]['color'] = '#6bf56e';
            } else if ($res->LiderazgoRelaciones >= 29 && $res->LiderazgoRelaciones < 42) {
                $result['val'][$i]['riesgo'] = 'medio';
                $result['val'][$i]['color'] = '#ffff00';
            } else if ($res->LiderazgoRelaciones >= 42 && $res->LiderazgoRelaciones < 58) {
                $result['val'][$i]['riesgo'] = 'alto';
                $result['val'][$i]['color'] = '#ffc000';
            } else if ($res->LiderazgoRelaciones >= 58) {
                $result['val'][$i]['riesgo'] = 'muy alto';
                $result['val'][$i]['color'] = '#ff0000';
            }

            $i++;
            $result['val'][$i]['categoria'] = 'Entorno organizacional';
            if($res->EntornoOrganizacional < 10){
                $result['val'][$i]['riesgo'] = 'nulo';
                $result['val'][$i]['color'] = '#9be5f7';
            } else if ($res->EntornoOrganizacional >= 10 && $res->EntornoOrganizacional < 14) {
                $result['val'][$i]['riesgo'] = 'bajo';
                $result['val'][$i]['color'] = '#6bf56e';
            } else if ($res->EntornoOrganizacional >= 14 && $res->EntornoOrganizacional < 18) {
                $result['val'][$i]['riesgo'] = 'medio';
                $result['val'][$i]['color'] = '#ffff00';
            } else if ($res->EntornoOrganizacional >= 18 && $res->EntornoOrganizacional < 23) {
                $result['val'][$i]['riesgo'] = 'alto';
                $result['val'][$i]['color'] = '#ffc000';
            } else if ($res->EntornoOrganizacional >= 23) {
                $result['val'][$i]['riesgo'] = 'muy alto';
                $result['val'][$i]['color'] = '#ff0000';
            }

        } else if ($IdSurvey == 1) {

            $res = Persons::
                select(
                    \DB::raw('CONCAT_WS(" ", `Name`, FirstName, LastName)  AS Empleado'),
                    \DB::raw('IFNULL((SELECT sum FROM view_sum_for_category sc WHERE sc.Id_Person=sur_his_persons.Id_Person AND sc.Id_Category = 1 LIMIT 1 ),0) AS AmbienteDeTrabajo'),
                    \DB::raw('IFNULL((SELECT sum FROM view_sum_for_category sc WHERE sc.Id_Person=sur_his_persons.Id_Person AND sc.Id_Category = 2 LIMIT 1 ),0) AS FactorPropioDeActividad'),
                    \DB::raw('IFNULL((SELECT sum FROM view_sum_for_category sc WHERE sc.Id_Person=sur_his_persons.Id_Person AND sc.Id_Category = 3 LIMIT 1 ),0) AS OrganizacionTiempoTrabajo'),
                    \DB::raw('IFNULL((SELECT sum FROM view_sum_for_category sc WHERE sc.Id_Person=sur_his_persons.Id_Person AND sc.Id_Category = 4 LIMIT 1 ),0) AS LiderazgoRelaciones')
                    )
                ->Where('Delete',0)
                ->groupBy('Id_Person')
                ->where('Id_Person',$Id_Person)
                ->where('Fk_IdSurvey',$IdSurvey)
                ->first();

            $result=['Empleado' => $res->Empleado];

            $riesgoGral = ($res->AmbienteDeTrabajo + $res->FactorPropioDeActividad + $res->OrganizacionTiempoTrabajo + $res->LiderazgoRelaciones + $res->EntornoOrganizacional);
            if($riesgoGral < 20){
                $result['colorGral'] = '#9be5f7';
                $result['riesgoGral'] = 'nulo';
            } else if ($riesgoGral >= 20 && $riesgoGral < 45) {
                $result['colorGral'] = '#6bf56e';
                $result['riesgoGral'] = 'bajo';
            } else if ($riesgoGral >= 45 && $riesgoGral < 70) {
                $result['colorGral'] = '#ffff00';
                $result['riesgoGral'] = 'medio';
            } else if ($riesgoGral >= 70 && $riesgoGral < 90) {
                $result['colorGral'] = '#ffc000';
                $result['riesgoGral'] = 'alto';
            } else if ($riesgoGral >= 90) {
                $result['colorGral'] = '#ff0000';
                $result['riesgoGral'] = 'muy alto';
            }

            $i=0;
            $result['val'][$i]['categoria'] = 'Ambiente de trabajo';
            if($res->AmbienteDeTrabajo < 3){
                $result['val'][$i]['riesgo'] = 'nulo';
                $result['val'][$i]['color'] = '#9be5f7';
            } else if ($res->AmbienteDeTrabajo >= 3 && $res->AmbienteDeTrabajo < 5) {
                $result['val'][$i]['riesgo'] = 'bajo';
                $result['val'][$i]['color'] = '#6bf56e';
            } else if ($res->AmbienteDeTrabajo >= 5 && $res->AmbienteDeTrabajo < 7) {
                $result['val'][$i]['riesgo'] = 'medio';
                $result['val'][$i]['color'] = '#ffff00';
            } else if ($res->AmbienteDeTrabajo >= 7 && $res->AmbienteDeTrabajo < 9) {
                $result['val'][$i]['riesgo'] = 'alto';
                $result['val'][$i]['color'] = '#ffc000';
            } else if ($res->AmbienteDeTrabajo >= 9) {
                $result['val'][$i]['riesgo'] = 'muy alto';
                $result['val'][$i]['color'] = '#ff0000';
            }
            $i++;
            $result['val'][$i]['categoria'] = 'Factores propios de la actividad';
            if($res->FactorPropioDeActividad < 10){
                $result['val'][$i]['riesgo'] = 'nulo';
                $result['val'][$i]['color'] = '#9be5f7';
            } else if ($res->FactorPropioDeActividad >= 10 && $res->FactorPropioDeActividad < 20) {
                $result['val'][$i]['riesgo'] = 'bajo';
                $result['val'][$i]['color'] = '#6bf56e';
            } else if ($res->FactorPropioDeActividad >= 20 && $res->FactorPropioDeActividad < 30) {
                $result['val'][$i]['riesgo'] = 'medio';
                $result['val'][$i]['color'] = '#ffff00';
            } else if ($res->FactorPropioDeActividad >= 30 && $res->FactorPropioDeActividad < 40) {
                $result['val'][$i]['riesgo'] = 'alto';
                $result['val'][$i]['color'] = '#ffc000';
            } else if ($res->FactorPropioDeActividad >= 40) {
                $result['val'][$i]['riesgo'] = 'muy alto';
                $result['val'][$i]['color'] = '#ff0000';
            }

            $i++;
            $result['val'][$i]['categoria'] = 'Organización del tiempo de trabajo';
            if($res->OrganizacionTiempoTrabajo < 4){
                $result['val'][$i]['riesgo'] = 'nulo';
                $result['val'][$i]['color'] = '#9be5f7';
            } else if ($res->OrganizacionTiempoTrabajo >= 4 && $res->OrganizacionTiempoTrabajo < 6) {
                $result['val'][$i]['riesgo'] = 'bajo';
                $result['val'][$i]['color'] = '#6bf56e';
            } else if ($res->OrganizacionTiempoTrabajo >= 6 && $res->OrganizacionTiempoTrabajo < 9) {
                $result['val'][$i]['riesgo'] = 'medio';
                $result['val'][$i]['color'] = '#ffff00';
            } else if ($res->OrganizacionTiempoTrabajo >= 9 && $res->OrganizacionTiempoTrabajo < 12) {
                $result['val'][$i]['riesgo'] = 'alto';
                $result['val'][$i]['color'] = '#ffc000';
            } else if ($res->OrganizacionTiempoTrabajo >= 12) {
                $result['val'][$i]['riesgo'] = 'muy alto';
                $result['val'][$i]['color'] = '#ff0000';
            }

            $i++;
            $result['val'][$i]['categoria'] = 'Liderazgo y relaciones en el trabajo';
            if($res->LiderazgoRelaciones < 10){
                $result['val'][$i]['riesgo'] = 'nulo';
                $result['val'][$i]['color'] = '#9be5f7';
            } else if ($res->LiderazgoRelaciones >= 10 && $res->LiderazgoRelaciones < 18) {
                $result['val'][$i]['riesgo'] = 'bajo';
                $result['val'][$i]['color'] = '#6bf56e';
            } else if ($res->LiderazgoRelaciones >= 18 && $res->LiderazgoRelaciones < 28) {
                $result['val'][$i]['riesgo'] = 'medio';
                $result['val'][$i]['color'] = '#ffff00';
            } else if ($res->LiderazgoRelaciones >= 28 && $res->LiderazgoRelaciones < 38) {
                $result['val'][$i]['riesgo'] = 'alto';
                $result['val'][$i]['color'] = '#ffc000';
            } else if ($res->LiderazgoRelaciones >= 38) {
                $result['val'][$i]['riesgo'] = 'muy alto';
                $result['val'][$i]['color'] = '#ff0000';
            }

            }
            if(isset($result)){



                return response()->json($result);
            }
            return response()->json([]);
    }
    /**
     * this function create pdf and send email to users
     */
    public function getPdf(Request $Request)
    {
        $data = $Request->only('slug','IdPerson','IdSurvey');
		$validate = [
			'slug'         => 'required',
			'IdPerson'     => 'required',
			'IdSurvey'     => 'required',
			'Paysheet'     => 'required'
		];
		$valid = \Validator::make($data, $validate);

		if ($valid->passes()) {
			$_slug = Slugs::select('Fk_IdSurvey','Id_Slug')
	                ->Where('Slug',$data['Survey'] )
	                ->first()->toArray();

			$data['Fk_IdSlug'] = $_slug['Id_Slug'];
			$data['Fk_IdSurvey'] = $_slug['Fk_IdSurvey'];

            $save = Persons::create($data);
                    return response()->json(['Fk_IdSurvey'=> $data['Fk_IdSurvey'],'Id_Person'=> $save->Id_Person, 'result'=>'success','alert'=>"Los datos personales se guardaron correctamente!"]);

            /*
			$per = Persons::
							where('Delete',0)
							-> where('Fk_IdSlug', $data['Fk_IdSlug'])
							-> where('Fk_IdSurvey', $data['Fk_IdSurvey'])
							-> where('Paysheet', trim($data['Paysheet']))
							-> count();

			if (isset($data['Id_Person'])) {
				if ($per > 1) {
					return response()->json(['result'=>'error','alert'=>"¡ya existe el registro del numero de nomina $data[Paysheet]!"]);
				}
				$persons = Persons::find($data['Id_Person']);

	            $persons->fill($data);

	            if($persons->save())
					return response()->json(['Fk_IdSurvey'=> $data['Fk_IdSurvey'],'Id_Person'=> $data['Id_Person'], 'result'=>'success','alert'=>"Los datos personales se guardaron correctamente!"]);
			} else {
				if (!$per) {
					$save = Persons::create($data);
					return response()->json(['Fk_IdSurvey'=> $data['Fk_IdSurvey'],'Id_Person'=> $save->Id_Person, 'result'=>'success','alert'=>"Los datos personales se guardaron correctamente!"]);
				} else{
					return response()->json(['result'=>'error','alert'=>"¡$data[Name] $data[FirstName] $data[LastName] ya contesto esta encuesta!"]);
				}
			}*/
		}else{
            return response()->json(['result'=>'error','alert'=>"HO HO! Algo solio mal, favor de refrescar y volver a intentar"]);
        }
    }
}
