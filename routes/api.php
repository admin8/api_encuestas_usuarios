<?php

use Illuminate\Http\Request;

/*
jwt login
https://medium.com/@manuelmauriciozamarrn/implementing-jwt-on-laravel-5-8-edc39f545886

|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
    'middleware' => 'api'
], function ($router) {

	//routes of survey
	Route::get('/survey/valid-survey/{survey}','SurveyController@validSurvey');
	Route::get('/survey/get-person/{Id_Person}/{Id_Survey}','SurveyController@getPerson');
	Route::get('/survey/get-one/{survey}/{Id_Person}/{Id_Survey}','SurveyController@getOneQuestion');
	Route::get('/survey/{survey}/{Id_Person}','SurveyController@getSurvey');
	Route::post('/survey/save-person','SurveyController@savePerson');
	Route::post('/survey/save-answer','SurveyController@saveAnswer');

	Route::get('/survey/get-results/{IdSurvey}/{Id_Person}','SurveyController@getResults');
	Route::post('/survey/get-pdf','PdfController@createPdf');

	Route::post('users/login', 'APILoginController@login');
	Route::post('users/logout', 'APILoginController@logout');


	Route::get('/results/total/slug/{slugs?}/token/{token}','ResultsController@GetTotalGraphic');
	Route::get('/results/categories/slug/{slugs?}/token/{token}','ResultsController@GetCategoriesGraphics');
	Route::get('/results/domains/slug/{slugs?}/token/{token}','ResultsController@GetDomainsGraphics');
	Route::get('/results/all/slug/{slugs?}/token/{token}','ResultsController@GetAllGraphics');
	Route::get('/results/slug-excel/token/{token}','ResultsController@loadSlugsForExcel');
	Route::get('/results/generate-excel/slug/{slugs?}/token/{token}','ResultsController@GenereteExcel');
});
